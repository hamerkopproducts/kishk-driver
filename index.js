/**
 * @format
 */
import 'react-native-gesture-handler';
/**
 * @format
 */
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import App from './App';
import { PersistGate } from 'redux-persist/es/integration/react';
import { store, persistor } from './configureStore';


export default class Emtyaz extends Component {
    render() {
        return (
        <Provider store={store}>
        <PersistGate loading={null} persistor={persistor} />
        <App />
    </Provider>
        );
    }
}
AppRegistry.registerComponent(appName, () => Emtyaz);
