import * as ActionTypes from '../actions/types'
import { collectionList } from '../lib/helperMethods'

const defaultState = {
    addressData: [],
    addressdetailData: [],
    isLoading: false,
    error: undefined,
    success: undefined,
    singleData: [],
    collectionListData: {},
    AllcollectionListData: {},
    TodaysRequestcollectionListData: {},
    AllRequestcollectionListData: {},
    collectOrderStatus: {},
    deliveredOrderStatus: {},
    orderDetailsData: {},
    requestDetailsData: {},
    requestCollectionSuccess: {},
    
    totalCount: 0,
    todaysCount: 0,
    requestTotalCount: 0,
    requestTodaysCount: 0,
};

export default function collectionReducer(state = defaultState, action) {
    switch (action.type) {
        case ActionTypes.COLLECTION_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.TODAYS_REQUEST_COLLECTION_SUCCESS:
            console.log("COUNT", action.responseData.data.total,)
            return Object.assign({}, state, {
                isLoading: false,
                TodaysRequestcollectionListData: action.responseData.data,
                requestTodaysCount: action.responseData.data.total,

            });
        case ActionTypes.TODAYS_REQUEST_COLLECTION_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
            });
        case ActionTypes.ALL_REQUEST_COLLECTION_SUCCESS:
            console.log("COUNT", action.responseData.data.total,)
            return Object.assign({}, state, {
                isLoading: false,
                AllRequestcollectionListData: action.responseData.data,
                requestTotalCount: action.responseData.data.total,

            });
        case ActionTypes.ALL_REQUEST_COLLECTION_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
            });
        case ActionTypes.COLLECTION_REQUEST_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                requestCollectionSuccess : action.responseData,

            });
        case ActionTypes.COLLECTION_REQUEST_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
            });
        case ActionTypes.ALL_COLLECTION_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                AllcollectionListData: action.responseData.data,
                totalCount: action.responseData.data.total,

            });
        case ActionTypes.ALL_COLLECTION_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
            });
        case ActionTypes.RESET_COLLECT_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                collectOrderStatus: {},

            });
        case ActionTypes.RESET_REQUEST_COLLECT_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                requestCollectionSuccess: {},

            });
        case ActionTypes.COLLECT_ORDER_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                collectOrderStatus: action.responseData,

            });
        case ActionTypes.COLLECT_ORDER_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                collectOrderStatus: action.responseData,
            });
        case ActionTypes.DELIVERED_ORDER_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredOrderStatus: action.responseData,

            });
        case ActionTypes.DELIVERED_ORDER_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredOrderStatus: action.responseData,
            });
        case ActionTypes.TODAYS_COLLECTION_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                collectionListData: action.responseData.data,
                todaysCount: action.responseData.data.total,
            });
        case ActionTypes.TODAYS_COLLECTION_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
            });
        case ActionTypes.ORDER_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                orderDetailsData: action.responseData
            });
        case ActionTypes.ORDER_DETAILS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                orderDetailsData: action.responseData
            });
        case ActionTypes.REQUEST_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                requestDetailsData: action.responseData
            });
        case ActionTypes.REQUEST_DETAILS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                requestDetailsData: action.responseData
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                addressData: [],
                addressdetailData: [],
                isLoading: false,
                error: undefined,
                success: undefined,
                singleData: [],
                collectionListData: {},
                AllcollectionListData: {},
                collectOrderStatus: {},
                deliveredOrderStatus: {},
                orderDetailsData: {},
                requestDetailsData: {},
                totalCount: 0,
                todaysCount: 0,
            };
        case ActionTypes.SINGLE_DATA_SUCCESS:
            //console.log(action.responseData)
            return Object.assign({}, state, {
                isLoading: false,
                singleData: action.responseData
            });
        default:
            return state;
    }
}