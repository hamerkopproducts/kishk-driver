import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import collectionReducer from './collectionReducer'
import termsFaqPrivacyReducer from './termsFaqPrivacyReducer'
import supportChatReducer from './supportChatReducer'
import editUserReducer from './editUserReducer';
import orderReducer from './orderReducer';
const rootReducer = combineReducers({
    loginReducer,
    collectionReducer,
    termsFaqPrivacyReducer,
    supportChatReducer,
    editUserReducer,
    orderReducer

});

export default rootReducer;
