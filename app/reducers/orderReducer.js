import * as ActionTypes from '../actions/types'

const defaultState = {
    isLoading: false,
    error: undefined,
    success: undefined,
    activeOrderListData: {},
    todaysOrderListData: {},
    deliveredOrderListData: {},
    orderDetailsData: {},
    activeCount: 0,
    deliveredCount: 0,
    todaysCount: 0,

    activeRequestListData: {},
    deliveredRequestListData: {},
    todaysRequestListData: {},
    activeRequestCount: 0,
    deliveredRequestCount: 0,
    todaysRequestCount: 0,
   

};

export default function orderReducer(state = defaultState, action) {
    switch (action.type) {
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });

        case ActionTypes.TODAYS_ORDERS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                todaysOrderListData: action.responseData,
                todaysCount: action.responseData.data.total
            });
        case ActionTypes.TODAYS_ORDERS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                todaysOrderListData: action.responseData
            });

        case ActionTypes.ACTIVE_ORDERS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                activeOrderListData: action.responseData,
                activeCount: action.responseData.data.total
            });
        case ActionTypes.ACTIVE_ORDERS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                activeOrderListData: action.responseData
            });
        case ActionTypes.DELIVERED_ORDERS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredOrderListData: action.responseData,
                deliveredCount: action.responseData.data.total
            });
        case ActionTypes.DELIVERED_ORDERS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredOrderListData: action.responseData
            });
        case ActionTypes.TODAYS_REQUEST_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                todaysRequestListData: action.responseData,
                todaysRequestCount: action.responseData.data.total
            });
        case ActionTypes.TODAYS_REQUEST_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                todaysRequestListData: action.responseData
            });

        case ActionTypes.ACTIVE_REQUEST_SUCCESS:
            // console.log("COUNT", action.responseData.data.total)
            return Object.assign({}, state, {
                isLoading: false,
                activeRequestListData: action.responseData,
                activeRequestCount: action.responseData.data.total
            });
        case ActionTypes.ACTIVE_REQUEST_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                activeRequestListData: action.responseData
            });
        case ActionTypes.DELIVERED_REQUEST_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredRequestListData: action.responseData,
                deliveredRequestCount: action.responseData.data.total
            });
        case ActionTypes.DELIVERED_REQUEST_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                deliveredRequestListData: action.responseData
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                error: undefined,
                success: undefined, 
                activeOrderListData: {},
                todaysOrderListData: {},
                deliveredOrderListData: {},
                orderDetailsData: {},
                activeCount: 0,
                deliveredCount: 0,
                todaysCount: 0,
            };
        default:
            return state;
    }
}