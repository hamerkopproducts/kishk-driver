import * as ActionTypes from '../actions/types'

const defaultLoginState = {
  userData: {},
  isLogged: false,
  resetNavigation: undefined,
  isLoading: false,
  error: undefined,
  success: undefined,
  getotpAPIReponse: {},
  otpAPIError: {},
  resendOtp: {},
  notificationCount: {}
};

export default function loginReducer(state = defaultLoginState, action) {
  switch (action.type) {


    case ActionTypes.GET_OTP_SERVICE_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        getotpAPIResponseError: action.error,
      });
    case ActionTypes.RESET_LOGOUT_DATA:
      return Object.assign({}, state, {
        logoutData: {},
      });
    case ActionTypes.GET_OTP_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        getotpAPIResponse: action.responseData,
      });
    case ActionTypes.RESEND_OTP_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        resendOtp: action.responseData,
      });
    case ActionTypes.NOTIFICATION_COUNT:
      return Object.assign({}, state, {
        isLoading: true,
        notificationCount: action.responseData,
      });
    case ActionTypes.LOGIN_SERVICE_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ActionTypes.LOGIN_SERVICE_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case ActionTypes.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        userData: action.responseData,
        isLogged: true
      });
    case ActionTypes.LOGIN_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        otpAPIError: action.error,
      });
    //alert('homescreen')
    case ActionTypes.LOGOUT_SUCCESS:
      return {
        ...state,
        logoutData: action.responseData,
        userData: {},
        isLogged: false,
        isLoading: false,
        error: undefined,
        success: undefined,
        resetNavigation: action.resetNavigation,
        isSessionExpired: true,
      };
    default:
      return state;
  }
}
