import {I18nManager} from 'react-native';
import {displayName as appName} from '../../app.json';

module.exports = {
  SCREEN_TITLE: {
    loginScreenTitle: 'Login',
  },
  STRING: {
    appName: appName,
    login: I18nManager.isRTL ? 'تسجيل الدخول' : 'Login',
    home: I18nManager.isRTL ? 'الصفحة الرئيسية' : 'Home',
    products: I18nManager.isRTL ? 'منتجات' : 'Products',
    favourites: I18nManager.isRTL ? 'المفضلة' : 'Favourites',
    myOrders: I18nManager.isRTL ? 'طلباتي' : 'My Orders',
    myProfile: I18nManager.isRTL ? 'ملفاتي' : 'My Profile',
    myOrdersview: I18nManager.isRTL ? 'MyOrdersView': 'MyOrdersView',
    productsview: I18nManager.isRTL ? 'ProductsView': 'ProductsView',
    scanview: I18nManager.isRTL ?'Scan View' : 'Scan View',
  },
  ALERT_MESSAGES: {
    specifyUsername: 'Enter Username.',
    specifyPassword: 'Enter Password.',
    specifyCreatePassword: 'Enter Create Password.',
    specifyNewPassword: 'Enter New Password.',
    specifyConfirmPassword: 'Enter Confirm Password.',
    passwordNotMatched: 'New Password and Confirm Password entries must match.',
    passwordNotValid: 'Enter a valid Password',
    specifyCurrentPassword: 'Enter Current Password.',
    noInternet: 'No Network Connection',
    touchIDDisabled: 'Touch ID login is disabled in your application.',
    specifyFriendlyName: 'Enter Friendly name.',
    invalidPin: 'Invalid Pin.',
    specifyPin: 'Enter Pin.',
     digit: I18nManager.isRTL
      ? 'Phone number should be 6-8 digits in length'
      : 'Phone number should be 6-8 digits in length',
      error: I18nManager.isRTL ? 'Error' : 'Error',
      phoneRequired: I18nManager.isRTL
      ? 'Phone number is required'
      : 'Phone number is required',
      firstnameRequired: I18nManager.isRTL
      ? 'Name is required'
      : 'Name is required',
      emailRequired: I18nManager.isRTL
      ? 'عنوان البريد الإلكتروني مطلوب'
      : 'Email address is required',
    validEmailRequired: I18nManager.isRTL
      ? 'الرجاء إدخال بريد إلكتروني صحيح'
      : 'Enter a valid email address',
  },
  APP_SLIDER: {
    Title: I18nManager.isRTL ? 'Effortless Payments!' : 'Effortless Payments!',
    Content: I18nManager.isRTL
      ? 'Lorem ipsum dolor sit amet, consectetur adispiscing elutwe sed '
      : 'Lorem ipsum dolor sit amet, consectetur adispiscing elutwe sed ',
    KishkWel: I18nManager.isRTL ? 'Welcome to KISHK' : 'Welcome to KISHK',
  },
  PROFILE: {
    Head: I18nManager.isRTL ? 'حسابي' : 'My Account',
    MyAccount: I18nManager.isRTL ? 'حسابي' : 'My Account',
    MyOrders: I18nManager.isRTL ? 'طلباتي' : 'My Orders',
    Mywishlist: I18nManager.isRTL ? 'قائمة الرغبات' : 'My Wishlist',
    Address: I18nManager.isRTL ? 'العناوين' : 'Address',
    Settings: I18nManager.isRTL ? 'الإعدادات والدعم' : 'Settings & Support',
    Change: I18nManager.isRTL ? 'تغيير اللغة' : 'Change language',
    Support: I18nManager.isRTL ? 'الدعم' : 'Support',
    FAQ: I18nManager.isRTL ? 'الأسئلة الاكثر شيوعاً' : 'FAQs',
    Privacy: I18nManager.isRTL ? 'سياسة الخصوصية' : 'Privacy Policy',
    Terms: I18nManager.isRTL ? 'الأحكام والشروط' : 'Terms And Conditions',
    About: I18nManager.isRTL ? 'عن كشك' : 'About Us',
    Addaddress: I18nManager.isRTL ? 'أضافة عنوان جديد' : 'Add New Address',
    Edit: I18nManager.isRTL ? 'تعديل العنوان' : 'Edit Address',
    profilemsg: I18nManager.isRTL
      ? 'Profile updated successfully'
      : 'Profile updated successfully',
  },
  SELECT_LANGUAGE: {
    Choose: I18nManager.isRTL ? 'Choose your language' : 'Choose your language',
    En: I18nManager.isRTL ? 'English' : 'English',
    AR: I18nManager.isRTL ? 'عربى' : 'عربى',
    selectL: I18nManager.isRTL ? 'Select Language' : 'Select Language',
  },
  //OtpScreen
  OTP: {
    expires: I18nManager.isRTL ? 'ينتهي في' : 'Expires In',
    VerifyY: I18nManager.isRTL ? 'أكد التسجيل و ابدأ بالتسوق' : 'Verify Your',
    Accounta: I18nManager.isRTL ? 'Account &' : 'Account &',
    starts: I18nManager.isRTL ? 'Start Shopping' : 'Start Shopping',
    enterC: I18nManager.isRTL ? 'ادخل الرمز' : 'Enter Code',
    resendC: I18nManager.isRTL ? 'أعادة إرسال الرمز' : 'Resend Code',
	VerifyDes: I18nManager.isRTL
      ? 'م إرسال رمز التحقق إلى الرقم'
      : 'Verification Code has been sent to ', 
	verifyText: I18nManager.isRTL ? 'ارسال' : 'Verify',
	verifyAccount: I18nManager.isRTL ? 'تأكيد التسجيل' : 'Verify Your Account',
	account: I18nManager.isRTL
	? 'أكد التسجيل حسابك عن طريق إدخال الرمز'
	: 'Verify Your Account by entering the code.'
	},
	COLLECTION:{
		today:I18nManager.isRTL ? 'Todays Collection' : 'Todays collection',
		all:I18nManager.isRTL ? 'All Collection' : 'All Collection',
	},
	Appintro_login:{
		login:I18nManager.isRTL ? 'Login' : 'Login',
		loginHeading:I18nManager.isRTL ? 'Please login to your account' : 'Please login to your account',
		phoneNumber:I18nManager.isRTL ? 'رقم الهاتف' : 'Phone Number',
		signIn:I18nManager.isRTL ? 'تسجيل الدخول' : 'Sign In',
	},
	NOTIFY:{
		heading:I18nManager.isRTL ? 'إشعارات' : 'Notifications',
	},
	MODAL:{
		deliver:I18nManager.isRTL ? 'Did you deliver the order to the customer?' : 'Did you deliver the order to the customer?',
    collect:I18nManager.isRTL ? 'Did you collect the order?' : 'Did you collect the order?',
    reqdeliver:I18nManager.isRTL ? 'Did you deliver the delivery request ?' : 'Did you deliver the delivery request ?',
		reqcollect:I18nManager.isRTL ? 'Did you collect the delivery request?' : 'Did you collect the delivery request?',
		no:I18nManager.isRTL ? 'No' : 'No',
		yes:I18nManager.isRTL ? 'Yes' : 'Yes',
		accept:I18nManager.isRTL ? 'ACCEPT' : 'ACCEPT',
		reject:I18nManager.isRTL ? 'REJECT' : 'REJECT',
	},
  FAQ:{
    faq:I18nManager.isRTL ? 'FAQs' : 'FAQs',
  },
  PRIVACY:{
    privacy:I18nManager.isRTL ? 'Privacy Ploicies' : 'Privacy Policies',
  },
  TERMS:{
    terms:I18nManager.isRTL ? 'Terms and Conditions' : 'Terms and Conditions',
  },
  COLLECTORDER:{
    success:I18nManager.isRTL ? 'Order collected' : 'Order collected',
  },

  LOGIN_TOASTMESSAGES: {
    error: I18nManager.isRTL ? 'Error' : 'Error',
    required: I18nManager.isRTL ? 'رقم الهاتف مطلوب' : 'Phone number required!',
    requiredmail: I18nManager.isRTL
      ? 'الرجاء إدخال عنوان البريد الإلكتروني الخاص بك'
      : ' Please enter your Email Address',
    digit: I18nManager.isRTL
      ? 'يجب أن يتكون رقم الهاتف من 8 أرقام'
      : 'Phone number should be 6-8 digits in length',
    mailvalid: I18nManager.isRTL
      ? 'الرجاء إدخال بريد إلكتروني صحيح'
      : 'Please enter a valid Email ID',
    otp: I18nManager.isRTL ? 'الرجاء إدخال الOTP' : 'Please enter OTP',
    otpsend: I18nManager.isRTL
      ? 'تم أراسل الOTP بنجاح'
      : 'OTP send successfully',
    logout: I18nManager.isRTL ? 'تسجيل الخروج' : 'logout',
  },
  //OrderDetailsScreen
  ORDER:{
	details: I18nManager.isRTL ? 'تفاصيل الطلب' : 'Order Details',
  custom: I18nManager.isRTL ? 'مصممة حسب الطلب' : 'Custom',
  sar: I18nManager.isRTL ? ' دينار بحريني' : 'BHD',
  collected:I18nManager.isRTL ? 'COLLECTED' : 'COLLECTED ',
  delivered:I18nManager.isRTL ? 'DELIVERED' : 'DELIVERED ',
  bill:I18nManager.isRTL ? 'Bill Details' : 'Bill Details ',
  subtotal: I18nManager.isRTL ? 'المجموع قبل رسوم الخدمة' : 'Subtotal',
  deliveryCh: I18nManager.isRTL ? 'رسوم التوصيل' : 'Delivery Charge',
  serviceCh: I18nManager.isRTL ? 'تكلفة الخدمة' : 'Service Charge',
  total: I18nManager.isRTL ? 'المجموع' : 'Total',
  vat: I18nManager.isRTL ? 'ضريبة القيمة المضافة' : 'VAT',
  discount: I18nManager.isRTL ? 'Discount' : 'Discount',
  total: I18nManager.isRTL ? 'المجموع' : 'Total',
  summary: I18nManager.isRTL ? 'تفاصيل الطلب' : 'Order Summary',
  supp:I18nManager.isRTL ? 'SUPP001 (1 item)' : 'SUPP001 (1 item)',
  id:I18nManager.isRTL ? 'ORDER ID' : 'ORDER ID',
  drid:I18nManager.isRTL ? 'REQUEST ID' : 'REQUEST ID',
  subordid:I18nManager.isRTL ? 'SUB ORDER ID' : 'SUB ORDER ID',
  call: I18nManager.isRTL ? 'اتصل الآن' : 'Call Now',
  Delivery: I18nManager.isRTL ? 'عنوان التوصيل' : 'Delivery Address',
  Billing: I18nManager.isRTL ? 'عنوان وصول الفواتير' : 'Billing Address',
  view:I18nManager.isRTL ? 'View Location' : 'View Location',
  location:I18nManager.isRTL ? 'LOCATION' : 'LOCATION',
  locdetails:I18nManager.isRTL ? 'DETAILS' : 'DETAILS',
  online: I18nManager.isRTL ? 'Online Payment' : 'Online Payment',
  cod: I18nManager.isRTL ? 'Cash on delivery' : 'Cash on delivery',
  payment: I18nManager.isRTL ? 'Payment method' : 'Payment method',
  markCollected:I18nManager.isRTL ? 'MARK AS COLLECTED' : 'MARK AS COLLECTED',
  markDelivered:I18nManager.isRTL ? 'MARK AS DELIVERED' : 'MARK AS DELIVERED',
  name:I18nManager.isRTL ? 'Customer name' : 'Customer name',
  codfee:I18nManager.isRTL ? 'Cod Fee' : 'Cod Fee',
  },

  
  EDIT: {
    editPro: I18nManager.isRTL ? 'تعديل الملف الشخصي' : 'Edit profile',
    name: I18nManager.isRTL ? 'الاسم' : 'Name',
    logout: I18nManager.isRTL ? 'تسجيل الخروج' : 'Logout',
    phone: I18nManager.isRTL ? 'رقم الهاتف' : 'Phone Number',
    email: I18nManager.isRTL ? 'عنوان البريد الإلكتروني' : 'Email Address',
    birth: I18nManager.isRTL ? 'تاريخ الميلاد' : 'Birth Date',
    male: I18nManager.isRTL ? 'ذكر' : 'Male',
    female: I18nManager.isRTL ? 'أنثى' : 'Female',
    save: I18nManager.isRTL ? 'حفظ' : 'Save',
    default: I18nManager.isRTL ? 'الأساسي' : 'Default',
  },
  //LogoutModal
  LOGOUT: {
    sure: I18nManager.isRTL
      ? 'هل أنت متأكد أنك تريد تسجيل الخروج؟'
      : 'Are you sure you want to log out?',
    cancel: I18nManager.isRTL ? 'إلغاء' : 'Cancel',
    logout: I18nManager.isRTL ? 'تسجيل الخروج' : 'Logout',
  },
  SUPPORT:
  {
    send:I18nManager.isRTL ? 'Send' : 'Send',
    message:I18nManager.isRTL ? 'Message' : 'Message',
    subject:I18nManager.isRTL ? 'Subject' : 'Subject',
    help: I18nManager.isRTL ? 'نحن هنا للمساعدة' : 'We are here to help',
  },
  ORDERCARD:
  {
    location:I18nManager.isRTL ? 'View location' : 'View location',
    callNow:I18nManager.isRTL ? 'Call Now' : 'Call Now',
    cname:I18nManager.isRTL ? 'Customer name' : 'Customer name',
    
  },
  CHAT:{
    Type: I18nManager.isRTL ? 'الرد على المسؤول' : 'Reply to admin',
    Send: I18nManager.isRTL ? 'سال' : 'SEND',
    placeholder: I18nManager.isRTL ? 'اكتب شيئا' : 'Type something',
  },
  NOTIFY: {
    heading: I18nManager.isRTL ? 'إشعارات' : 'Notifications',
    content1: I18nManager.isRTL
      ? 'Delivery executive Ahmed is assigned for your order KIS-1001'
      : 'Delivery executive Ahmed is assigned for your order KIS-1001',
    content2: I18nManager.isRTL
      ? 'Delivery date of your order KIS-1001 is set at 21/01/2021 10AM to 12PM'
      : 'Delivery date of your order KIS-1001 is set at 21/01/2021 10AM to 12PM',
    seedetails: I18nManager.isRTL
      ? 'انقر لرؤية التفاصيل'
      : 'Click to see details',
    hrs: I18nManager.isRTL ? 'قبل ساعة' : ' hrs ago',
    done_button: I18nManager.isRTL ? 'تم' : 'Done',
  },


};
