import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import globals from '../lib/globals';
import appTexts from '../lib/appTexts';

import SplashScreen from '../containers/SplashScreen';
import AppIntrosliderloginScreen from '../containers/AppIntrosliderloginScreen';
import LoginScreen from '../containers/LoginScreen';
import OtpScreen from '../containers/OtpScreen';
import HomeScreen from '../containers/HomeScreen';
import ScanScreen from '../containers/ScanScreen';
import FavouritesScreen from '../containers/FavouritesScreen';
import MyOrdersScreen from '../containers/MyOrdersScreen';
import ProfileScreen from '../containers/ProfileScreen';
import FooterTabItem from '../components/FooterTabItem';
import ChooseLanguageScreen from '../containers/ChooseLanguageScreen';
import OrderDetailsScreen from '../containers/OrderDetailsScreen';
import NotificationScreen from '../containers/NotificationScreen';
import FaqScreen from '../containers/FaqScreen';
import PrivacyScreen from '../containers/PrivacyScreen';
import TermsScreen from '../containers/TermsScreen';
import EditProfileScreen from '../containers/EditProfileScreen';
import ChatScreen from '../containers/ChatScreen';
//Login stacks for screen navigation
const LoginStack = createStackNavigator();
const LoginStackNavigator = () => {
  return (
    <LoginStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <LoginStack.Screen
        name="AppIntrosliderloginScreen"
        component={AppIntrosliderloginScreen}
      />
      <LoginStack.Screen name="LoginScreen" component={LoginScreen} />
      <LoginStack.Screen name="OtpScreen" component={OtpScreen} />
      
     
    </LoginStack.Navigator>
  );
};

//Language stacks for screen navigation
const LanguageStack = createStackNavigator();
const LanguageStackNavigator = () => {
  return (
    <LanguageStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <LanguageStack.Screen
        name="ChooseLanguageScreen"
        component={ChooseLanguageScreen}
      />
    </LanguageStack.Navigator>
  );
};

//Splash stacks for screen navigation
const SplashStack = createStackNavigator();
const SplashStackNavigator = () => {
  return (
    <SplashStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <SplashStack.Screen name="SplashScreen" component={SplashScreen} />
    </SplashStack.Navigator>
  );
};

// HomeTab stacks for screen navigation
const HomeStack = createStackNavigator();
const HomeTab = () => {
  return (
    <HomeStack.Navigator screenOptions={{headerShown: false}}>
      <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
      
      
    </HomeStack.Navigator>
  );
};

// ProductsTab stacks for screen navigation
const ProductsScreenStack = createStackNavigator();
const ProductsTab = () => {
  return (
    <ProductsScreenStack.Navigator screenOptions={{headerShown: false}}>
      <ProductsScreenStack.Screen
        name="ScanScreen"
        component={ScanScreen}
      />
    </ProductsScreenStack.Navigator>
  );
};

// FavouriteTab stacks for screen navigation
const FavouritesScreenStack = createStackNavigator();
const FavouriteTab = () => {
  return (
    <FavouritesScreenStack.Navigator screenOptions={{headerShown: false}}>
      <FavouritesScreenStack.Screen
        name="FavouritesScreen"
        component={FavouritesScreen}
      />
    </FavouritesScreenStack.Navigator>
  );
};

// MyOrdersTab stacks for screen navigation
const MyOrdersScreenStack = createStackNavigator();
const MyOrdersTab = () => {
  return (
    <MyOrdersScreenStack.Navigator screenOptions={{headerShown: false}}>
      <MyOrdersScreenStack.Screen
        name="MyOrdersScreen"
        component={MyOrdersScreen}
      />
    </MyOrdersScreenStack.Navigator>
  );
};

// ProfileTab stacks for screen navigation
const ProfileScreenStack = createStackNavigator();
const ProfileTab = () => {
  return (
    <ProfileScreenStack.Navigator screenOptions={{headerShown: false}}>
      <ProfileScreenStack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
      />
     
    </ProfileScreenStack.Navigator>
  );
};

//Initialize bottom tab navigator
const DashboardTabRoutes = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <DashboardTabRoutes.Navigator tabBarOptions={{showLabel: false,
      style: {
				height: globals.INTEGER.footerTabBarHeight,
				borderWidth: 0,
				borderTopLeftRadius: 20,
				borderTopRightRadius: 20,
				borderTopWidth: 0.5,
				borderRightWidth: 0.5,
				borderLeftWidth: 0.5,
				backgroundColor: 'white',
				borderColor: "#ccc",
				shadowColor: "#000",
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0.2,
				shadowRadius: 1,
				elevation: 1,
			}
    }}>
      <DashboardTabRoutes.Screen
        name="HomeScreen"
        component={HomeTab}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => (
            <FooterTabItem
              tabBarIndex={0}
              tabBarLabel={appTexts.STRING.home}
              isFocused={focused}
            />
          ),
        }}
      />
      <DashboardTabRoutes.Screen
        name="Products"
        component={ProductsTab}
        options={{
          tabBarLabel: 'Products',
          tabBarIcon: ({focused}) => (
            <FooterTabItem
              tabBarIndex={2}
              tabBarLabel={appTexts.STRING.products}
              isFocused={focused}
            />
          ),
        }}
      />
      <DashboardTabRoutes.Screen
        name="Favourites"
        component={FavouriteTab}
        options={{
          tabBarLabel: 'Favourites',
          tabBarIcon: ({focused}) => (
            <FooterTabItem
              tabBarIndex={1}
              tabBarLabel={appTexts.STRING.favourites}
              isFocused={focused}
            />
          ),
        }}
      />
      {/* <DashboardTabRoutes.Screen
        name="MyOrders"
        component={MyOrdersTab}
        options={{
          tabBarLabel: 'My Orders',
          tabBarIcon: ({focused}) => (
            <FooterTabItem
              tabBarIndex={3}
              tabBarLabel={appTexts.STRING.myOrders}
              isFocused={focused}
            />
          ),
        }}
      />
      <DashboardTabRoutes.Screen
        name="MyProfile"
        component={ProfileTab}
        options={{
          tabBarLabel: 'My Profile',
          tabBarIcon: ({focused}) => (
            <FooterTabItem
              tabBarIndex={4}
              tabBarLabel={appTexts.STRING.myProfile}
              isFocused={focused}
            />
          ),
        }}
      /> */}
    </DashboardTabRoutes.Navigator>
  );
};

//Main stacks for screen navigation
const MainScreenStack = createStackNavigator();
const MainScreenStackNavigator = () => {
  return (
    <MainScreenStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <MainScreenStack.Screen
        name="SplashStackNavigator"
        component={SplashStackNavigator}
      />
      <MainScreenStack.Screen
        name="LangStackNavigator"
        component={LanguageStackNavigator}
      />
      <MainScreenStack.Screen
        name="LoginStackNavigator"
        component={LoginStackNavigator}
      />
      <MainScreenStack.Screen
        name="TabNavigator"
        component={TabNavigator}
        options={{gestureEnabled: false}}
      />
        <MainScreenStack.Screen name="FaqScreen" component={FaqScreen} />
        <MainScreenStack.Screen name="PrivacyScreen" component={PrivacyScreen} />
        <MainScreenStack.Screen name="TermsScreen" component={TermsScreen} />
        <MainScreenStack.Screen name="NotificationScreen" component={NotificationScreen} />
        <MainScreenStack.Screen name="ChatScreen" component={ChatScreen} />
        <MainScreenStack.Screen name="OrderDetailsScreen" component={OrderDetailsScreen} />
        <MainScreenStack.Screen name="ProfileScreen" component={ProfileScreen} />
      
      <MainScreenStack.Screen
        name="EditProfileScreen"
        component={EditProfileScreen}
      />
       
    </MainScreenStack.Navigator>
  );
};

export const LanguageContainer = () => {
  return (
    <NavigationContainer>
      <LanguageStackNavigator />
    </NavigationContainer>
  );
};

export const LoginContainer = () => {
  return (
    <NavigationContainer>
      <LoginStackNavigator />
    </NavigationContainer>
  );
};

export const HomeContainer = () => {
  return (
    <NavigationContainer>
      <MainScreenStackNavigator />
    </NavigationContainer>
  );
};
