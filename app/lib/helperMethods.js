import _ from 'lodash'
import { I18nManager } from "react-native";


export const collectionList = (data) => {
    console.log("************", data)
    return _.map(data, item => {
        return {
            order_id: _.get(item, ['ord_id'], '01'),
            order_status:  _.get(item, ['order_status'], 'GOOD'),
            time_slot:  _.get(item, ['time_slot'], '12 - 1'),
            collection_date:  _.get(item, ['collection_date'], '22/12/2021'),

            // image: I18nManager.isRTL ?
            // _.get(item, ['lang', 'ar', 'image'], '') :
            // _.get(item, ['lang', 'en', 'image'], ''),

}
    })
} 

export const howtouseDetailsMapper = (data) => {
    const lang = I18nManager.isRTL ? "ar" : "en";
    const finalData = data.filter(function(item) {
      if (_.get(item, ["lang", lang, "content"], null) == null) {
          return false;
        }
      return true;
    }).map(function(item) {
      return {
          title: I18nManager.isRTL
            ? _.get(item, ["lang", "ar", "title"], "")
            : _.get(item, ["lang", "en", "title"], ""),
    
          text: I18nManager.isRTL
            ? _.get(item, ["lang", "ar", "content"], "")
            : _.get(item, ["lang", "en", "content"], ""),
    
          image: I18nManager.isRTL
            ? _.get(item, ["lang", "ar", "image"], "")
            : _.get(item, ["lang", "en", "image"], ""),
        };
     });
  
     return finalData;
  };
