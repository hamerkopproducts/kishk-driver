import globals from './globals';

const under_dev = false;   
let productionURL = 'https://kishk.app/';
// let productionURL = 'https://demo.kishk.app/';
let developmentURL = 'https://kishk.ourdemo.online/';
//let developmentURL = 'https://kiskdev.ourdemo.online/';
//let baseURL = productionURL;
let baseURL = under_dev ? developmentURL : productionURL;   

let getOtpAPI = 'api/driver/login';
let getfaqAPI = 'api/driver/faq';
let getPrivacyAPI = 'api/driver/privacy-policies';
let getTermsAPI = 'api/driver/terms-conditions';
let loginAPI = 'api/driver/verify';
let requestSupportApi = 'api/driver/support';
let getEditUserAPI = 'api/driver/my-details';
let getCollection = 'api/driver/order-collection';
let getRequestCollection = 'api/driver/request-collection';
let getRequestCollectionDetails = 'api/driver/request-collection/details/';
let getOrders = 'api/driver/order-delivery/active';
let collectOrder = 'api/driver/order-collection/status-update';
let collectRequest = 'api/driver/request-collection/mark-as-collected';
let collectDeliveredRequest = 'api/driver/request-delivery/mark-as-delivered';
let deliverOrder = 'api/driver/order-delivery/mark-as-delivered';
let getDeliveredOrder = 'api/driver/order-delivery/delivered';
let getLogoutAPI = 'api/driver/logout';
let resendAPI = 'api/driver/resend';
let notificationCount = 'api/driver/notification/count';
let getOrderDetails = 'api/driver/order-collection/details/';
let getDeliveredOrderDetails = 'api/driver/order-delivery/details/';
let howToUse = 'api/driver/useapp';
let getRequestDelivered = '';
let getActiveRequests = 'api/driver/request-delivery/active';
let getDeliveredRequests = 'api/driver/request-delivery/delivered';
let getRequestDeliveredDetails = 'api/driver/request-delivery/details/';

const apiHelper = {
  getBaseUrl: () => {
    return baseURL;
  },

  getAPIHeaderForDelete: (params, token) => {
    const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthStr,
        Accept: 'application/json',
      },
      data: params,
    };
    return headerConfig;
  },

  getAPIHeader: token => {
    const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    };
    return headerConfig;
  },

  getLoginAPIHeader: () => {
    let headerConfig = {
      headers: {'Content-Type': 'application/json', Accept: 'application/json'},
    };
    return headerConfig;
  },
  loginAPI: () => {
    return baseURL + loginAPI;
  },
  getUserEditDetailsAPI: () => {
    return baseURL + getEditUserAPI;
  },
  getOtpAPI: () => {
    return baseURL + getOtpAPI;
  },
  resendOtpAPI: () => {
    return baseURL + resendAPI;
  },
  getNotificationCount: () => {
    return baseURL + notificationCount;
  },
  getFaqsAPI: () => {
    return baseURL + getfaqAPI;
  },
  getPrivacyPolicyAPI: () => {
    return baseURL + getPrivacyAPI;
  },
  getTermsAndConditionAPI: () => {
    return baseURL + getTermsAPI;
  },
  getHowToUseAPI: () => {
    return baseURL + howToUse;
  },
  RequestSupportApi: () => {
    return baseURL + requestSupportApi;
  },
  getTodaysCollection: (params) => {
    return baseURL + getCollection + "?date=" + params.date ;
  },
  getTodaysRequestCollection: (params) => {
    return baseURL + getRequestCollection + "?date=" + params.date ;
  },
  getTodaysRequest: (params) => {
    // console.log("FULL URL", baseURL + getActiveRequests + "?date=" + params.date)
    return baseURL + getActiveRequests + "?date=" + params.date ;
  },
  getAllRequestCollection: () => {
    return baseURL + getRequestCollection 
  },
  getAllCollection: () => {
    return baseURL + getCollection 
  },
  getTodaysOrders: (params) => {
    return baseURL + getOrders + "?date=" + params.date ;
  },
  getActiveOrders: () => {
    return baseURL + getOrders 
  },
  getActiveRequest: () => {
    return baseURL + getActiveRequests
  },
  getDeliveredRequest: () => {
    return baseURL + getDeliveredRequests
  },
  getDeliveredOrders: () => {
    return baseURL + getDeliveredOrder
  },
  getOrdersDetails: (id, flag) => {
    let suburl = flag ? getOrderDetails : getDeliveredOrderDetails;
    return baseURL +  suburl + id ;
  },
  getRequestDetails: (id, flag) => {
    let suburl = flag ? getRequestCollectionDetails : getRequestDeliveredDetails;
    return baseURL + suburl  + id ;
  },
  setcollectOrder: () => {
    return baseURL +  collectOrder ;
  },
  setcollectRequest: (flag) => {
    let suburl = flag ? collectRequest : collectDeliveredRequest;
    return baseURL +  suburl ;
  },
  setDeliverOrder: () => {
    // console.log("FULL URL", baseURL +  deliverOrder )
    return baseURL +  deliverOrder ;
  },



  logoutApi: () => {
    return baseURL + getLogoutAPI;
  },
  getAPIHeaderMultipart: token => {
    const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    };
    return headerConfig;
  },
};

export default apiHelper;
