import React from 'react';
import { View, Text } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

const ProductsView = (props) => {
	const {
		
	} = props;

	return (
	
				<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
					<View style={styles.screenContainer}>
						<Text>{appTexts.STRING.productsview}</Text>
					</View>
				</View>
			

	);
};

ProductsView.propTypes = {
	
};

export default ProductsView;
