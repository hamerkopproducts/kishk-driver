import {StyleSheet, I18nManager} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globals from '../../lib/globals';

const images = {driverImage: require("../../assets/images/SplashScreen/Splash.png"),};
const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
  },

  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    //marginBottom:'5%'
  },

  logoi: {
    width: '100%',
    height: '100%',
    // flex:1,
    resizeMode: 'stretch',
  },
});

export {images, styles};
