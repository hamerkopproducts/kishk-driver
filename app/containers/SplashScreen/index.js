import React, {Component} from 'react';
import {View, Image, StatusBar} from 'react-native';
import {styles, images} from './styles';
import globals from '../../lib/globals';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as LoginActions from '../../actions/LoginActions';
class Splash extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => {
      if (
        this.props.isLogged === true 
      ) {
       
        this.props.navigation.navigate('TabNavigator', {screen: 'HomeScreen'});
      }  else {
        this.setState({loaded: true});
       this.props.navigation.navigate('LoginStackNavigator');
      }
    }, 2000);
  }


  render() {
    return (
      <View style={styles.screenMain}>
        <StatusBar
          barStyle="dark-content"
          hidden={true}
          backgroundColor={'transparent'}
          translucent={true}
        />
        <View style={styles.imageContainer}>
          <Image source={images.driverImage} style={styles.logoi} />
        </View>
      </View>
    );
  }
}

// Splash.navigationOptions = ({navigation}) => ({
//   header: null,
// });

// export default Splash;

const mapStateToProps = (state, props) => {
  return {
    isLogged: state.loginReducer.isLogged,
    selectedLanguage: state.loginReducer.chosenLanguage,
    guestLogin: state.loginReducer.guestLogin,
    is_intro_finished: state.loginReducer.intro_finished,
    is_guest_logged_in: state.loginReducer.is_guest_logged_in,
    smsStatus: state?.loginReducer?.smsstatus || null,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      smsStatusResponse: LoginActions.getSmsStatus,
      resetSmsStatus: LoginActions.resetSmsStatus,
    },
    dispatch,
  );
};

const splashWithRedux = connect(mapStateToProps, mapDispatchToProps)(Splash);

splashWithRedux.navigationOptions = ({navigation}) => ({
  header: null,
});

export default splashWithRedux;
