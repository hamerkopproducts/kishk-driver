import React, {useEffect, useState} from 'react';
import {I18nManager} from 'react-native';
import {connect} from 'react-redux';
import ProfileView from './ProfileView';
import {bindActionCreators} from 'redux';
import appTexts from '../../lib/appTexts';
import functions from '../../lib/functions';
import RNRestart from 'react-native-restart';
import * as notifyActions from '../../actions/notifyActions';
import _ from 'lodash';
import * as editUserActions from '../../actions/editUserActions';
import NetInfo from '@react-native-community/netinfo';
import {BackHandler, ToastAndroid} from 'react-native';

const ProfileScreen = props => {
  //Initialising states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isBoxModalVisible, setIsBoxModalVisible] = useState(false);
  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
  const [isLogoutModalVisible, setIsLogoutModalVisible] = useState(false);
  const [isContactModalVisible, setIsContactModalVisible] = useState(false);
  const [isLanguageModalVisible, setIsLanguageModalVisible] = useState(false);
  const [isWishlistModalVisible, setIsWishlistModalVisibile] = useState(false);
  const [isRateModalVisible, setIsRateModalVisible] = useState(false);
  const [isPriceModalVisible, setIsPriceModalVisible] = useState(false);
  const [isSelectLanguageModalVisible, setIsSelectLanguageModalVisibile] =
    useState(false);
  const [isCancelModalVisible, setIsCancelModalVisibile] = useState(false);
  const [isSupportModalVisible, setIsSupportModalVisibile] = useState(false);

  

  /** will focus */
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      props.editUserData(props.token);
      props.getNotificationCount(props.token);
    });
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (backPressed > 0) {
        BackHandler.exitApp();
        backPressed = 0;
      } else {
        backPressed++;
        ToastAndroid.show('Please tap again to exit', ToastAndroid.SHORT);

        setTimeout(() => {
          backPressed = 0;
        }, 2000);
        return true;
      }
    });

    return () => {
      unsubscribe();
    };
    
  }, [props.navigation, props.token]);

  //mounted
  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
   
    //foregroundState();
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.editUserData(props.token);
        //console.log('hAI');
      } else {
        functions.displayAlert(
          null,
          appTexts.ALERT_MESSAGES.noInternet,
          'Profile',
        );
      }
    });
    // BackHandler.addEventListener(
    //   'hardwareBackPress',
    //   (backPressed = () => {
    //     BackHandler.exitApp();
    //     return true;
    //   }),
    // );
  };
  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => {
    if (props.loggedoutData?.success) {
      handleComponentUpdated();
    }
  }, [props.loggedoutData?.success]);

  const handleComponentUpdated = () => {
    console.log('sucess::' + props.loggedoutData.success);
    functions.displayToast(
      'success',
      'top',
      appTexts.LOGIN_TOASTMESSAGES.logout,
      "Logout successful"
      //_.get(props, 'loggedoutData.msg') || props.logoutError?.msg,
    );
    props.resetLogoutError();
    props.resetLogoutData();
    props.navigation.navigate('LoginStackNavigator', {
      screen: 'AppIntrosliderloginScreen',
    });

    props.resetLogoutError();
    props.resetLogoutData();
  };

  const onBackButtonClick = () => {
    props.navigation.navigate('HomeScreen');
  };
  const onNotificationButtonPress = () => {
    props.navigation.navigate('NotificationScreen');
  };
  const faqPress = () => {
    props.navigation.navigate('FaqScreen');
  };
  const termsPress = () => {
    props.navigation.navigate('TermsScreen');
  };
  const openSupportModal = () => {
    setIsSupportModalVisibile(!isSupportModalVisible);
  };

  const supportCloseModal = () => {
    setIsSupportModalVisibile(!isSupportModalVisible);
  };

  const logoutButtonPress = () => {
    //props.logOutPressed(props.token)
    // props.navigation.navigate('LoginScreen');
    props.navigation.navigate('LoginStackNavigator', {screen: 'LoginScreen'});
    //toggleLogoutModal()
  };

  const openSelectLanguageModal = () => {
    setIsSelectLanguageModalVisibile(!isSelectLanguageModalVisible);
  };

  const SelectLanguagecloseModal = () => {
    setIsSelectLanguageModalVisibile(!isSelectLanguageModalVisible);
  };

  const openCancelModal = () => {
    setIsCancelModalVisibile(!isCancelModalVisible);
  };

  const cancelCloseModal = () => {
    setIsCancelModalVisibile(!isCancelModalVisible);
  };

  const toggleLogoutModal = () => {
    setIsLogoutModalVisible(false);
  };
  const openLogoutModal = () => {
    setIsBoxModalVisible(false);
    setIsLogoutModalVisible(!isLogoutModalVisible);
  };
  const privacyPolicyPress = () => {
    // alert('Privacy clicked');
    props.navigation.navigate('PrivacyScreen');
  };
  const orderPress = () => {
    props.navigation.navigate('ActiveDeliverScreen');
    // props.navigation.navigate('CheckoutScreen1');
    //alert('as');
  };
  const onLogoutClick = () => {
    props.logOutPressed(props.token);
    toggleLogoutModal();
  };

  console.log('logout pressed999909899089', props.loggedoutData);
  const onCancelClick = () => {
    toggleLogoutModal();
  };

  const wishlistPress = () => {
    props.navigation.navigate('WishlistScreen');
    // props.navigation.navigate('CheckoutScreen1');
    //alert('as');
  };
  const openWishlistModal = () => {
    setIsWishlistModalVisibile(!isWishlistModalVisible);
  };

  const openrateModal = () => {
    //alert('jjj');
    setIsRateModalVisible(!isRateModalVisible);
  };

  const openpriceModal = () => {
    setIsPriceModalVisible(!isPriceModalVisible);
  };
  const pricecloseModal = () => {
    setIsPriceModalVisible(!setIsPriceModalVisible);
  };

  const closeModal = () => {
    setIsWishlistModalVisibile(!isWishlistModalVisible);
  };

  const ratecloseModal = () => {
    setIsRateModalVisible(!setIsRateModalVisible);
  };
  const addressPress = () => {
    //alert('hai');
    props.navigation.navigate('AddressScreen');
  };
  const termsAndConditionPress = () => {
    props.navigation.navigate('TermsScreen');
  };
  const openHelpModal = () => {
    setIsHelpModalVisible(true);
  };
  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible);
  };
  const openBoxModal = () => {
    setIsBoxModalVisible(!isBoxModalVisible);
  };
  const toggleModal = () => {
    setIsModalVisible(false);
  };
  const openContactModal = () => {
    setIsContactModalVisible(true);
  };
  const toggleContactModal = () => {
    setIsContactModalVisible(false);
  };
  const openLanguageModal = () => {
    setIsLanguageModalVisible(true);
  };
  const toggleLanguageModal = () => {
    setIsLanguageModalVisible(false);
  };
  const onClosePress = () => {
    setIsBoxModalVisible(false);
  };

  const onEditClick = () => {
    setIsBoxModalVisible(false);
    props.navigation.navigate('EditProfileScreen');
  };
  const selectLanguage = selectedLanguage => {
    if (selectedLanguage === 'EN') {
      I18nManager.forceRTL(false);
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    } else if (selectedLanguage === 'AR') {
      I18nManager.forceRTL(true);
      setTimeout(() => {
        RNRestart.Restart();
        props.saveSelectedLanguage(selectedLanguage);
      }, 500);
    }
    props.saveSelectedLanguage(selectedLanguage);
  };
  return (
    <ProfileView
      logoutButtonPress={logoutButtonPress}
      isLogoutModalVisible={isLogoutModalVisible}
      openLogoutModal={openLogoutModal}
      onEditClick={onEditClick}
      toggleLogoutModal={toggleLogoutModal}
      selectLanguage={selectLanguage}
      onBackButtonClick={onBackButtonClick}
      onNotificationButtonPress={onNotificationButtonPress}
      privacyPolicyPress={privacyPolicyPress}
      orderPress={orderPress}
      addressPress={addressPress}
      faqPress={faqPress}
      termsAndConditionPress={termsAndConditionPress}
      toggleHelpModal={toggleHelpModal}
      isModalVisible={isModalVisible}
      isHelpModalVisible={isHelpModalVisible}
      openHelpModal={openHelpModal}
      toggleModal={toggleModal}
      openBoxModal={openBoxModal}
      isBoxModalVisible={isBoxModalVisible}
      isContactModalVisible={isContactModalVisible}
      openContactModal={openContactModal}
      toggleContactModal={toggleContactModal}
      openLanguageModal={openLanguageModal}
      toggleLanguageModal={toggleLanguageModal}
      isLanguageModalVisible={isLanguageModalVisible}
      openSupportModal={openSupportModal}
      supportCloseModal={supportCloseModal}
      isSupportModalVisible={isSupportModalVisible}
      userdetails={props.editUserDetails}
      languageSelected={props.languageSelected}
      openWishlistModal={openWishlistModal}
      closeModal={closeModal}
      isWishlistModalVisible={isWishlistModalVisible}
      openrateModal={openrateModal}
      isRateModalVisible={isRateModalVisible}
      ratecloseModal={ratecloseModal}
      openpriceModal={openpriceModal}
      isPriceModalVisible={isPriceModalVisible}
      pricecloseModal={pricecloseModal}
      isBoxModalVisible={isBoxModalVisible}
      onClosePress={onClosePress}
      openSelectLanguageModal={openSelectLanguageModal}
      SelectLanguagecloseModal={SelectLanguagecloseModal}
      isSelectLanguageModalVisible={isSelectLanguageModalVisible}
      openCancelModal={openCancelModal}
      cancelCloseModal={cancelCloseModal}
      isCancelModalVisible={isCancelModalVisible}
      wishlistPress={wishlistPress}
      termsPress={termsPress}
      openSupportModal={openSupportModal}
      supportCloseModal={supportCloseModal}
      isSupportModalVisible={isSupportModalVisible}
      userdetails={props.editUserDetails}
      isLoading={props.isLoading}
      onCancelClick={onCancelClick}
      onLogoutClick={onLogoutClick}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token', null),
    editUserDetails: _.get(state, 'editUserReducer.editUserData', ''),
    isLoading: _.get(state, 'editUserReducer.isLoading', ''),
    loggedoutData: _.get(state, 'editUserReducer.logoutData', ''),
    logoutError: state.editUserReducer.logoutError,
    loggedoutData: _.get(state, 'loginReducer.logoutData', ''),
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      editUserData: editUserActions.doUserDetailsEdit,
      logOutPressed: editUserActions.doLogout,
      resetLogoutError: editUserActions.resetLogoutError,
      resetLogoutData: editUserActions.resetLogoutData,
      getNotificationCount: notifyActions.getNotificationCount,
    },
    dispatch,
  );
};

const ProfileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);

ProfileScreenWithRedux.navigationOptions = ({navigation}) => ({
  header: null,
});

export default ProfileScreenWithRedux;
