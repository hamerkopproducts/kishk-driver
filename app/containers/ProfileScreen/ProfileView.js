import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity, Image, ScrollView, TouchableHighlight} from 'react-native';
import globals from '../../lib/globals';
import {styles, images} from './styles';
import appTexts from '../../lib/appTexts';
import ProfileItem from './ProfileItem';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Modal from 'react-native-modal';
import Header from '../../components/Header';
// import RateorderModal from '../../components/RateorderModal';
// import RateitemModal from '../../components/RateitemModal';
// import PriceModal from '../../components/PriceModal';

// import CancelModal from '../../components/CancelModal';
// import SupportModal from '../../components/SupportModal';
// import LogoutModal from '../../components/LogoutModal';
import SelectLanguageModal from '../../components/SelectLanguageModal';
import Loader from '../../components/Loader';
import LogoutModal from '../../components/LogoutModal';
import SupportModal from '../../components/SupportModal';
import FastImageLoader from '../../components/FastImage/FastImage';

import _ from 'lodash';
import ModalComponent from '../../components/ModalComponent';
// import Modal from 'react-native-modal';

const ProfileView = props => {
  const {
    logoutButtonPress,
    
    orderPress,
    addressPress,
    privacyPolicyPress,
    termsAndConditionPress,
    toggleHelpModal,
    toggleModal,
    toggleLogoutModal,
    openBoxModal,
    isBoxModalVisible,
    isModalVisible,
    isHelpModalVisible,
    openHelpModal,
    isLogoutModalVisible,
    isContactModalVisible,
    toggleContactModal,
    faqPress,
    openContactModal,
    openLanguageModal,
    openLogoutModal,
    toggleLanguageModal,
    isLanguageModalVisible,
    selectLanguage,
    userdetails,
    onBackButtonClick,
    onRightButtonPress,
    languageSelected,
    openWishlistModal,
    closeModal,
    isWishlistModalVisible,
    isRateModalVisible,
    isPriceModalVisible,
    openrateModal,
    ratecloseModal,
    openpriceModal,
    pricecloseModal,
    onEditClick,
    openSelectLanguageModal,
    SelectLanguagecloseModal,
    isSelectLanguageModalVisible,
    onClosePress,
    privacyPress,
    isCancelModalVisible,
    cancelCloseModal,
    openCancelModal,
    isSupport,
    wishlistPress,
    termsPress,
    openSupportModal,
    supportCloseModal,
    isSupportModalVisible,
    onNotificationButtonPress,
    isNotificationButtonRequired,
    isLoading,
    onCancelClick,
    onLogoutClick

  } = props;
  const pro_pic = _.get(userdetails, 'data.photo', null);
  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        iscenterLogoRequired={false}
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        isNotificationButtonRequired={true}
        onNotificationButtonPress={onNotificationButtonPress}
        isProfileScreen={true}
        isCentreTitleRequired={true}
        centerText={appTexts.PROFILE.Head}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
  {isLoading && <Loader />}
      <View style={styles.screenContainer}>
        <View style={styles.lineBorder} />
        <ScrollView>
          <View style={styles.formWrapper}>
            <View style={styles.contentWrapper}>
              <View style={styles.profileWrapper}>
                <View style={styles.editSection}>
                  {/* <View style={styles.imageSection}>
                  <Image
                        source={require('../../assets/images/Profile/users.png')}
                        style={styles.logo}
                      />
                  </View> */}
                  <View style={styles.imageSection}>
                    {pro_pic == null && (
                      <Image
                        source={require('../../assets/images/Profile/users.png')}
                        style={styles.logo}
                      />
                    )}
                    {pro_pic != null && (
                      <FastImageLoader
                        resizeMode={'cover'}
                        photoURL={pro_pic}
                        style={styles.logo}
                        loaderStyle={{}}
                      />
                    )}
                  </View>
                  <View style={styles.detailsWrapper}>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={styles.sName}>Hi,</Text>
                      <Text style={styles.fName}> {
                          _.get(userdetails, 'data.name', '')}</Text>
                      
                    </View>
                    <View style={styles.phoneLine}>
                      <View style={styles.phoneIcon}>
                        <Image
                          source={images.mailIcon}
                          style={styles.mailImage}
                        />
                      </View>
                      <Text style={styles.Phone}> {
                          _.get(userdetails, 'data.email', '')}</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        openContactModal();
                      }}>
                      <View style={styles.emailLine}>
                        <View style={styles.emailIcon}>
                          <Image
                            source={images.phoneIcon}
                            style={styles.phoneImage}
                          />
                        </View>
                        <Text style={styles.Phone}>+966   {
                          _.get(userdetails, 'data.phone', '')}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.editButton}>
                  <TouchableOpacity
                    onPress={() => {
                      openBoxModal();
                    }}>
                    <Image source={images.action} style={styles.edit} />
                    {isBoxModalVisible ? (
                      <Image source={images.action} style={styles.edit} />
                    ) : null}
                    {/* <Text>sss</Text> */}
                  </TouchableOpacity>
                </View>
              </View>

              <View style={styles.mainList}>
                <View style={styles.firstprofileWrapper}>


                  <View style={styles.insidecontentWrapper}>
                   
                 
               
                  
                    <View style={styles.firstprofileWrapper}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.accountName}>
                          {appTexts.PROFILE.Settings}
                        </Text>
                      </View>
                      <View style={styles.insidecontentWrapper}>
                        {/* <View style={styles.marginFity}>
                          <ProfileItem
                            itemText={appTexts.PROFILE.Change}
                            itemImage={images.langIcon}
                            languageText={'English'}
                            arrowImage={images.arrowIcon}
                            onItemClick={openSelectLanguageModal}
                          />
                          <View style={styles.borderStyle} />
                        </View> */}
                        <View style={styles.marginFity}>
                          <ProfileItem
                            itemText={appTexts.PROFILE.Support}
                            itemImage={images.supportIcon}
                            arrowImage={images.arrowIcon}
                            onItemClick={openSupportModal}
                          />
                        </View>
                        <View style={styles.borderStyle} />
                        <View style={styles.marginFity}>
                          <ProfileItem
                            itemText={appTexts.PROFILE.FAQ}
                            itemImage={images.faqIcon}
                            arrowImage={images.arrowIcon}
                            onItemClick={faqPress}
                          />
                        </View>
                        <View style={styles.borderStyle} />
                        <View style={styles.marginFity}>
                          <ProfileItem
                            itemText={appTexts.PROFILE.Privacy}
                            itemImage={images.privacyIcon}
                            arrowImage={images.arrowIcon}
                            onItemClick={privacyPolicyPress}
                          />
                        </View>
                        <View style={styles.borderStyle} />
                        <View style={styles.marginFity}>
                          <ProfileItem
                            itemText={appTexts.PROFILE.Terms}
                            itemImage={images.termsIcon}
                            arrowImage={images.arrowIcon}
                            onItemClick={termsPress}
                          />
                        </View>
                        <Modal
        style={styles.boxModalStyle}
        isVisible={isBoxModalVisible}
        animationIn="zoomInUp"
        backdropColor={'#d2d2d2'}
        animationOut="zoomOutRight"
        onSwipeComplete={openBoxModal}
        onBackdropPress={() => openBoxModal()}
        swipeDirection={[]}>
        <View style={styles.boxWrapperView}>
             <TouchableOpacity
            onPress={() => {
              onEditClick();
            }}>
            <View style={styles.logoutText}>
              <Text style={styles.modalText}>{appTexts.EDIT.editPro}</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.line}></View>
          <TouchableOpacity
            onPress={() => {
              openLogoutModal();
            }}>
            <View style={styles.logoutText}>
              <Text style={styles.modalText}>{appTexts.EDIT.logout}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </Modal>
						<SelectLanguageModal
        isSelectLanguageModalVisible={isSelectLanguageModalVisible}
        SelectLanguagecloseModal={SelectLanguagecloseModal}
      />

<LogoutModal
        isLogoutModalVisible={isLogoutModalVisible}
        logoutButtonPress={openLogoutModal}
        onCancelClick={onCancelClick}
        onLogoutClick={onLogoutClick}
      />
      <SupportModal
        isSupportModalVisible={isSupportModalVisible}
        openSupportModal={openSupportModal}
        supportCloseModal={supportCloseModal}

      />
      
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
     

      
    </View>
  );
};

ProfileView.propTypes = {
  isModalVisible: PropTypes.bool,
  isHelpModalVisible: PropTypes.bool,
  isBoxModalVisible: PropTypes.bool,
  logoutButtonPress: PropTypes.func,
  privacyPolicyPress: PropTypes.func,
  termsAndConditionPress: PropTypes.func,
  toggleHelpModal: PropTypes.func,
  orderPress: PropTypes.func,
  toggleModal: PropTypes.func,
  toggleLogoutModal: PropTypes.func,
  selectLanguage: PropTypes.func,
  goBackButton: PropTypes.func,
  addressPress: PropTypes.func,
  openWishlistModal: PropTypes.func,
  openrateModal: PropTypes.func,
  openpriceModal: PropTypes.func,
};

export default ProfileView;
