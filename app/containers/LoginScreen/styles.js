import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const images = {

};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      backgroundColor: globals.COLOR.screenBackground
  },
  bgImage:{
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  screenMainContainer:{
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    justifyContent: 'center'
  },
  screenContainer:{
    flex:1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  }
});

export { images, styles };
