import React, { useState, useEffect } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import LoginView from './LoginView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";

const LoginScreen = (props) => {
  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
  };

  const loginButtonPress = () => {
    props.doLogin({});
    props.navigation.navigate('LoginStackNavigator' , { screen: 'OtpScreen' });
  };
  return (
    <LoginView loginButtonPress={loginButtonPress}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
  
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogin: LoginActions.doLogin
  }, dispatch)
};

const loginWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);

loginWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default loginWithRedux;
