import React from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar, Text, TouchableOpacity } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts"
import { images, styles } from "./styles";

const LoginView = (props) => {
	const {
		loginButtonPress
	} = props;
	return (
		<View style={styles.screenMain}>
			<StatusBar barStyle="dark-content"
				backgroundColor={globals.COLOR.screenBackground} />

			<View style={styles.screenMainContainer}>

				<TouchableOpacity onPress={() => { loginButtonPress() }}>
					<Text>{appTexts.STRING.login}</Text>
				</TouchableOpacity>
			</View>
		</View>
	);

};

LoginView.propTypes = {
	loginButtonPress: PropTypes.func,
};


export default LoginView;