import React from 'react';
import { View, Text } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { styles } from "./styles";

import Header from "../../components/Header";

const MyOrdersView = (props) => {
	const {
		
	} = props;

	return (
		
				<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
					<View style={styles.screenContainer}>
						<Text>{appTexts.STRING.myordersview}</Text>
					</View>
				</View>
			
	);
};

MyOrdersView.propTypes = {
	
};

export default MyOrdersView;
