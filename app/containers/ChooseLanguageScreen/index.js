import React, { useState, useEffect } from 'react';
import { BackHandler, I18nManager } from 'react-native';
import { connect } from 'react-redux';

import { bindActionCreators } from "redux";

import ChooseLanguageView from './ChooseLanguageView';

import RNRestart from 'react-native-restart';


const ChooseLanguageScreen = (props) => {
  useEffect(() => handleComponentMounted(), []);
  
  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

 
  const selectLanguage = (selectedLanguage) => {
   
    if (selectedLanguage === 'EN' && I18nManager.isRTL) {
      I18nManager.forceRTL(false);
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    } else if (selectedLanguage === 'AR' && I18nManager.isRTL == false){
      I18nManager.forceRTL(true);
      setTimeout(() => {
        RNRestart.Restart();
        
      }, 500);
     }else{
       props.navigation.navigate('LoginStackNavigator');
    }

};


  return (
    
    <ChooseLanguageView selectLanguage={selectLanguage}/>
   
  );

};

ChooseLanguageScreen.navigationOptions = ({ navigation }) => ({
  header: null
});

export default ChooseLanguageScreen;
