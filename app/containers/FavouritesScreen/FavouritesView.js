import React from 'react';
import { View, Text, TouchableOpacity, FlatList, RefreshControl } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import PropTypes from 'prop-types';
import TodaysOrderCard from '../../components/TodaysOrderCard'
import DeliveredCard from '../../components/DeliveredCard'
import Loader from '../../components/Loader';
const FavouritesView = (props) => {
	const {
		tabIndex,
		listData,
		requestlistData,
		topTabIndex,
		changeTab,
		changeTopTab,
		itemClick,
		onProfileButtonPress,
		onNotificationButtonPress,
		call,
		viewLocation,
		rfLoader,
		onRefreshList,
		isLoading,
		todayCounts,
		activeCounts,
		deliveredCounts,
		onFavouriteScreen,

	} = props;
	const renderItem = ({ item, index }) => (
		tabIndex !== 2 ? 
			<TodaysOrderCard topTabIndex={topTabIndex} viewLocation={viewLocation} call={call} item={item} tabIndex={tabIndex} itemClick={itemClick} /> :
			<DeliveredCard topTabIndex={topTabIndex} item={item} itemClick={itemClick} tabIndex={tabIndex} />

	);
	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				isProfileButtonRequired={true}
				onProfileButtonPress={onProfileButtonPress}
				isNotificationButtonRequired={true}
				onNotificationButtonPress={onNotificationButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>

			<View style={styles.screenContainer}>
				<View style={styles.lineBorder} />
				<View style={styles.containerTop}>
					<TouchableOpacity
						style={topTabIndex === 0 ? styles.myOrdersContainertop : styles.myOrdersContainerNo}
						onPress={() => {
							changeTopTab(0);
						}}>
						<Text
							style={topTabIndex === 0 ? styles.orderLabelTop : styles.inactiveLabelTop}>
							{'Order'}
						</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={topTabIndex === 1 ? styles.myOrdersContainertop : styles.myOrdersContainerNo}
						onPress={() => {
							changeTopTab(1);
						}}>
						<Text
							style={topTabIndex === 1 ? styles.orderLabelTop : styles.inactiveLabelTop}>
							{'Delivery Request'}
						</Text>

					</TouchableOpacity>
				</View>
				<View style={styles.container}>
					<TouchableOpacity
						style={styles.myOrdersContainer}
						onPress={() => {
							changeTab(0);
						}}>
						<Text
							style={tabIndex === 0 ? styles.orderLabel : styles.inactiveLabel}>
							{"Todays Order (" + todayCounts + ")"}
						</Text>
						<View
							style={
								tabIndex === 0
									? styles.underLineStyle
									: styles.transparentUnderLineStyle
							}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.myQuotationContainer}
						onPress={() => {
							changeTab(1);
						}}>
						<Text
							style={tabIndex === 1 ? styles.orderLabel : styles.inactiveLabel}>
							{"Active (" + activeCounts + ")"}
						</Text>
						<View
							style={[
								tabIndex === 1
									? styles.underLineStyle
									: styles.transparentUnderLineStyle,
								styles.underLineWidth,
							]}
						/>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.myDeliverContainer}
						onPress={() => {
							changeTab(2);
						}}>
						<Text
							style={tabIndex === 2 ? styles.orderLabel : styles.inactiveLabel}>
							{"Delivered (" + deliveredCounts + ")"}
						</Text>
						<View
							style={[
								tabIndex === 2
									? styles.underLineStyle
									: styles.transparentUnderLineStyle,
								styles.underLineWidth,
							]}
						/>
					</TouchableOpacity>
				</View>
				{props.isLoading && <Loader />}
				<View style={styles.addressCard}>
					<View style={styles.listContainer}>
						{listData === [] ? (
							<View style={styles.noDataContainer}>
								<Text style={styles.noDataText}>{'No Data Found'}</Text>
							</View>
						) : (
								<FlatList
									style={styles.flatListStyle}
									data={topTabIndex === 0 ?listData : requestlistData}
									extraData={topTabIndex === 0 ?listData : requestlistData}
									keyExtractor={(item, index) => index.toString()}
									showsVerticalScrollIndicator={false}
									renderItem={renderItem}
									tabIndex={tabIndex}
									refreshControl={
										<RefreshControl
											refreshing={rfLoader}
											onRefresh={onRefreshList}
											title=""
											tintColor="red"
											colors={['red']}
										/>
									}
								/>
							)}
					</View>
				</View>
			</View>
		</View>


	);
};

FavouritesView.propTypes = {

	changeTab: PropTypes.func,
	changeTopTab: PropTypes.func,
	call: PropTypes.func,
	viewLocation: PropTypes.func,
	tabIndex: PropTypes.number,

};

export default FavouritesView;
