import React, { useEffect, useState,useRef } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import FavouritesView from './FavouritesView';
import { bindActionCreators } from "redux";
import {AppState} from 'react-native';
import globals from "../../lib/globals";
import functions from "../../lib/functions"
import * as ordersActions from '../../actions/orderActions';
import _ from 'lodash'
import moment from 'moment';
import { Linking, Alert, Platform } from 'react-native';
import * as notifyActions from '../../actions/notifyActions';
import Sound from 'react-native-sound';
import {Notifications} from 'react-native-notifications';
import {BackHandler, ToastAndroid} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { showLocation } from 'react-native-map-link'

const FavouritesScreen = (props) => {
  const [topTabIndex, settopTabIndex] = useState(0);
  const [tabIndex, setTabIndex] = useState(0);
  const [rfLoader, setRefreshLoader] = useState(false);
  const [todayCount, settodayCount] = useState(
    _.get(props, 'todaysCount', 0)
   );
   const [activeCount, setactiveCount] = useState(
     _.get(props, 'activeCount', 0)
   );
   const [deliveredCount, setdeliveredCount] = useState(
     _.get(props, 'deliveredCount', 0)
   );

   const appState = useRef(AppState.currentState);
   const [appStateVisible, setAppStateVisible] = useState(appState.current);
 
   useEffect(() => {
     AppState.addEventListener('change', _handleAppStateChange);
 
     return () => {
       AppState.removeEventListener('change', _handleAppStateChange);
     };
   }, []);
 
   const _handleAppStateChange = nextAppState => {
     if (
       appState.current.match(/inactive|background/) &&
       nextAppState === 'active'
     ) {
      
     }
 
     appState.current = nextAppState;
     setAppStateVisible(appState.current);
     if (appState.current === 'active') {
     props.getNotificationCount(props.token);
     }
   };
    const [notifyData, setNotifyData] = useState({});
   const [displayNotificationPopup, setDisplayNotificationPopup] =
     useState(false);
 
     const notificationRead = async id => {
       const sAsyncWrapper = new ServiceWrapperAwait();
       await sAsyncWrapper.get('driver/notification/read/' + id, {
         language_attach: false,
         is_auth_required: true,
       });
     };
   
     playSound = () => {
       const callback = (error, sound) => {
         if (error) {
           return;
         }
         sound.play(() => {
           sound.release();
         });
       };
       const audio = require('../../sound/salon_alarm.mp3');
       const sound = new Sound(audio, error => callback(error, sound));
     };
   
     initialNotification = async () => {
       const notification = await Notifications.getInitialNotification();
       if (typeof notification !== 'undefined' && notification) {
         props.getNotificationCount(props.token);
         console.log('response in initialnotiictaion');
         try {
           const _data = notification.payload;
           if (_data.notification_id) {
             notificationRead(_data.notification_id);
           }
           if (_data.type == 'order') {
             props.navigation.navigate('OrderDetailsScreen', {id: _data.order_id});
           } 
           else if (_data.type == 'collection') {
             props.navigation.navigate('OrderDetailsScreen', {isCollectionScreen: true,id: _data.order_id});
           } 
           else if (_data.type == 'support') {
             props.navigation.navigate('ChatScreen', {
               chat_id: _data.request_id,
             });
           }
           else if (_data.type == 'request') {
            props.navigation.navigate('OrderDetailsScreen', {
              isCollectionScreen: tabIndex === 0 ? true : false,
              order_id: _data.request_id,
              from: true
            });
          }
         } catch (err) {
           console.log('open notify err', err);
         }
       }
     };
 
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      props.getNotificationCount(props.token);
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let date = new Date().toDateString()
          let params = {
            "date": moment(date).format('YYYY/MM/DD')
          }
          props.getTodayOrders(params, props.token)
          props.getAllOrders(props.token)
          props.getDeliveredOrders(props.token)

          props.getAllRequest(props.token)
        props.getTodayRequest(params, props.token)
        props.getDeliveredRequest(props.token)

        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', () => {
        // BackHandler.exitApp();
        // return true;
        if (backPressed > 0) {
          BackHandler.exitApp();
          backPressed = 0;
        } else {
          backPressed++;
          ToastAndroid.show('Please tap again to exit', ToastAndroid.SHORT);

          setTimeout(() => {
            backPressed = 0;
          }, 2000);
          return true;
        }
      });
    });
  }, [props.navigation]);

  let backPressed = 0;

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    props.getNotificationCount(props.token)
    foregroundState();
    Sound.setCategory('Playback', true);
    Notifications.registerRemoteNotifications();
    initialNotification();
    
    Notifications.events().registerNotificationOpened(
      (notification, completion) => {
        try {
          const _data = notification.payload;
          if (_data.notification_id) {
            notificationRead(_data.notification_id);
          }
          if (_data.type == 'order') {
            props.navigation.navigate('OrderDetailsScreen', {
              order_id: _data.order_id,
            });
            
          } 
          else if (_data.type == 'collection') {
            props.navigation.navigate('OrderDetailsScreen', {isCollectionScreen: true,
              order_id : _data.order_id,
            });
            
          } 
          else if (_data.type == 'support') {
            props.navigation.navigate('ChatScreen', {
              chat_id: _data.request_id,
            });
          }
          else if (_data.type == 'request') {
            props.navigation.navigate('OrderDetailsScreen', {
              isCollectionScreen: tabIndex === 0 ? true : false,
              order_id: _data.request_id,
              from: true
            });
          }
          // RootNavigation.navigate('ChatScreen', { chat_id: '_data.request_id' });
        } catch (err) {
          console.log('open notify err', err);
        }
        completion();
      },
      );
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let date = new Date().toDateString()
        let params = {
          "date": moment(date).format('YYYY/MM/DD')
        }
        props.getTodayOrders(params, props.token)
        props.getAllOrders(props.token)
        props.getDeliveredOrders(props.token)

        props.getAllRequest(props.token)
        props.getTodayRequest(params, props.token)
        props.getDeliveredRequest(props.token)

      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {

  };

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if(!props.isLoading)
    {
      settodayCount( topTabIndex === 0 ?
        _.get(props, 'todaysCount', 0) :
        _.get(props, 'todaysRequestCount', 0)
        )
        setactiveCount( topTabIndex === 0 ?
          _.get(props, 'activeCount', 0) :
          _.get(props, 'activeRequestCount', 0)
        )
        setdeliveredCount( topTabIndex === 0 ?
          _.get(props, 'deliveredCount', 0) :
          _.get(props, 'deliveredRequestCount', 0)
        )
    }

  };
  const foregroundState = () => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      props.getNotificationCount(props.token);
      playSound();
      const _data = getNotificationData(remoteMessage);
      setNotifyData(_data);
      setDisplayNotificationPopup(true);
      let date = new Date().toDateString()
      let params = {
        "date": moment(date).format('YYYY/MM/DD')
      }
      props.getTodayCollection(params, props.token)
      props.getAllCollection(props.token)

    });
    return unsubscribe;
  };
  const getNotificationData = remoteMessage => {
    let body = 'You have a new notification';
    let title = 'Notification';

    if (typeof remoteMessage.notification != 'undefined') {
      if (typeof remoteMessage.notification.body != 'undefined') {
        body = remoteMessage.notification.body;
      }
      if (typeof remoteMessage.notification.title != 'undefined') {
        title = remoteMessage.notification.title;
      }
    } else if (typeof remoteMessage.data != 'undefined') {
      if (typeof remoteMessage.data.body != 'undefined') {
        body = remoteMessage.data.body;
      } else if (
        typeof remoteMessage.data.notification != 'undefined' &&
        typeof remoteMessage.data.notification.body != 'undefined'
      ) {
        body = remoteMessage.data.notification.body;
      }
      if (typeof remoteMessage.data.title != 'undefined') {
        title = remoteMessage.data.title;
      } else if (
        typeof remoteMessage.data.notification != 'undefined' &&
        typeof remoteMessage.data.notification.title != 'undefined'
      ) {
        body = remoteMessage.data.notification.title;
      }
    }
    let details = null;
    if (
      remoteMessage &&
      remoteMessage.data &&
      remoteMessage.data &&
      remoteMessage.data.type
    ) {
      if (remoteMessage.data.type === 'order') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        };
        
      } 
      else if (remoteMessage.data.type === 'collection') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        };
        
      } 
      else if (remoteMessage.data.type === 'support') {
        details = {
          type: remoteMessage.data.type,
          requestId: remoteMessage.data.request_id,
        };
      }
      else if (remoteMessage.data.type === 'request') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.request_id,
        };
      }
    }

    return {
      title: title,
      body: body,
      details: details,
    };
  };
  const changeTab = index => {
    if (tabIndex !== index) {
      setTabIndex(index);
    }
  };
  const changeTopTab = index => {
    if (topTabIndex !== index) {
      settopTabIndex(index);
    }
  };
  const onProfileButtonPress = () => {
    props.navigation.navigate('ProfileScreen')
  };
  const call = (phone) => {
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    }
    else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
          // return Linking.openURL(phoneNumber);
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };
  const viewLocation =(locattiondetails) =>{
    let cordinates = locattiondetails.delivery_loc_coordinates ?  JSON.parse(locattiondetails.delivery_loc_coordinates) : ''
    let isLatitude = cordinates ? true : false
    showLocation({
      latitude: cordinates.latitude ? cordinates.latitude : '',
      longitude: cordinates.longitude ? cordinates.longitude : '',
      googleForceLatLon: isLatitude,
      alwaysIncludeGoogle: true,
      cancelText: 'Cancel',
      title: locattiondetails.delivery_location,
      appsWhiteList: ['google-maps']
    })
  }
  const onNotificationButtonPress = () => {
    props.navigation.navigate('NotificationScreen');
  };
  const itemClick = (id) => {
    props.navigation.navigate('OrderDetailsScreen', { isCollectionScreen: false, order_id: id, from: topTabIndex  === 0 ? false : true})
  };

  const onRefreshFlatList = () => {
    setRefreshLoader(false)
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let date = new Date().toDateString()
        let params = {
          "date": moment(date).format('YYYY/MM/DD')
        }
        props.getTodayOrders(params, props.token)
        props.getAllOrders(props.token)
        props.getDeliveredOrders(props.token)

        props.getAllRequest(props.token)
        props.getTodayRequest(params, props.token)
        props.getDeliveredRequest(props.token)
        

      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };
  return (
    <FavouritesView
    topTabIndex={topTabIndex}
      todayCounts = {todayCount}
      activeCounts={activeCount}
      deliveredCounts={deliveredCount}
      isLoading={props.isLoading}
      onRefreshList={onRefreshFlatList}
      rfLoader={rfLoader}
      onProfileButtonPress={onProfileButtonPress}
      onNotificationButtonPress={onNotificationButtonPress}
      itemClick={itemClick}
      changeTab={changeTab}
      changeTopTab={changeTopTab}
      tabIndex={tabIndex}
      
      call={call}
      viewLocation={viewLocation}
      listData={
        tabIndex === 0 && topTabIndex === 0
          ? props.todaysOrdersData !== null && props.todaysOrdersData
            ? props.todaysOrdersData
            : []
          : tabIndex === 1 && topTabIndex === 0
            ? props.activeOrdersData !== null && props.activeOrdersData
              ? props.activeOrdersData
              : []
            : tabIndex === 2 && topTabIndex === 0
              ? props.deliveredOrdersData !== null && props.deliveredOrdersData
                ? props.deliveredOrdersData
                : []
              : null
      }
      requestlistData={
        tabIndex === 0 && topTabIndex === 1
          ? props.todaysRequestData !== null && props.todaysRequestData
            ? props.todaysRequestData
            : []
          : tabIndex === 1 && topTabIndex === 1
            ? props.activeRequestData !== null && props.activeRequestData
              ? props.activeRequestData
              : []
            : tabIndex === 2 && topTabIndex === 1
              ? props.deliveredRequestData !== null && props.deliveredRequestData
                ? props.deliveredRequestData
                : []
              : null
      }
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token'),
    todaysOrdersData: _.get(state, 'orderReducer.todaysOrderListData.data.data'),
    activeOrdersData: _.get(state, 'orderReducer.activeOrderListData.data.data'),
    deliveredOrdersData: _.get(state, 'orderReducer.deliveredOrderListData.data.data'),
    isLoading: _.get(state, 'orderReducer.isLoading'),

    todaysCount: _.get(state, 'orderReducer.todaysCount'),
    activeCount: _.get(state, 'orderReducer.activeCount'),
    deliveredCount: _.get(state, 'orderReducer.deliveredCount'),

    todaysRequestData: _.get(state, 'orderReducer.todaysRequestListData.data.data'),
    activeRequestData: _.get(state, 'orderReducer.activeRequestListData.data.data'),
    deliveredRequestData: _.get(state, 'orderReducer.deliveredRequestListData.data.data'),


    activeRequestCount: _.get(state, 'orderReducer.activeRequestCount'),
    todaysRequestCount: _.get(state, 'orderReducer.todaysRequestCount'),
    deliveredRequestCount: _.get(state, 'orderReducer.deliveredRequestCount'),

    // activeRequestListData
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getNotificationCount: notifyActions.getNotificationCount,

    getTodayOrders: ordersActions.doGetTodaysOrders,
    getAllOrders: ordersActions.doGetAllOrders,
    getDeliveredOrders: ordersActions.doGetDeliveredOrders,
    
    getTodayRequest: ordersActions.doGetTodaysRequest,
    getAllRequest: ordersActions.doGetAllRequest,
    getDeliveredRequest: ordersActions.doGetDeliveredRequest,

  }, dispatch)
};

const favouriteScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouritesScreen);

favouriteScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default favouriteScreenWithRedux;
