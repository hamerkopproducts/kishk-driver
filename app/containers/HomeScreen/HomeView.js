import React from 'react';
import {
  View,
  ScrollView,
  Text,
  I18nManager,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
} from 'react-native';
import globals from '../../lib/globals';
import {styles, images} from './styles';
import RNRestart from 'react-native-restart';
import Header from '../../components/Header';
import PropTypes from 'prop-types';
import CollectionCard from '../../components/CollectionCard';
import InAppNotificationModal from '../NotificationPopup/InAppNotificationModal';
import Loader from '../../components/Loader';
const HomeView = (props) => {
	const {
    changeTab,
    changeTopTab,
    isLoading,
    tabIndex,
    topTabIndex,
    listData,
    requestlistData,
		itemClick,
    onNotificationButtonPress,
    onProfileButtonPress,
    displayNotificationPopup,
     notifyData,
     setDisplayNotificationPopup,
     navigateTochat,
     onRefreshList = () => {},
     rfLoader,
     todayCount,
     totalCount,
     onEndReached
  } = props;
	const switchlang = () => {
		if (I18nManager.isRTL) {
		  I18nManager.forceRTL(false);
		} else {
		  I18nManager.forceRTL(true);
		}
		setTimeout(() => {
		  RNRestart.Restart();
		}, 500);
	  }
	  const renderItem = ({item, index}) => (
		<CollectionCard isrequest = {topTabIndex === 0 ? false: true} item={item} itemClick={itemClick} tabIndex={tabIndex} />
	  );
	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				isProfileButtonRequired={true}
        onProfileButtonPress={onProfileButtonPress}
        isNotificationButtonRequired={true}
        onNotificationButtonPress={onNotificationButtonPress}
        customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>

      <View style={styles.screenContainer}>
      {displayNotificationPopup && (
          <InAppNotificationModal
            hide={() => setDisplayNotificationPopup(false)}
            visible={true}
            title={notifyData.title}
            body={notifyData.body}
            redirect={() => navigateTochat(notifyData)}
          />
        )}
        <View style={styles.lineBorder} />
        <View style={styles.containerTop}>
          <TouchableOpacity
            style={ topTabIndex === 0 ? styles.myOrdersContainertop: styles.myOrdersContainerNo}
            onPress={() => {
              changeTopTab(0);
            }}>
            <Text
              style={topTabIndex === 0 ? styles.orderLabelTop : styles.inactiveLabelTop}>
              {'Order'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={ topTabIndex === 1 ? styles.myOrdersContainertop: styles.myOrdersContainerNo}
            onPress={() => {
              changeTopTab(1);
            }}>
            <Text
              style={topTabIndex === 1 ? styles.orderLabelTop : styles.inactiveLabelTop}>
              {'Delivery Request'}
            </Text>
          
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.myOrdersContainer}
            onPress={() => {
              changeTab(0);
            }}>
            <Text
              style={tabIndex === 0 ? styles.orderLabel : styles.inactiveLabel}>
              {'Todays Collection (' + todayCount + ')'}
            </Text>
            <View
              style={
                tabIndex === 0
                  ? styles.underLineStyle
                  : styles.transparentUnderLineStyle
              }
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.myQuotationContainer}
            onPress={() => {
              changeTab(1);
            }}>
            <Text
              style={tabIndex === 1 ? styles.orderLabel : styles.inactiveLabel}>
              {'All Collection (' + totalCount + ')'}
            </Text>
            <View
              style={[
                tabIndex === 1
                  ? styles.underLineStyle
                  : styles.transparentUnderLineStyle,
                styles.underLineWidth,
              ]}
            />
          </TouchableOpacity>
        </View>
        {props.isLoading && <Loader />}
        <View style={styles.addressCard}>
          <View style={styles.listContainer}>
            {listData.length  === 0 ? (
              <View style={styles.noDataContainer}>
                <Text style={styles.noDataText}>{I18nManager.isRTL ? 'لاتوجد بيانات' : 'No data found'}</Text>
              </View>
            ) : (
                <FlatList
                  style={styles.flatListStyle}
                  data={ topTabIndex === 0 ? listData : requestlistData}
                  extraData={ topTabIndex === 0 ? listData : requestlistData}
                  keyExtractor={(item, index) => index.toString()}
                  showsVerticalScrollIndicator={false}
                  renderItem={renderItem}
                  onEndReached={() => onEndReached(tabIndex)}
              onEndReachedThreshold={0.5}
                  tabIndex={tabIndex}
                  refreshControl={
                    <RefreshControl
                      refreshing={rfLoader}
                      onRefresh={onRefreshList}
                      title=""
                      tintColor="red"
                      colors={['red']}
                    />
                  }
                />
              )}
          </View>
        </View>
      </View>
    </View>
  );
};

HomeView.propTypes = {
  changeTab: PropTypes.func,
  changeTopTab: PropTypes.func,
  onEndReached: PropTypes.func,
  tabIndex: PropTypes.number,
};

export default HomeView;
