import React, { useEffect, useState, useRef } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import HomeView from './HomeView';
import { bindActionCreators } from "redux";
import * as notifyActions from '../../actions/notifyActions';
import Sound from 'react-native-sound';
import { Notifications } from 'react-native-notifications';
import { AppState } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import globals from "../../lib/globals";
import functions from "../../lib/functions"
import * as collectionActions from '../../actions/collectionActions';
import * as ordersActions from '../../actions/orderActions';
import messaging from '@react-native-firebase/messaging';
import _ from 'lodash'
import moment from 'moment';
import { BackHandler, ToastAndroid } from 'react-native';

const HomeScreen = (props) => {


  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [topTabIndex, settopTabIndex] = useState(0);
  const [rfLoader, setRefreshLoader] = useState(false);
  const [todayCount, setTodayCount] = useState(0);
  const [totalCount, setTotalCount] = useState(0);
  const [pageTab1, setPageTab1] = useState(1);
  const [pageTab2, setPageTab2] = useState(1);

  const [tab1_lastPage, setTab1LastPage] = useState(0);
  const [tab2_lastPage, setTab2LastPage] = useState(0);

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = nextAppState => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {

    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    if (appState.current === 'active') {
      props.getNotificationCount(props.token);
    }
  };
  const [notifyData, setNotifyData] = useState({});
  const [displayNotificationPopup, setDisplayNotificationPopup] =
    useState(false);

  const notificationRead = async id => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    await sAsyncWrapper.get('driver/notification/read/' + id, {
      language_attach: false,
      is_auth_required: true,
    });
  };

  playSound = () => {
    const callback = (error, sound) => {
      if (error) {
        return;
      }
      sound.play(() => {
        sound.release();
      });
    };
    const audio = require('../../sound/salon_alarm.mp3');
    const sound = new Sound(audio, error => callback(error, sound));
  };

  initialNotification = async () => {
    const notification = await Notifications.getInitialNotification();
    if (typeof notification !== 'undefined' && notification) {
      props.getNotificationCount(props.token);
      console.log('response in initialnotiictaion');
      try {
        const _data = notification.payload;
        if (_data.notification_id) {
          notificationRead(_data.notification_id);
        }
        if (_data.type == 'order') {
          props.navigation.navigate('OrderDetailsScreen', { 
            order_id: _.get(_data, 'order_id', ''),
          });
        }
        else if (_data.type == 'collection') {
          props.navigation.navigate('OrderDetailsScreen', 
          { 
            isCollectionScreen: true, 
            order_id: _.get(_data, 'order_id', ''),
            
          
          });
        }
        else if (_data.type == 'support') {
          props.navigation.navigate('ChatScreen', {
            chat_id: _.get(_data, 'request_id', ''),
          });
        }
        else if (_data.type == 'request') {
          props.navigation.navigate('OrderDetailsScreen', {
            isCollectionScreen: tabIndex === 0 ? true : false,
            order_id: _.get(_data, 'request_id', ''),
            from: true
          });
        }
      } catch (err) {
        console.log('open notify err', err);
      }
    }
  };

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      props.getNotificationCount(props.token);
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let date = new Date().toDateString()
          let params = {
            "date": moment(date).format('YYYY/MM/DD')
          }
          props.getTodayCollection(params, props.token)
          props.getAllCollection(props.token)
          props.getTodayRequestCollection(params, props.token)
          props.getAllRequestCollection(props.token)

        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', () => {
        // BackHandler.exitApp();
        // return true;
        if (backPressed > 0) {
          BackHandler.exitApp();
          backPressed = 0;
        } else {
          backPressed++;
          ToastAndroid.show('Please tap again to exit', ToastAndroid.SHORT);

          setTimeout(() => {
            backPressed = 0;
          }, 2000);
          return true;
        }
      });
    });
  }, [props.navigation]);

  let backPressed = 0;

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    props.getNotificationCount(props.token)
    console.log('count at mount home', props.token);
    console.log('count at mount', props.notificationCount)
    foregroundState();
    Sound.setCategory('Playback', true);
    Notifications.registerRemoteNotifications();
    initialNotification();

    Notifications.events().registerNotificationOpened(
      (notification, completion) => {
        try {
          const _data = notification.payload;
          if (_data.notification_id) {
            notificationRead(_data.notification_id);
          }
          if (_data.type == 'order') {
            props.navigation.navigate('OrderDetailsScreen', {
              order_id: _.get(_data, 'order_id', ''),
            });

          }
          else if (_data.type == 'collection') {
            props.navigation.navigate('OrderDetailsScreen', {
              isCollectionScreen: true,
              order_id: _.get(_data, 'order_id', ''),
            });

          }
          else if (_data.type == 'support') {
            props.navigation.navigate('ChatScreen', {
              chat_id: _data.request_id,
            });
          }
          else if (_data.type == 'request') {
            props.navigation.navigate('OrderDetailsScreen', {
              isCollectionScreen: tabIndex === 0 ? true : false,
              order_id: _.get(_data, 'request_id', ''),
              from: true
            });
          }
          // RootNavigation.navigate('ChatScreen', { chat_id: '_data.request_id' });
        } catch (err) {
          console.log('open notify err', err);
        }
        completion();
      },
    );
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let date = new Date().toDateString()
        let params = {
          "date": moment(date).format('YYYY/MM/DD')
        }
        props.getTodayCollection(params, props.token)
        props.getAllCollection(props.token)
        props.getTodayRequestCollection(params, props.token)
          props.getAllRequestCollection(props.token)
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }

    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {

  };

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (!props.isLoading) {
      setTodayCount(
        _.get(props, 'todaysCount', 0)
      )
      setTotalCount(
        _.get(props, 'allCount', 0)
      )
    }

  };
  const foregroundState = () => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      props.getNotificationCount(props.token);
      playSound();
      const _data = getNotificationData(remoteMessage);
      setNotifyData(_data);
      setDisplayNotificationPopup(true);
      let date = new Date().toDateString()
      let params = {
        "date": moment(date).format('YYYY/MM/DD')
      }
      props.getTodayCollection(params, props.token)
      props.getAllCollection(props.token),
      props.getTodayOrders(params, props.token)
      props.getAllOrders(props.token)
      props.getDeliveredOrders(props.token)

    });
    return unsubscribe;
  };

  const getNotificationData = remoteMessage => {
    let body = 'You have a new notification';
    let title = 'Notification';

    if (typeof remoteMessage.notification != 'undefined') {
      if (typeof remoteMessage.notification.body != 'undefined') {
        body = remoteMessage.notification.body;
      }
      if (typeof remoteMessage.notification.title != 'undefined') {
        title = remoteMessage.notification.title;
      }
    } else if (typeof remoteMessage.data != 'undefined') {
      if (typeof remoteMessage.data.body != 'undefined') {
        body = remoteMessage.data.body;
      } else if (
        typeof remoteMessage.data.notification != 'undefined' &&
        typeof remoteMessage.data.notification.body != 'undefined'
      ) {
        body = remoteMessage.data.notification.body;
      }
      if (typeof remoteMessage.data.title != 'undefined') {
        title = remoteMessage.data.title;
      } else if (
        typeof remoteMessage.data.notification != 'undefined' &&
        typeof remoteMessage.data.notification.title != 'undefined'
      ) {
        body = remoteMessage.data.notification.title;
      }
    }
    let details = null;
    if (
      remoteMessage &&
      remoteMessage.data &&
      remoteMessage.data &&
      remoteMessage.data.type
    ) {
      if (remoteMessage.data.type === 'order') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        };

      }
      else if (remoteMessage.data.type === 'request') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.request_id,
          isFrom: remoteMessage.data.is_from === 'collection' ? true : false
        };

      }
      else if (remoteMessage.data.type === 'collection') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        };

      }
      else if (remoteMessage.data.type === 'support') {
        details = {
          type: remoteMessage.data.type,
          requestId: remoteMessage.data.request_id,
        };
      }
    }

    return {
      title: title,
      body: body,
      details: details,
    };
  };

  const changeTab = index => {
    if (tabIndex !== index) {
      setTabIndex(index);
    }
  };
  const changeTopTab = index => {
    if (topTabIndex !== index) {
      settopTabIndex(index);
    }
  };
  const onEndReached = (tab) => {
    if (tab == 0) {
      const _page = parseInt(pageTab1) + 1;
      if (_page <= tab1_lastPage) {
        setPageTab1(_page);
        // collectionsDataFetchToday(_page, '');
      }
    } else {
      const _page = parseInt(pageTab2) + 1;
      if (_page <= tab2_lastPage) {
        setPageTab2(_page);
        // collectionsDataFetchAll(_page, '');
      }
    }
  }
  const itemClick = (id, isrequest, subOrderId) => {
    //alert('detailsPage Clicked');
    props.navigation.navigate('OrderDetailsScreen', { isCollectionScreen: true, order_id: id, sub_ord_id: subOrderId, from: isrequest })
  };

  const onProfileButtonPress = () => {
    //alert('detailsPage Clicked');
    props.navigation.navigate('ProfileScreen')
  };
  const onNotificationButtonPress = () => {
    //alert('detailsPage Clicked');
    props.navigation.navigate('NotificationScreen')
  };
  const onRefreshFlatList = () => {
    setRefreshLoader(false)
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let date = new Date().toDateString()
        let params = {
          "date": moment(date).format('YYYY/MM/DD')
        }
        props.getTodayCollection(params, props.token)
        props.getAllCollection(props.token)
        props.getTodayRequestCollection(params, props.token)
          props.getAllRequestCollection(props.token)

      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };
  return (
    <HomeView
    
  
      onRefreshList={onRefreshFlatList}
      onEndReached={onEndReached}
      todayCount={topTabIndex === 0 ? _.get(props, 'todaysCount', 0) : _.get(props, 'todaysRequestCount', 0)}
      totalCount={topTabIndex === 0 ? _.get(props, 'allCount', 0) : _.get(props, 'requestallCount', 0)}
      rfLoader={rfLoader}
      isLoading={props.isLoading}
      onProfileButtonPress={onProfileButtonPress}
      onNotificationButtonPress={onNotificationButtonPress}
      changeTab={changeTab}
      changeTopTab={changeTopTab}
      tabIndex={tabIndex}
      topTabIndex={topTabIndex}
      itemClick={itemClick}
      notification_badge={props.notification_badge}
      is_notification_active={props.is_notification_active}
      displayNotificationPopup={displayNotificationPopup}
      setDisplayNotificationPopup={setDisplayNotificationPopup}
      notifyData={notifyData}
      navigateTochat={_data => {
        setDisplayNotificationPopup(false);
        if (_.get(_data, 'details.type', '') == 'order') {
          props.navigation.navigate('OrderDetailsScreen', {
            order_id: _data.details.order_id,
          });
        }
        else if (_.get(_data, 'details.type', '') == 'collection') {
          props.navigation.navigate('OrderDetailsScreen', {
            isCollectionScreen: true,
            order_id: _data.details.order_id,
          });
        } else if (_.get(_data, 'details.type', '') == 'support') {
          props.navigation.navigate('ChatScreen', {
            chat_id: _data.details.requestId,
          });
        }
        else if (_.get(_data, 'details.type', '')  == 'request') {
          props.navigation.navigate('OrderDetailsScreen', {
            isCollectionScreen: _data.details.isFrom ,
            order_id: _.get(_data, 'details.order_id', ''),
            from: true
          });
        }
      }}
      listData={
        tabIndex === 0 && topTabIndex === 0
          ? _.get(props, 'collectionData.data', {})
          : tabIndex === 1 && topTabIndex === 0
            ? _.get(props, 'allcollectionData.data', {})
            : 'null'
      }
      requestlistData={
        tabIndex === 0 && topTabIndex === 1
          ? _.get(props, 'collectionRequestData.data', {})
          : tabIndex === 1 && topTabIndex === 1
            ? _.get(props, 'allcollectionRequestData.data', {})
            : 'null'
      }
    />
  );

};

const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token'),
    isLoading: _.get(state, 'collectionReducer.isLoading'),
    collectionData: _.get(state, 'collectionReducer.collectionListData'),
    allcollectionData: _.get(state, 'collectionReducer.AllcollectionListData'),
    collectionRequestData: _.get(state, 'collectionReducer.TodaysRequestcollectionListData'),
    allcollectionRequestData: _.get(state, 'collectionReducer.AllRequestcollectionListData'),
    allcollectionRequestData: _.get(state, 'collectionReducer.AllRequestcollectionListData'),
    allCount: _.get(state, 'collectionReducer.totalCount'),
    todaysCount: _.get(state, 'collectionReducer.todaysCount'),
    
    requestallCount: _.get(state, 'collectionReducer.requestTotalCount'),
    todaysRequestCount: _.get(state, 'collectionReducer.requestTodaysCount'),

    notification_badge: _.get(state, 'loginReducer.notificationbadge', ''),
    notificationCount: _.get(state, 'loginReducer.notificationCount', ''),
    notificationCount: _.get(state, 'loginReducer.notificationCount', ''),
    token: _.get(state, 'loginReducer.userData.data.access_token', ''),

  }
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getTodayRequestCollection: collectionActions.doGetTodayRequestCollection,
      getAllRequestCollection: collectionActions.doGetAllRequestCollection,
      getTodayCollection: collectionActions.doGetTodaysCollection,
      getAllCollection: collectionActions.doGetAllCollection,
      getNotificationCount: notifyActions.getNotificationCount,
      getTodayOrders: ordersActions.doGetTodaysOrders,
      getAllOrders: ordersActions.doGetAllOrders,
      getDeliveredOrders: ordersActions.doGetDeliveredOrders

    }, dispatch);
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default homeScreenWithRedux;
