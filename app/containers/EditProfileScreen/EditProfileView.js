import React, {useState, useRef} from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  StatusBar,
  I18nManager,
  Image,
  TextInput,
} from 'react-native';
import globals from '../../lib/globals';
import appTexts from '../../lib/appTexts';
import {styles, images} from './styles';
import PropTypes from 'prop-types';
import Header from '../../components/Header';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import CustomButton from '../../components/CustomButton';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FloatButton from '../../components/FloatButton';
import Loader from '../../components/Loader';
import CropButton from '../../components/CropButton';
import {launchImageLibrary} from 'react-native-image-picker';
import {CropView} from 'react-native-image-crop-tools';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import FastImageLoader from '../../components/FastImage/FastImage';
const EditProfileView = props => {
  const {
    onBackButtonClick,
    profileupdatePress,
    setName,
    name,
    phone,
    setPhone,
    setEmail,
    email,
    isLoading,
    photo,
    imageData,
    setImageData,
  } = props;

  let cropViewRef = null;

  const pickLogo = async () => {
    const options = {
      title: 'Select profile picture',
    };
    try {
      launchImageLibrary(options, res => {
        if (res.didCancel) {
        } else if (res.error) {
        } else {
          if (res.fileSize / 1000000 > 10) {
            functions.displayToast(
              'error',
              'top',
              'Error',
              'Image size should not exceed 1MB',
            );
            return false;
          }
          try {
            const data = Object.assign({}, res.assets[0]);
            setImageData({
              logo: data,
              logoUrl: null,
              selected: true,
              cropped: false,
              typeImage: data.type,
              nameImage: data.fileName,
            });
          } catch (err) {
            console.log('select-rr');
          }
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const cropLogo = () => {
    const quality = Platform.OS === 'ios' ? 0.5 : 50;
    cropViewRef.saveImage(true, quality);
    return false;
  };
  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={globals.COLOR.screenBackground}
      />
      <Header
        navigation={props.navigation}
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        isLeftTitleRequired={true}
        title={appTexts.EDIT.editPro}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: 'center',
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <KeyboardAwareScrollView
        style={styles.screenContainerScrollView}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="always">
        <View style={styles.screenDesignContainer}>
          {/* <View style={styles.profilePic}>
            <Image
              source={images.pro}
              style={styles.pro}
              resizeMode="contain"
            />
          </View> */}
           <View style={styles.profilePic}>
            <TouchableOpacity
              onPress={() => {
                pickLogo();
              }}>
              {imageData.logo == null && photo == null && (
                <Image
                source={images.newF}
                  style={styles.pro}
                  resizeMode="contain"
                />
              )}

              {imageData.logo != null && (
                <>
                  {imageData.selected == false && imageData.cropped == true && (
                    <View>
                      <Image
                        source={{uri: imageData.logo.uri}}
                        style={styles.modallogo}
                      />
                      {/* <Image source={images.frame} style={styles.fram} /> */}
                    </View>
                  )}
                </>
              )}

              {imageData.logo == null && (
                <>
                  {photo != null && (
                    <View style={{marginBottom:5}}>
                      <FastImageLoader
                        resizeMode={'cover'}
                        photoURL={photo}
                        style={styles.modallogo}
                        loaderStyle={{}}
                      />
                      <Image source={images.NewC} style={styles.male} />
                    </View>
                  )}
                  {photo == null && (
                    <View>
                      {/* <Image source={images.main} style={styles.modallogo} /> */}
                      {/* <Image source={images.frame} style={styles.pro} /> */}
                    </View>
                  )}
                </>
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.modalimageWarpper}>
            {imageData.selected == true && imageData.cropped == false && (
              <CropView
                style={{
                  width: 200,
                  height: 200,
                  alignItems: 'center',
                  marginHorizontal: '15%',
                }}
                sourceUrl={imageData.logo.uri}
                setLoader={false}
                ref={ref => (cropViewRef = ref)}
                onImageCrop={res => {
                  setImageData({
                    logo: res,
                    cropped: true,
                    selected: false,
                    typeImage: imageData.typeImage,
                    nameImage: imageData.nameImage,
                  });
                }}
                keepAspectRatio
                aspectRatio={{width: 1, height: 1}}
              />
            )}

            {imageData.selected == true && imageData.cropped == false && (
              <TouchableOpacity
                style={{alignSelf: 'center'}}
                onPress={() => cropLogo()}>
                <View style={styles.cus}>
                  <CropButton
                    buttonstyle={styles.custombuttonStyle}
                    buttonText={'Crop'}
                  />
                </View>
              </TouchableOpacity>
            )}
          </View>

          {isLoading && <Loader />}
          <View style={styles.editSection}>
            <View style={styles.boxView}>
              <View style={styles.name}>
                <Text style={styles.name}>{appTexts.EDIT.name}</Text>
              </View>
              <View style={styles.textInput}>
                <TextInput
                  style={styles.input}
                  value={name}
                  onChangeText={val => {
                    setName(val);
                  }}
                />
              </View>
            </View>
           
            <View style={[styles.boxView, {backgroundColor: '#F5F5F5'}]}>
              <View style={styles.name}>
                <Text style={styles.name}>{appTexts.EDIT.phone}</Text>
              </View>
              <View style={styles.flagLine}>
                <View style={styles.flagImage}>
                <Image
                    source={require('../../assets/images/ChooseLanguage/mask.png')}
                    style={styles.flagSaudi}
                  />
                </View>
                <View style={styles.disable}>
                  <Text style={styles.disText}>+973</Text>
                </View>
                <View style={styles.txt}>
                  <TextInput
                    style={styles.txtin}
                    value={phone}
                    onChangeText={val => {
                      setPhone(val);
                    }}
                    editable={false}
                    keyboardType={'number-pad'}
                  />
                </View>
              </View>
            </View>
            <View style={styles.boxView}>
              <View style={styles.mail}>
                <Text style={styles.name}>{appTexts.EDIT.email}</Text>
                {/* <Text style={styles.star}>*</Text> */}
              </View>
              <View style={styles.textInput}>
                <TextInput
                  style={styles.input}
                  value={email}
                  onChangeText={val => {
                    setEmail(val);
                  }}
                  
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                profileupdatePress();
              }}>
              <View style={styles.saveButton}>
                <Text style={styles.buttonText}>{appTexts.EDIT.save}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
      {/* <FloatButton  isCategory={true} buttonText={appTexts.EDIT.save}></FloatButton> */}
    </View>
  );
};

EditProfileView.propTypes = {
  //onCartButtonPress: PropTypes.func,
};

export default EditProfileView;
