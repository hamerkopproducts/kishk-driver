import React, {useEffect, useState} from 'react';
import EditProfileView from './EditProfileView';
import * as editUserActions from '../../actions/editUserActions';
import NetInfo from '@react-native-community/netinfo';
import {connect} from 'react-redux';
import _ from 'lodash';
import {I18nManager, BackHandler, Keyboard} from 'react-native';
import {bindActionCreators} from 'redux';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import FastImageLoader from '../../components/FastImage/FastImage';
import functions from '../../lib/functions';
import appTexts from '../../lib/appTexts';
const EditProfileScreen = props => {
  const [name, setName] = useState(
    _.get(props, 'editUserDetails.data.name', ''),
  );
  const [phone, setPhone] = useState(
    _.get(props, 'editUserDetails.data.phone', ''),
  );
  const [email, setEmail] = useState(
    _.get(props, 'editUserDetails.data.email', ''),
  );
  const [photo, setPhoto] = useState(
    _.get(props, 'editUserDetails.data.photo', null),
  );
  const [imageData, setImageData] = useState({logo: null});
  const [loader, setLoader] = useState(false);
  const [backDropOpacity, setBackDropOpacity] = useState(0.7);
  //will focus
  useEffect(() => {}, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.editUserData(props.token);
      } else {
        functions.displayAlert(
          null,
          appTexts.ALERT_MESSAGES.noInternet,
          'EditProfile',
        );
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);
  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.homepageAPIReponse) {
    }
  };
  const onBackButtonClick = () => {
    props.navigation.navigate('ProfileScreen');
  };
  // const onCategoryPress = () => {
  //   props.navigation.navigate('CategoryFilterScreen');
  // };
  // const onBrandsPress = () => {
  //   props.navigation.navigate('BrandsScreen');
  // };

  const validateEmail = emailField => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false) {
      return false;
    }
    return true;
  };

  const profileupdatePress = () => {
      if (name.trim() === '' || name === null) {
      functions.displayToast(
        'error',
        'top',
        appTexts.ALERT_MESSAGES.error,
        appTexts.ALERT_MESSAGES.firstnameRequired,
      );
      return;
    }
    else if (email.trim() === '' || email === null) {
      functions.displayToast(
        'error',
        'top',
        appTexts.ALERT_MESSAGES.error,
        appTexts.ALERT_MESSAGES.emailRequired,
      );
      return;
    } else if (validateEmail(email.trim()) === false) {
      functions.displayToast(
        'error',
        'top',
        appTexts.ALERT_MESSAGES.error,
        appTexts.ALERT_MESSAGES.validEmailRequired,
      );
      return;
    } else {
      let _imageData = imageData;
      if (_imageData.logo != null && _imageData.cropped == false) {
        setImageData({
          logo: null,
          cropped: false,
          selected: false,
        });
        _imageData = {logo: null};
      }
      Keyboard.dismiss();
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let _data = new FormData();
          if (_imageData.logo != null) {
            _data.append('photo', {
              name: _imageData.nameImage,
              type: _imageData.typeImage,
              uri:
                Platform.OS === 'android'
                  ? _imageData.logo.uri
                  : _imageData.logo.uri.replace('file://', ''),
            });
          }
          _data.append('name', name);
          _data.append('phone', phone);
          _data.append('email', email);
          saveUserData(_data);
        } else {
          functions.displayToast('error', 'top', '!!!!ERROR', 'MESSAGE ERROR');
        }
      });
    }
  };

  // const profileupdatePress = () => { 
  // alert('update button Pressed');
  // }
  const changeBackDropOpacity = () => {
    setBackDropOpacity(0.1);
    setTimeout(() => {
      setBackDropOpacity(0.7);
    }, 3000);
  };

  const updateProfile = async _data => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.post(
      'api/driver/update-me',
      _data,
      'multipart/form-data',
    );
    setLoader(false);
    try {
      const data = new Promise((resolve, reject) => {
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
      return data;
    } catch (err) {
      console.log(err);
    }
  };

  const saveUserData = async _data => {
    const data = await updateProfile(_data);
    if (data.success == true) {
      props.editUserData(props.token);
      functions.displayToast(
        'success',
        'top',
        'success',
        appTexts.PROFILE.profilemsg,
      );
   
      props.navigation.navigate('ProfileScreen');
      } else {
      functions.displayToast('error', 'top', 'Error', data.error.msg);
    }
  };

  return (
    <EditProfileView
      onBackButtonClick={onBackButtonClick}
      profileupdatePress={profileupdatePress}
      //isLoading={props.isLoading}
      isLoading={props.isLoading || loader}
      name={name}
      setName={setName}
      phone={phone}
      setPhone={setPhone}
      email={email}
      setEmail={setEmail}
      photo={photo}
      imageData={imageData}
      setImageData={setImageData}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token', null),
    editUserDetails: _.get(state, 'editUserReducer.editUserData', ''),
    isLoading: _.get(state, 'editUserReducer.isLoading', ''),
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      editUserData: editUserActions.doUserDetailsEdit,
    },
    dispatch,
  );
};

const EditProfileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditProfileScreen);

EditProfileScreenWithRedux.navigationOptions = ({navigation}) => ({
  header: null,
});

export default EditProfileScreenWithRedux;
