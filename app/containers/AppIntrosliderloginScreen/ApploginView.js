import React from 'react';
import {
  View,
  Text,
  TextInput,
  StatusBar,
  SafeAreaView,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppIntroSlider from 'react-native-app-intro-slider';
import globals from '../../lib/globals';
import appTexts from '../../lib/appTexts';
import {styles, images} from './styles';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import FastImage from 'react-native-fast-image';
import CustomButton from '../../components/CustomButton';
const Apploginview = props => {
  const {loginButtonPress, setPhone, phone,isLoading,data,is_intro_finished} = props;

  const renderItem = ({item}) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}>
        <SafeAreaView>
          {/* <Image source={item.image} style={styles.image} /> */}
          <FastImage
            source={{
              uri: item.image,
              priority: FastImage.priority.normal,
              cache: FastImage.cacheControl.immutable,
            }}
            style={styles.image}
          />
          <View style={styles.titlestyle}>
            <Text style={styles.title}>{item.title}</Text>
          </View>
          <View style={styles.Textwidth}>
            <Text style={styles.text}>{item.text}</Text>
          </View>
        </SafeAreaView>
      </View>
    );
  };

  const _data = props?.howToUseData || [];
  let _max = 0;
  try {
    _max = _data ? _data.length - 1 : -1;
  } catch (err) {
    _max = -1;
  }
  
  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="light-content"
        hidden={false}
        backgroundColor={'transparent'}
        translucent={false}
      />

      <Header
        headerTitle={appTexts.Appintro_login.login}
        customHeaderStyle={{
          height: 40,
          bottom: 15,
          justifyContent: 'center',
          marginHorizontal: '7.5%',
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      {console.log("ISLOADING",isLoading )}
      {isLoading && <Loader />}
      <View style={styles.IntroViews}>
        <AppIntroSlider
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          showDoneButton={false}
          showNextButton={false}
          dotStyle={styles.inactiveDot}
          activeDotStyle={styles.activeDot}
          data={data}
          onSlideChange={(index, lastIndex) => {
            if (index == _max) {
              props.is_intro_finished(true);
            }
          }}
        />
      </View>
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'position' : null}>
     
        <View
          style={
            ([styles.loginformWrapper],
            {
              // height: hp('31%'),
              // shadowColor: '#000000',
              // shadowOffset: { width: 0, height: 2 },
              // shadowOpacity: 0.9,
              // shadowRadius: 3,
              // elevation: 2,
              // backgroundColor: 'white',
              // borderTopLeftRadius: 15,
              // borderTopRightRadius: 15,
              // borderWidth: 1,
              // borderColor: '#ddd',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
              height: hp('31%'),
              shadowOpacity:
                Platform.OS === 'ios' ? 0.1 : globals.INTEGER.opacityHigh,
              elevation: Platform.OS === 'ios' ? 1 : 5,
              shadowRadius: Platform.OS === 'ios' ? 3 : 3,
              shadowOffset: {
                width: 1,
                height: 1,
              },
            })
          }>
          <View style={styles.logininsideWrapper}>
            <Text style={styles.apptitle}>
              {appTexts.Appintro_login.loginHeading}
            </Text>
          </View>
          <View style={styles.inputWrapper}>
            <View style={styles.boxView}>
              <View style={styles.name}>
                <Text style={styles.name}>
                  {appTexts.Appintro_login.phoneNumber}
                </Text>
              </View>
              <View style={styles.flagLine}>
                <View style={styles.flagImage}>
                  <Image source={images.saudi} style={styles.flagSaudi} />
                </View>
                <View style={styles.disable}>
                  <Text style={styles.disText}>+973</Text>
                </View>
                <View style={styles.txt}>
                  <TextInput
                    style={styles.txtin}
                    keyboardType={'number-pad'}
                    value={phone}
                    onChangeText={num => {
                      setPhone(num);
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
          {/* {props.isLoading && <Loader />} */}
          <View style={styles.buttonWrapper}>
            <TouchableOpacity
              onPress={() => {
                loginButtonPress();
              }}>
              <View style={styles.submitButton}>
                <Text style={styles.buttonText}>
                  {appTexts.Appintro_login.signIn}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

export default Apploginview;
