import React, {useState, useEffect} from 'react';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';
import ApploginView from './ApploginView';
import {bindActionCreators} from 'redux';
import functions from '../../lib/functions';
import appTexts from '../../lib/appTexts';
import * as LoginActions from "../../actions/LoginActions";
import _ from "lodash"
import NetInfo from '@react-native-community/netinfo';
import * as termsPrivacyFaqActions from '../../actions/termsPrivacyFaqActions';


const WalkthroughScreen = props => {

  const [phone, setPhone] = useState('');
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      setPhone('');
    });
  }, [props.navigation]);
  
  //mounted
  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.howToUse();
        // props.smsStatusResponse();
      } else {
        functions.displayAlert(
          null,
          globals.ALERT_MESSAGES.noInternet,
          'Intro',
        );
      }
    });
    BackHandler.addEventListener(
      'hardwareBackPress',
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      }),
    );
  };

//will focus
useEffect(() => {
  return props.navigation.addListener('focus', () => {
    NetInfo.fetch().then(state => {
    console.log("LOADER", props.isLoading)
    });

  });
}, [props.navigation]);

  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (
      _.get(props, 'otpAPIResponse') &&
      _.get(props, 'otpAPIResponse.success')
    ) {
      functions.displayToast(
        'success',
        'top',
        appTexts.LOGIN_TOASTMESSAGES.otpsend,
        _.get(props, 'otpAPIResponse.msg'),
      );

      props.navigation.navigate('OtpScreen', { mobileNumber: phone});
      props.getOtpSuccess(null);
    } else if (
      _.get(props, 'otpAPIResponseError') &&
      _.get(props, 'otpAPIResponseError.success') === false
    ) {
      functions.displayToast(
        'error',
        'top',
        appTexts.LOGIN_TOASTMESSAGES.error,
        _.get(props, 'otpAPIResponseError.error.msg'),
      );
      props.resetOtpError(null);
    }
  };

  const loginButtonPress = () =>{
  if (phone.trim() === '' || phone === null) {
    functions.displayToast(
      'error',
      'top',
      appTexts.ALERT_MESSAGES.error,
      appTexts.ALERT_MESSAGES.phoneRequired,
    );
    return;
  }
  else if (!functions.isValidPhone(phone.trim())) {
    functions.displayToast(
      'error',
      'top',
      appTexts.ALERT_MESSAGES.error,
      appTexts.ALERT_MESSAGES.digit,
    );
  }
  else{
    let params = {
      "phone": phone
    }
    console.log(phone);
    props.getOtp(params);
    }

  }
     
  return <ApploginView
  loginButtonPress={loginButtonPress}
  setPhone={setPhone}
  phone={phone}
  isLoading={props.isLoading} 
  data={props.howToUseData}
  is_intro_finished={props.is_intro_finished}
  />;
};

const mapStateToProps = (state, props) => {
  return {
    howToUseData: _.get(
      state,
      'termsFaqPrivacyReducer.howToUseDataReponse',
      '',
    ),
    otpAPIResponse: _.get(state, 'loginReducer.getotpAPIResponse', ''),
    isLoading: _.get(state, 'loginReducer.isLoading', ''),
    is_intro_finished: _.get(state, 'loginReducer.intro_finished', ''),
    otpAPIResponseError: _.get(
      state,
      'loginReducer.getotpAPIResponseError',
      '',
    ),
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getOtp: LoginActions.getOtp,
    getOtpSuccess: LoginActions.getOtpSuccess,
    resetOtpError: LoginActions.otpServiceError,
    howToUse: termsPrivacyFaqActions.howToUseApp,
    
  }, dispatch)
};
const WalkthroughWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps,
)(WalkthroughScreen);

WalkthroughWithRedux.navigationOptions = ({navigation}) => ({
  header: null,
});

export default WalkthroughWithRedux;
