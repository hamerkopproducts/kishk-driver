import {StyleSheet, I18nManager, Platform} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globals from '../../lib/globals';

const images = {
  backImage: require('../../assets/images/ChooseLanguage/Back.png'),
  saudi:require('../../assets/images/Flag/flagnew.png'),
};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    backgroundColor: 'white',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
  },
  image: {
    width: wp('80%'),
    height: hp('30%'),
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: hp('2%'),
  },
  title: {
    color: '#233249',

    textAlign: 'center',
    marginLeft: '4%',
    marginRight: '4%',

    marginVertical: hp('1%'),

    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
    //fontSize: hp('2.6%'),
    fontSize:19
  },
  apptitle:{
    color: '#233249',

    textAlign: 'center',
    marginLeft: '4%',
    marginRight: '4%',

    marginVertical: hp('1%'),

    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
    //fontSize: hp('2.3%'),
    fontSize:16
  },
  titlestyle: {
    marginTop: hp('3%'),
    //backgroundColor:'red'
  },
  Textwidth: {
    width: '90%',
    marginTop: hp('1%'),
  },
  text: {
    color: '#707070',

    textAlign: 'center',
    marginLeft: '4%',
    marginRight: '4%',
    lineHeight: 24,
    marginVertical: hp('1%'),

    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairoRegular,
    //fontSize: hp('2.1%'),
    fontSize:I18nManager.isRTL ? 14 :15
  },
  IntroView: {
    width: '100%',
    flex: 1,
    borderBottomWidth: 0,
    borderColor: globals.COLOR.lightgrey,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: globals.COLOR.grey,
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
  },
  dotB: {
    backgroundColor: globals.COLOR.custombuttoncolor,
    marginTop:
      globals.SCREEN_SIZE.height <= 600
        ? hp('15%')
        : globals.SCREEN_SIZE.height <= 700
        ? hp('0%')
        : hp('0%'),
    height: 5,
    width: 25,
  },
  dotA: {
    marginTop:
      globals.SCREEN_SIZE.height <= 600
        ? hp('15%')
        : globals.SCREEN_SIZE.height <= 700
        ? hp('0%')
        : hp('0%'),
    backgroundColor: globals.COLOR.lightgrey,
    height: 5,
    width: 25,
  },
  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',

    marginVertical: hp('5%'),
  },
  loginformWrapper: {
    height: hp('32%'),
    paddingLeft: '2%',
    paddingRight: '2%',
  },
  
  logininsideWrapper: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: hp('1%'),
    paddingBottom:hp('1%')
  },
  loginText: {
    paddingTop: '5%',
    //fontSize: 16,
    paddingTop: hp('2.5'),
    fontSize: hp('2.2%'),
    fontFamily: globals.FONTS.avenirLight,
  },
  phoneScetion: {
    flexDirection: 'row',
    //backgroundColor:'red',
    bottom: 10,
    paddingLeft: '1%',
  },
  firstSection: {
    flexBasis: '30%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 15,
  },
  firstSection: {
    flexBasis: '30%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 15,
  },
  maskImage: {
    height: 20,
    width: 28,
    borderRadius: 5,
  },
  inpPhone: {
    flexDirection: 'row',
    marginLeft: 2,
  },
  secondSection: {
    paddingLeft: 5,
    flexBasis: '70%',

    flexDirection: 'row',
  },
  borderphone: {
    borderWidth: 1,
    marginHorizontal: '7.5%',
    height: 65,
    borderRadius: 5,
    borderColor: '#ececec',
    marginTop: hp('0.8%'),
    backgroundColor: globals.COLOR.backgroundColor,
  },
  phoneNo: {
    paddingLeft: '6%',
    marginTop: hp('1%'),
  },
  phoneStyle: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairoSemiBold,
    color: globals.COLOR.grey,
    paddingLeft: '3%',
    textAlign: 'left',
  },
  activeDot:{
    backgroundColor: '#ff2c2c',
            marginTop:
              globals.SCREEN_SIZE.height <= 600
                ? hp('10%')
                : globals.SCREEN_SIZE.height <= 700
                ? hp('0%')
                : hp('0%'),
            height: 3,
            width: 30,
  },
inactiveDot:{
  backgroundColor: '#ececec',
  marginTop:
    globals.SCREEN_SIZE.height <= 600
      ? hp('15%')
      : globals.SCREEN_SIZE.height <= 700
      ? hp('0%')
      : hp('0%'),
  height: 3,
  width: 30,
},
buttonWrapper:{
  paddingTop:hp('1%'),
  // backgroundColor:'green',
   alignItems:'center',
   justifyContent:'center'
},
IntroViews: {
  width: '100%',
  flex: 1,
  borderBottomWidth: 0,
  borderColor: globals.COLOR.lightgrey,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
},
boxView:{
  width:'84%',
  height:69,
  borderWidth:0.5,
 // marginTop:'8%',
  marginBottom:'5%',
  borderRadius:4,
  borderColor:globals.COLOR.border,
  paddingLeft:'4%',
  paddingRight:'2%',
  paddingTop:'1%',
//backgroundColor:'cyan'
//paddingBottom:'2%'
},
name:{
  color: globals.COLOR.greyText,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoRegular,
  fontSize:14,
  textAlign:'left'
},
flagLine:{
  flexDirection:'row',
  alignItems:'center'
},
flagSaudi:{
  width:28,
  height:16,
  borderRadius:5,
},
disable:{
  paddingLeft:'2%',
},
disText:{
  color: globals.COLOR.text,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoSemiBold,
  fontSize:16 
},
txt:{
  paddingLeft:'2%',
  width:'80%',
},
txtin:{
color: globals.COLOR.text,
fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoSemiBold,
fontSize:16,
textAlign: I18nManager.isRTL ? "right" : "left",
},
inputWrapper:{
  alignItems:'center',
  paddingTop:'2%'
},
submitButton:{
  backgroundColor: globals.COLOR.custo,
  width: 303,
  height: 45,
  borderRadius: 5,
  backgroundColor: globals.COLOR.custombuttoncolor,
  justifyContent: 'center',
  alignItems: 'center',
},
buttonText: {
  color: 'white',
  fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.cairoSemiBold,
  // fontFamily: globals.FONTS.avenirMedium,
  fontSize: globals.FONTSIZE.fontSizeFifteen,
},
});

export {images, styles};
