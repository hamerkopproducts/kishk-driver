import React, { useEffect, useState } from 'react';
import OrderDetailsView from './OrderDetailsView';
import globals from '../../lib/globals';

import functions from '../../lib/functions';
import NetInfo from "@react-native-community/netinfo";
import _ from 'lodash'
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as collectionActions from '../../actions/collectionActions';
import appTexts from '../../lib/appTexts';
import { Linking, Alert, Platform } from 'react-native';
import { showLocation } from 'react-native-map-link'

// onst OtpScreen = props => {
//   const { mobileNumber } = props.route.params;

const OrderDetailsScreen = props => {
  const {
    order_id,
    from,
    isCollectionScreen,
    sub_ord_id,
  } = props.route.params;
  const [isModalComponentVisible, setIsModalComponentVisible] = useState(false);
  const [subOrderIndex, setSubOrderIndex] = useState([]);
  const [initiallyProcessed, setInitiallyProcessed] = useState(false);
  const [requestId, setRequestId] = useState('');


  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          {!from ? props.getOrdersDetails(order_id, props.token, isCollectionScreen)
            : props.getRequestDetails(order_id, props.token, isCollectionScreen)
          }
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        {!from ? props.getOrdersDetails(order_id, props.token, isCollectionScreen)
          : props.getRequestDetails(order_id, props.token, isCollectionScreen)
        }
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        if (_.get(props, 'collectOrderStatus.success')) {
          {!from ? props.getOrdersDetails(order_id, props.token, isCollectionScreen)
            : props.getRequestDetails(order_id, props.token, isCollectionScreen)
          }
          functions.displayToast(
            'success',
            'top',
            appTexts.COLLECTORDER.success,
            _.get(props, 'collectOrderStatus.msg'),
          );
        }
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
      if(_.get(props, 'collectionSuccessStatus.msg') === 'Collected' && 
      _.get(props, 'collectionSuccessStatus.success'))
      {
        // props.navigation.navigate('HomeScreen')
      }
      else if(_.get(props, 'collectionSuccessStatus.success'))
      {
        props.resetRequestCollcetSuccess()
      }
      if(_.get(props, 'orderDetailsData.order_status') === 'Collection completed' && 
      _.get(props, 'collectOrderStatus.success'))
      {
        // props.navigation.navigate('HomeScreen')
      }
      else if(_.get(props, 'collectOrderStatus.success'))
      {
        props.resetCollcetSuccess()
      }
    });
     /** To make clicked suborder accordion active */
     if (props.orderDetailsData?.sub_orders && subOrderIndex.length === 0 && initiallyProcessed == false) {
       setInitiallyProcessed(true);
      let found = false;
      let data = _.get(props, 'orderDetailsData.sub_orders', [])
      data.map((item, index) => {
        if (item.sub_id == sub_ord_id) {
          found = true;
          setSubOrderIndex([index]);
        }
      });
      if (found == false) {
        setSubOrderIndex([0]);
      }
    }

  };
  const openModalComponent = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
  };

  const closeModalComponent = () => {
    setIsModalComponentVisible(false);
  };
  const onYesClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
    let params = {
      'request_id': requestId
    }
    props.getRequestCollect(params,props.token,order_id,isCollectionScreen)
  };
  const onNoClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
  };

  const onBackButtonClick = () => {
    props.navigation.goBack();
    // props.navigation.navigate('ChatScreen', 
  };
  const call = phone => {
    let phoneNumber = phone;
  if (Platform.OS !== 'android') {
    phoneNumber = `telprompt:${phone}`;
  }
  else  {
    phoneNumber = `tel:${phone}`;
  }
  Linking.canOpenURL(phoneNumber)
  .then(supported => {
    if (!supported) {
      Alert.alert('Phone number is not available');
    } else {
      return Linking.openURL(phoneNumber);
    }
  })
  .catch(err => console.log(err));
  };

  const viewLocation =(location_cordinates, location) =>{
    let cordinates = location_cordinates ?  JSON.parse(location_cordinates) : ''
    let isLatitude = cordinates ? true : false
    showLocation({
      latitude: cordinates.latitude ? cordinates.latitude : '',
      longitude: cordinates.longitude ? cordinates.longitude : '',
      googleForceLatLon: isLatitude,
      alwaysIncludeGoogle: true,
      cancelText: 'Cancel',
      title: location,
      appsWhiteList: ['google-maps']
    })
  }
  return <OrderDetailsView
    devRequest = {from}
    openModalComponent={openModalComponent}
    isModalComponentVisible={isModalComponentVisible}
    closeModalComponent={closeModalComponent}
    onBackButtonClick={onBackButtonClick}
    call={call}
    viewLocation={viewLocation}
    isCollectionScreen={isCollectionScreen}
    orderDetails={props.orderDetailsData}
    isLoading={props.isLoading}
    setIsModalComponentVisible={setIsModalComponentVisible}
    onYesClick={onYesClick}
    onNoClick={onNoClick}
    subOrderIndex={subOrderIndex}
    requestDetailsData={props.requestDetailsData}
    setSubOrderIndex={setSubOrderIndex}
    setRequestId={setRequestId}
  />;
};
const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token', ''),
    orderDetailsData: _.get(state, 'collectionReducer.orderDetailsData.data', {}),
    requestDetailsData: _.get(state, 'collectionReducer.requestDetailsData.data', {}),
    isLoading: _.get(state, 'collectionReducer.isLoading'),
    collectOrderStatus: _.get(state, 'collectionReducer.collectOrderStatus'),
    collectionSuccessStatus: _.get(state, 'collectionReducer.requestCollectionSuccess'),
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

    getOrdersDetails: collectionActions.doGetOrdersDetails,
    getRequestDetails: collectionActions.doGetRequestDetails,
    resetCollcetSuccess: collectionActions.doresetCollcetSuccess,
    resetRequestCollcetSuccess: collectionActions.doresetRequestCollcetSuccess,
    getRequestCollect: collectionActions.doRequestCollcet,
  }, dispatch)
};

const OrderDetailsScreenWithdRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetailsScreen);

OrderDetailsScreenWithdRedux.navigationOptions = ({ navigation }) => ({
  header: null
});
export default OrderDetailsScreenWithdRedux;
