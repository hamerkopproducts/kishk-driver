import React, { useState } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import ScreenTitle from '../../components/ScreenTitle/ScreenTitle';
import { styles, images } from './styles';
//import Accordion from 'react-native-collapsible/Accordion';
import OrderCard from '../../components/OrderCard/OrderCard';
import PriceCard from '../../components/PriceCard';
import CustomerOrderAddressCard from '../../components/CustomerOrderAddressCard/CustomerOrderAddressCard';
import OrderSummaryCard from '../../components/OrderSummaryCard/OrderSummaryCard';
import CustomButton from '../../components/CustomButton';
import ModalComponent from '../../components/ModalComponent';
import appTexts from '../../lib/appTexts';
import Header from '../../components/Header';
import globals from '../../lib/globals';
import Accordion from '../../components/react-native-collapsible/Accordion';
import Loader from '../../components/Loader';
import PropTypes from 'prop-types';
import _ from 'lodash'


const OrderIdCard = ({ viewLocation, order_Details, fromTab, isCollectionScreen, setIsModalComponentVisible, isModalComponentVisible, setRequestId }) => (
  <View style={fromTab ? styles.orderContainerForRequest : styles.orderContainer}>
    <Text style={styles.orderTextHead}>{fromTab ? appTexts.ORDER.drid : appTexts.ORDER.id}</Text>
    <Text style={styles.orderId}>{fromTab ? _.get(order_Details, 'delivery_request_id', '') : _.get(order_Details, 'ord_id', '')}</Text>
    {fromTab && <View>
      <Text style={styles.orderTextHead}>{appTexts.ORDER.location}</Text>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        {isCollectionScreen ? <Text style={styles.locationText}>{_.get(order_Details, 'pickup_location', '')}</Text>
          : <Text style={styles.locationText}>{_.get(order_Details, 'delivery_location', '')}</Text>}
        <TouchableOpacity
          onPress={() => viewLocation(
            isCollectionScreen ? order_Details.pickup_location_coordinates : order_Details.delivery_location_coordinates,
            isCollectionScreen ? order_Details.pickup_location : order_Details.delivery_location)}
        >
          <Text style={styles.viewLoc}>{appTexts.ORDERCARD.location}</Text>
        </TouchableOpacity>
      </View>
    </View>}
    {fromTab && _.get(order_Details, 'pickup_location_detail', '') ? <View>
      <Text style={styles.orderTextHead}>{appTexts.ORDER.locdetails}</Text>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        {isCollectionScreen ? <Text style={styles.locationText}>{_.get(order_Details, 'pickup_location_detail', '')}</Text>
          : <Text style={styles.locationText}>{_.get(order_Details, 'delivery_location_detail', '')}</Text>}
      </View>
    </View> : null}
    {/* _.get(order_Details, 'delivery_location_detail', '')  */}
    {fromTab && _.get(order_Details, 'delivery_location_detail', '') ? <View>
      <Text style={styles.orderTextHead}>{appTexts.ORDER.locdetails} </Text>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        {isCollectionScreen ? <Text style={styles.locationText}>{_.get(order_Details, 'pickup_location_detail', '')}</Text>
          : <Text style={styles.locationText}>{_.get(order_Details, 'delivery_location_detail', '')}</Text>}
      </View>
    </View> : null}
    <View style={styles.orderInnerWarp}>
      <View style={styles.orderDateWarp}>
        <Image source={images.dateIcon} style={styles.icon} />
        {isCollectionScreen ? <Text style={styles.dateAndTime}>{fromTab ? _.get(order_Details, 'collection_time', '') : _.get(order_Details, 'time_slot', '')}</Text>
          : <Text style={styles.dateAndTime}>{fromTab ? _.get(order_Details, 'delivery_time', '') : _.get(order_Details, 'time_slot', '')}</Text>}
      </View>
      <View style={styles.orderTimeWarp}>
        <Image source={images.timeIcon} style={styles.icon} />
        {isCollectionScreen ? <Text style={styles.dateAndTime}>{fromTab ? _.get(order_Details, 'collection_date', '') : _.get(order_Details, 'delivery_schedule_date', '')}</Text>
          : <Text style={styles.dateAndTime}>{fromTab ? _.get(order_Details, 'delivery_date', '') : _.get(order_Details, 'delivery_schedule_date', '')}</Text>}
      </View>
    </View>
    {fromTab && isCollectionScreen &&
      <View style={styles.bottomButtonWarp}>
        {order_Details.status === '2' ?
          <TouchableOpacity style={styles.bottomMarkWarp}
            onPress={() => {
              setIsModalComponentVisible(!isModalComponentVisible)
              setRequestId(order_Details.id)
            }}
          >
            <Text style={styles.markButtonLabel}>{appTexts.ORDER.markCollected}</Text>
          </TouchableOpacity> :
          <View style={styles.bottomButtonInnerWarp}>
            <Image source={images.tickIcon} style={styles.bottomIMG} />
            <Text style={styles.bottomButtonLabel}>{appTexts.ORDER.collected}</Text>
          </View>}
      </View>
    }
    {fromTab && !isCollectionScreen &&
      <View style={styles.bottomButtonWarp}>
        {_.get(order_Details, 'status', '') === '4' ?
          <TouchableOpacity style={styles.bottomMarkWarp}
            onPress={() => {
              setIsModalComponentVisible(!isModalComponentVisible)
              setRequestId(order_Details.id)
            }}
          >
            <Text style={styles.markButtonLabel}>{appTexts.ORDER.markDelivered}</Text>
          </TouchableOpacity> :
          <View style={styles.bottomButtonInnerWarp}>
            <Image source={images.tickIcon} style={styles.bottomIMG} />
            <Text style={styles.bottomButtonLabel}>{appTexts.ORDER.delivered}</Text>
          </View>}
      </View>
    }
  </View>
);

const renderHeader = (section, index, isActive) => {
  return (
    <View style={styles.headerHead}>
      <Text style={styles.headerHeadText}>{section.sub_id}</Text>
      <Image
        source={
          isActive
            ? require('../../assets/images/cart/Up.png')
            : require('../../assets/images/cart/Down.png')
        }
        style={styles.headerHeadImg}
      />
    </View>
  );
};

const renderContent = (section) => {
  return (
    <View style={{ backgroundColor: 'white', paddingVertical: 20 }}>
      <OrderCard
        section={section.items}
        isAccordian={true}
        isCollectionButtonHighlighted={true} />
    </View>
  );
};

const OrderDetailsView = props => {
  const {
    openModalComponent,
    isModalComponentVisible,
    setIsModalComponentVisible,
    closeModalComponent,
    onBackButtonClick,
    isCollectionScreen,
    orderDetails,
    requestDetailsData,
    call,
    viewLocation,
    isLoading,
    onNoClick,
    onYesClick,
    subOrderIndex,
    setSubOrderIndex,
    devRequest,
    setRequestId
  } = props;

  const updateSections = activeSections => {
    setSubOrderIndex(activeSections);
  };
  console.log("FROM", devRequest)
  return (
    <View style={styles.container}>
      {/* <ScreenTitle /> */}
      <Header
        navigation={props.navigation}
        onBackButtonClick={onBackButtonClick}
        isOrderDetails={true}
        heading="Order Details"
        subheading={
          devRequest ? _.get(requestDetailsData, 'request_status', '') :
            _.get(orderDetails, 'order_status', '')}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      {isLoading && <Loader />}
      {!isLoading && <ScrollView scrollEnabled={devRequest ? false : true} showsVerticalScrollIndicator={false}>
        <OrderIdCard
          viewLocation={viewLocation}
          order_Details={!devRequest ? orderDetails : requestDetailsData}
          fromTab={devRequest}
          isCollectionScreen={isCollectionScreen}
          setIsModalComponentVisible={setIsModalComponentVisible}
          isModalComponentVisible={isModalComponentVisible}
          setRequestId={setRequestId}

        />
        {!isCollectionScreen && !devRequest ? (
          <CustomerOrderAddressCard devRequest={devRequest} viewLocation={viewLocation} call={call} order_Details={orderDetails} />
        ) : (!devRequest &&
          <View style={{ marginTop: 5 }}>
            <Accordion
              sections={orderDetails.sub_orders !== null &&
                orderDetails.sub_orders ? orderDetails.sub_orders
                : []}
              activeSections={subOrderIndex}
              renderHeader={renderHeader}
              renderContent={renderContent}
              onChange={updateSections}
            />
          </View>
          )}

        {isCollectionScreen ? null : (
          <OrderSummaryCard
            fullData={orderDetails}
            subOrderData={orderDetails.sub_orders !== null &&
              orderDetails.sub_orders ? orderDetails.sub_orders
              : []}
            isCollectionScreen={false} />
        )}

        {!devRequest && <PriceCard order_Details={orderDetails} />}
        {!devRequest && <View style={styles.background}>
          <View style={styles.expBoxJust}>
            <View style={styles.leftText}>
              <Text style={styles.method}>{appTexts.ORDER.payment}</Text>
            </View>
            <View style={styles.rightText}>
              <View style={styles.cardImage}>
                <Image
                  resizeMode="contain"
                  source={images.card}
                  style={styles.cardImage}
                />
              </View>
              <View style={styles.onlineTextWarp}>
                <Text style={styles.onlineText}>
                  {orderDetails.payment_method === 'COD' ? appTexts.ORDER.cod : appTexts.ORDER.online}</Text>
              </View>
            </View>
          </View>
        </View>}
      </ScrollView>}
      <ModalComponent
        isModalComponentVisible={isModalComponentVisible}
        modalSwipePress={closeModalComponent}
        modalImage={images.deliverModal}
        modalTitle={isCollectionScreen ? appTexts.MODAL.reqcollect : appTexts.MODAL.reqdeliver}
        buttonLabelOne={appTexts.MODAL.no}
        buttonLabelTwo={appTexts.MODAL.yes}
        onYesClick={onYesClick}
        onNoClick={onNoClick}
      />
    </View>
  );
};
OrderDetailsView.propTypes = {

  onYesClick: PropTypes.func,
  onNoClick: PropTypes.func,
  setSubOrderIndex: PropTypes.func,
  setRequestId: PropTypes.func,

};
export default OrderDetailsView;
