import {StyleSheet, I18nManager} from 'react-native';

import globals from '../../lib/globals';

const images = {
  dateIcon: require('../../assets/images/order/date.png'),
  timeIcon: require('../../assets/images/order/time.png'),
  buttonArrow: require('../../assets/images/cart/Arrow-down.png'),
  card: require('../../assets/images/checkout/Card.png'),
  deliverModal: require('../../assets/images/modal/deliverModal.png'),
  down:require('../../assets/images/cart/Down.png'),
  up:require('../../assets/images/cart/Up.png'),
  tickIcon: require('../../assets/images/ChooseLanguage/checkbox.png'),
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: globals.COLOR.red,
  },
  viewLoc: {
    color: '#1492E6',
    minWidth: 450,
    flexDirection: 'row-reverse',
    textDecorationLine: 'underline',
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
  },
  orderContainer: {
    // borderBottomWidth: 1,
    borderBottomColor: 'gray',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: globals.COLOR.screenBackground,
    paddingHorizontal: 20,
    paddingVertical: 20,
    top: 3,
  },
  orderContainerForRequest: {
    // borderBottomWidth: 1,
    borderBottomColor: 'gray',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: globals.COLOR.screenBackground,
    paddingHorizontal: 20,
    paddingVertical: 20,
    top: 3,
    height: 1000,
  },
  orderTextHead: {
    color: '#707070',
    fontFamily: globals.FONTS.cairoRegular,
    fontSize: 14,
    textAlign: 'left',
  },
  orderId: {
    color: '#434A5E',
    fontFamily: globals.FONTS.cairoSemiBold,
    fontSize: 16,
    textAlign: 'left',
  },
  locationText: {
    color: '#434A5E',
    fontFamily: globals.FONTS.cairoSemiBold,
    fontSize: 16,
    textAlign: 'left',
    width: 250
  },
  bottomMarkWarp: {
    paddingHorizontal: 25,
    paddingVertical: 5,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: globals.COLOR.red,
  },
  bottomButtonWarp: {
    marginTop: 15,
    alignItems: 'center',
    marginBottom: 15,
  },
  bottomButtonLabel: {
    fontSize: 14,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.red,
  },
  bottomIMG: {
    width: 20,
    height: 20,
    borderRadius: 30,
    marginRight: 10,
  },
  bottomButtonInnerWarp: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.red,
    borderWidth: 1,
    paddingHorizontal: 28,
    paddingVertical: 2,
    borderRadius: 8,
  },
  markButtonLabel: {
    fontSize: 13,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.screenBackground,
  },
  orderInnerWarp: {
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'space-between',
  },
  orderDateWarp: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  orderTimeWarp: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 30,
  },
  dateAndTime: {
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    fontSize: 14,
    marginLeft: 10,
  },
  headerHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginBottom: 3,
    backgroundColor: globals.COLOR.screenBackground,
  },
  headerHeadText: {
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.cairoSemiBold,
    fontSize: 14,
  },
  headerHeadImg: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  background: {
    backgroundColor: 'white',
    paddingBottom: '24%',
  },
  expBoxJust: {
    width: '92%',
    marginLeft: '4%',
    marginRight: '4%',
    paddingLeft: '2%',
    paddingRight: '2%',
    borderWidth: 0.5,
    borderStyle: 'dotted',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 70,
    marginBottom: '10%',
    backgroundColor: 'white',
  },
  method: {
    color: globals.COLOR.greyText,
    fontSize: 15,
    fontFamily: globals.FONTS.cairoSemiBold,
  },
  rightText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardImage: {
    width: 30,
    height: 30,
  },
  onlineTextWarp: {
    marginLeft: 10,
  },
  onlineText: {
    color: globals.COLOR.text,
    fontSize: 16,
    fontFamily: globals.FONTS.cairoSemiBold,
    // textTransform: 'uppercase',
  },
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    backgroundColor: globals.COLOR.screenBackground,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  buttonWarpContainer: {
    backgroundColor: globals.COLOR.red,
    paddingHorizontal: 50,
    paddingVertical: 10,
    borderRadius: 10,
  },
  buttonTextLabel: {
    color: 'white',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    fontSize: globals.FONTSIZE.fontSizeSixteen,
  },
});

export {images, styles};
