import React, {useState, useEffect,useRef} from 'react';
import {BackHandler} from 'react-native';
import {connect} from 'react-redux';
import OtpView from './OtpView';

import {bindActionCreators} from 'redux';
import _ from 'lodash';
import functions from '../../lib/functions';
import appTexts from '../../lib/appTexts';
import * as LoginActions from '../../actions/LoginActions';
import messaging from '@react-native-firebase/messaging';

import {useDispatch} from 'react-redux';


const OtpScreen = props => {
  const { mobileNumber } = props.route.params;
  console.log('NUMBER:'+mobileNumber);
  //const dispatch = useDispatch();
  const [otp, setOtp] = useState('');
  const [loader, setLoader] = useState(false);
 const [fcm_token, setFcmToken] = useState('');
  const [success, setSuccess] = useState(false);

  /** will focus */
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
    setSuccess(false);
    });

    return () => {
      unsubscribe();
    };
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    console.log(props.token);
    BackHandler.addEventListener(
      'hardwareBackPress',
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      }),
    );
  };
  useEffect(() => {
    let isMounted = true;
    const fetchToken = async () => {
      console.log('erro above requestuser');
      const _token = await requestUserPermission();
      if (_token != '' && isMounted) {
        setFcmToken(_token);
      }
    };
    fetchToken();
    return () => {
      isMounted = false;
    };
  });

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      return await getFcmToken();
    }
    return '';
  };

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log('fcm token at otp', fcmToken);
      return fcmToken;
    }

    return '';
  };

  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    // console.log('uodate console',props.userData, props.isLogged);
    /** user logged in */
    if (
      _.get(props, 'userData') &&
      _.get(props, 'userData.success') &&
      success == false
    )
    {
      setSuccess(true);
      props.navigation.navigate('TabNavigator', {screen: 'HomeScreen'});
      
      }
       // /** Otp validation failed */
    if (
      _.get(props, 'otpAPIError') &&
      _.get(props, 'otpAPIError.success') === false
    ) {
      setLoader(false);
      functions.displayToast(
        'error',
        'top',
        appTexts.LOGIN_TOASTMESSAGES.error,
        _.get(props, 'otpAPIError.error.msg'),
      );
      props.resetOtpError(null);
    }
     // /** resent otp success */
     if (_.get(props, 'resendOtpData.success') === true) {
      setLoader(false);
      functions.displayToast(
        'success',
        'top',
        appTexts.LOGIN_TOASTMESSAGES.success,
        _.get(props, 'resendOtpData.msg'),
      );
      props.resendOtpReset(null);
    }
    };
 

  const resendOtpPress = () => {
      
      setLoader(true);
    let apiParam = {
      "phone": mobileNumber,
    
    };
    props.resendOtp(apiParam);
  };

  const loginButtonPress = () => {
   // props.navigation.navigate('TabNavigator', {screen: 'HomeScreen'});
    setLoader(true);
    if (otp.trim() === '') {
      setLoader(false);
      functions.displayToast(
        'error',
        'top',
        appTexts.LOGIN_TOASTMESSAGES.error,
        appTexts.LOGIN_TOASTMESSAGES.otp,
      );
    } else {
      let params = {
        
          phone: mobileNumber,
          otp: otp,
          fcm_token: fcm_token,
      
      };
      props.doLogin(params);
      console.log('params',params)
    }
   // props.navigation.navigate('TabNavigator', {screen: 'HomeScreen'});
  };

  const onBackButtonClick = () => {
    props.navigation.goBack();
  };

  return (
    <OtpView
      loginButtonPress={loginButtonPress}
      onBackButtonClick={onBackButtonClick}
      setOtp={setOtp}
      mobileNumber={mobileNumber}
      isLoading={props.isLoading}
      resendOtpPress={resendOtpPress}
      //send_to={props.route.params.type}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
     userData: state.loginReducer?.userData || {},
     token:_.get(state, 'loginReducer.userData.data.access_token', ''),
     isLogged: _.get(state, 'loginReducer.isLogged', ''),
     resendOtpData: _.get(state, 'loginReducer.resendOtp', ''),
     otpAPIError: _.get(state, 'loginReducer.otpAPIError', ''),
     isLoading: _.get(state, 'loginReducer.isLoading', ''),
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
       doLogin: LoginActions.doLogin,
       resendOtp: LoginActions.resendOtp,
       resendOtpReset: LoginActions.resendOtpServiceActionSuccess,
       resetOtpError: LoginActions.loginServiceError,
    },
    dispatch,
  );
};


const otpScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps,
)(OtpScreen);

otpScreenWithRedux.navigationOptions = ({navigation}) => ({
  header: null,
});

export default otpScreenWithRedux;
