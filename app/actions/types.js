

export const COLLECTION_LOADING = 'collection_loading';
export const SERVICE_LOADING = 'service_loading';
export const SERVICE_ERROR = 'service_error';
export const GET_ADDRESS_SUCCESS = 'get_address_success';
export const GET_ADDRESS_DETAILS_SUCCESS = 'get_address_details_success';
export const RESET_ERROR = 'reset_error';
export const SINGLE_DATA_SUCCESS = 'single_data_success';


//login
export const LOGIN_SUCCESS = 'login_success'
export const LOGIN_ERROR = 'login_error'
export const LOGIN = 'login'

//get otp
export const LOGIN_SERVICE_LOADING = 'login_service_loading';
export const GET_OTP_SUCCESS = 'get_otp_success';
export const GET_OTP_SERVICE_ERROR = 'get_otp_service_error';
export const RESET_OTP_ERROR = 'reset_otp_error';
export const RESEND_OTP_SUCCESS = 'resend_otp_success';

//privacy
export const PRIVACY_SUCCESS = 'privacy_success';
export const PRIVACY_ERROR = 'privacy_error';
export const PRIVACY_LOADING = 'privacy_loading';
//about as
export const ABOUT_SUCCESS = 'about_success';
export const ABOUT_ERROR = 'about_error';
export const ABOUT_LOADING = 'about_loading';

//faq
export const FAQS_SUCCESS = 'faqs_success';
export const FAQS_ERROR = 'faqs_error';
export const FAQS_LOADING = 'faqs_loading';

//terms&Conditio
export const TERMS_SUCCESS = 'terms_success';
export const TERMS_LOADING = 'terms_loading';
export const TERMS_ERROR = 'terms_error';

//howtouse
export const HOWTOUSE_LOADING = 'howtouse_loading';
export const HOWTOUSE_SUCCESS = 'howtouse_success';
export const HOWTOUSE_ERROR = 'howtouse_error';

//support
export const SUPPORT_CHAT_SERVICE_LOADING = 'support_chat_service_loading';
export const SUPPORT_CHAT_SERVICE_SUCCESS = 'support_chat_service_success';
export const SUPPORT_CHAT_SERVICE_ERROR = 'support_chat_service_error';
export const CLEAR_SUPPORT_CHAT_MSG = 'clear_support_chat_msg';

//EditUser
export const EDIT_USER_SUCCESS = 'edit_user_success';
export const EDIT_USER_ERROR = 'edit_user_error';
export const LOGOUT_ERROR_RESET = 'reset_logout_error';
export const EDIT_USER_LOADING = 'edit_user_loading';


//TODAYS COLLECTION
export const TODAYS_COLLECTION_SUCCESS = 'todays_collection_success';
export const TODAYS_COLLECTION_ERROR = 'todays_collection_error';

//ALL COLLECTION
export const ALL_COLLECTION_SUCCESS = 'all_collection_success';
export const ALL_COLLECTION_ERROR = 'all_collection_error';

//TODAYS ORDERS
export const TODAYS_ORDERS_SUCCESS = 'todays_orders_success';
export const TODAYS_ORDERS_ERROR = 'todays_orders_error';

//ACTIVE ORDERS
export const ACTIVE_ORDERS_SUCCESS = 'active_orders_success';
export const ACTIVE_ORDERS_ERROR = 'active_orders_error';

//DELIVERED ORDERS
export const DELIVERED_ORDERS_SUCCESS = 'delivered_orders_success';
export const DELIVERED_ORDERS_ERROR = 'delivered_orders_error';

//DELIVERED ORDERS
export const ORDER_DETAILS_SUCCESS = 'order_details_success';
export const ORDER_DETAILS_ERROR = 'order_details_error';
export const DELIVERED_ORDER_SUCCESS = 'delivered_order_success';
export const DELIVERED_ORDER_ERROR = 'delivered_order_error';
//COLLECT ORDERS
export const COLLECT_ORDER_SUCCESS = 'collect_order_success';
export const COLLECT_ORDER_ERROR = 'collect_order_error';
export const RESET_COLLECT_SUCCESS = 'reset_collect_success';

//TODAYS REQUEST COLLECTIONS
export const TODAYS_REQUEST_COLLECTION_SUCCESS = 'todays_request_collection_success';
export const TODAYS_REQUEST_COLLECTION_ERROR = 'todays_request_collection_error';

//ALL REQUEST COLLECTIONS
export const ALL_REQUEST_COLLECTION_SUCCESS = 'all_request_collection_success';
export const ALL_REQUEST_COLLECTION_ERROR = 'all_request_collection_error';

//ALL REQUEST DETAILS COLLECTIONS
export const REQUEST_DETAILS_SUCCESS = 'request_details_success';
export const REQUEST_DETAILS_ERROR = 'request_details_error';

//REQUEST COLLECTIONS
export const COLLECTION_REQUEST_SUCCESS = 'collection_request_success';
export const COLLECTION_REQUEST_ERROR = 'collection_request_error';
export const RESET_REQUEST_COLLECT_SUCCESS = 'reset_request_collect_success';

//DELIVRED REQUEST DELIVERED 
export const DELIVERED_REQUEST_SUCCESS = 'delivered_request_success';
export const DELIVERED_REQUEST_ERROR = 'delivered_request_error';

//TODAYS REQUEST DELIVERED 
export const TODAYS_REQUEST_SUCCESS = 'todays_request_success';
export const TODAYS_REQUEST_ERROR = 'todays_request_error';

//ACTIVE REQUEST DELIVERED 
export const ACTIVE_REQUEST_SUCCESS = 'active_request_success';
export const ACTIVE_REQUEST_ERROR = 'active_request_error';



export const LOGOUT_SUCCESS = 'logout_success';
export const RESET_LOGOUT_DATA = 'reset_logout_data';
//Notification
export const NOTIFICATION_COUNT = 'notification_count';
