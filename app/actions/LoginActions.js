import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"




export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});

export const loginServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING,
  });

export function loginServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: responseData
	};
}
export const loginServiceError = error => ({
	type: ActionTypes.LOGIN_ERROR,
	error: error,
  });


export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}
export function getOtpSuccess(responseData) {
	return {
	  type: ActionTypes.GET_OTP_SUCCESS,
	  responseData: responseData,
	};
  }
  export const otpServiceError = error => ({
	type: ActionTypes.GET_OTP_SERVICE_ERROR,
	error: error,
  });
  export function resendOtpServiceActionSuccess(responseData) {
	return {
	  type: ActionTypes.RESEND_OTP_SUCCESS,
	  responseData: responseData,
	};
  }


export function getOtp(params) {
	console.log('inside actions');
	return async dispatch => {
	  //console.debug('inside getOtp actions loading', this.isLoading);
	  dispatch(loginServiceActionLoading());
	//   console.debug(
	// 	'inside getOtp actions loading after dispatch',
	// 	this.isLoading,
	//   );
	  await axios
		.post(apiHelper.getOtpAPI(), params)
		.then(response => {
		  console.log('inside response', response.data);
		  dispatch(getOtpSuccess(response.data));
		})
		.catch(error => {
		  console.log('error', error.response.data);
		  dispatch(otpServiceError(error.response.data));
		});
	};
  }
  export function resendOtp(apiParam) {
	return async dispatch => {
	  dispatch(apiServiceActionLoading());
	  console.log('inside try login action resed otp');
	  await axios
		.post(apiHelper.resendOtpAPI(), apiParam)
		.then(response => {
		  dispatch(resendOtpServiceActionSuccess(response.data));
		})
		.catch(error => {
			console.log('resed otp api error',error)
		  //dispatch(apiServiceActionError(error.response.data.message));
		});
	};
  }

  export function doLogin(params) {
	return async dispatch => {
		console.log('inside actions');
	  dispatch(apiServiceActionLoading());
	  await axios
		.post(apiHelper.loginAPI(), params, apiHelper.getLoginAPIHeader())
		.then(response => {
		  dispatch(loginServiceActionSuccess(response.data));
		  console.log('otp response at actions',response.data);
		})
		.catch(error => {
		  dispatch(loginServiceError(error.response.data));
		  console.log(error.response.data)
		});
	};
  }

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}

