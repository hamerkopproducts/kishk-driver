import * as ActionTypes from './types';
import axios from 'react-native-axios';
import globals from '../lib/globals';
import { I18nManager } from 'react-native';
import apiHelper from '../lib/apiHelper';
export const apiServiceActionLoading = () => (
  {
  type: ActionTypes.COLLECTION_LOADING,
});

export function GetTodayRequestCollectionSuccess(responseData) {
  return {
    type: ActionTypes.TODAYS_REQUEST_COLLECTION_SUCCESS,
    responseData: responseData,
  };
}
export function GetTodayRequestCollectionError(responseData) {
  return {
    type: ActionTypes.TODAYS_REQUEST_COLLECTION_ERROR,
    responseData: responseData,
  };
}
export function GetAllRequestCollectionSuccess(responseData) {
  return {
    type: ActionTypes.ALL_REQUEST_COLLECTION_SUCCESS,
    responseData: responseData,
  };
}
export function GetAllRequestCollectionError(responseData) {
  return {
    type: ActionTypes.ALL_REQUEST_COLLECTION_ERROR,
    responseData: responseData,
  };
}
export function GetTodayCollectionSuccess(responseData) {
  return {
    type: ActionTypes.TODAYS_COLLECTION_SUCCESS,
    responseData: responseData,
  };
}
export function GetAllCollectionSuccess(responseData) {
  return {
    type: ActionTypes.ALL_COLLECTION_SUCCESS,
    responseData: responseData,
  };
}

export function GetTodayCollectionError(responseData) {
  return {
    type: ActionTypes.TODAYS_COLLECTION_ERROR,
    responseData: responseData,
  };
}
export function GetAllCollectionError(responseData) {
  return {
    type: ActionTypes.ALL_COLLECTION_ERROR,
    responseData: responseData,
  };
}
export function doresetCollcetSuccess(responseData) {
  return {
    type: ActionTypes.RESET_COLLECT_SUCCESS,
    responseData: null,
  };
}
export function doresetRequestCollcetSuccess(responseData) {
  return {
    type: ActionTypes.RESET_REQUEST_COLLECT_SUCCESS,
    responseData: null,
  };
}
export function GetOrdersDetailsSuccess(responseData) {
  return {
    type: ActionTypes.ORDER_DETAILS_SUCCESS,
    responseData: responseData,
  };
}

export function GetOrdersDetailsError(responseData) {
  return {
    type: ActionTypes.ORDER_DETAILS_ERROR,
    responseData: responseData,
  };
}
export function GetRequestDetailsSuccess(responseData) {
  return {
    type: ActionTypes.REQUEST_DETAILS_SUCCESS,
    responseData: responseData,
  };
}

export function GetRequestDetailsError(responseData) {
  return {
    type: ActionTypes.REQUEST_DETAILS_ERROR,
    responseData: responseData,
  };
}
export function GetcollectOrderSuccess(responseData) {
  return {
    type: ActionTypes.COLLECT_ORDER_SUCCESS,
    responseData: responseData,
  };
}

export function GetcollectOrderError(responseData) {
  return {
    type: ActionTypes.COLLECT_ORDER_ERROR,
    responseData: responseData,
  };
}
export function GetDeliveredOrderSuccess(responseData) {
  return {
    type: ActionTypes.DELIVERED_ORDER_SUCCESS,
    responseData: responseData,
  };
}

export function GetDeliveredOrderError(responseData) {
  return {
    type: ActionTypes.DELIVERED_ORDER_ERROR,
    responseData: responseData,
  };
}
export function GetcollectRequestSuccess(responseData) {
  return {
    type: ActionTypes.COLLECTION_REQUEST_SUCCESS,
    responseData: responseData,
  };
}

export function GetcollectRequestError(responseData) {
  return {
    type: ActionTypes.COLLECTION_REQUEST_ERROR,
    responseData: responseData,
  };
}

export function doGetTodayRequestCollection(params, token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getTodaysRequestCollection(params), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetTodayRequestCollectionSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetTodayRequestCollectionError(error.response.data.error));
      });
  };
}
export function doGetAllRequestCollection(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getAllRequestCollection(), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('ALLL  ******* DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetAllRequestCollectionSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetAllRequestCollectionError(error.response.data.error));
      });
  };
}
export function doGetTodaysCollection(params, token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getTodaysCollection(params), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetTodayCollectionSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetTodayCollectionError(error.response.data.error));
      });
  };
}
export function doGetAllCollection(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getAllCollection(), apiHelper.getAPIHeader(token))
      .then(response => {
        dispatch(GetAllCollectionSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetAllCollectionError(error.response.data.error));
      });
  };
}

export function doGetOrdersDetails(id,token,flag) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getOrdersDetails(id,flag), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DATA****************'+JSON.stringify(response.data));
        dispatch(GetOrdersDetailsSuccess(response.data));
      })
      .catch(error => {
        // console.log('DATA****************'+JSON.stringify(error.response.data.error));
        dispatch(GetDeliveredOrdersError(error.response.data.error));
      });
  };
}
export function doGetRequestDetails(id,token,flag) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getRequestDetails(id,flag), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DATA****************'+JSON.stringify(response.data));
        dispatch(GetRequestDetailsSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetRequestDetailsError(error.response.data.error));
      });
  };
}

export function doRequestCollcet(params, token, order_id, flag) {
  let request_id = params
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .post(apiHelper.setcollectRequest(flag),params, apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('****************'+JSON.stringify(response.data));
        dispatch(doGetRequestDetails(order_id, token, flag))
        dispatch(GetcollectRequestSuccess(response.data));
      })
      .catch(error => {
        // console.log('ERRORRRRR  ****************'+JSON.stringify(error.response.data));
        dispatch(GetcollectRequestError(error.response.data.error));
      });
  };
}
export function collectOrder(params, token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .post(apiHelper.setcollectOrder(),params, apiHelper.getAPIHeader(token))
      .then(response => {
        
        dispatch(doGetOrdersDetails(params.order_id, token))
        dispatch(GetcollectOrderSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetcollectOrderError(error.response.data.error));
      });
  };
}

export function deliverOrder(params, token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .post(apiHelper.setDeliverOrder(),params, apiHelper.getAPIHeader(token))
      .then(response => {
        dispatch(doGetOrdersDetails(params.order_id, token))
        dispatch(GetDeliveredOrderSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetDeliveredOrderError(error.response.data.error));
      });
  };
}
export function addressdetailsActionSuccess(responseData) {
  return {
    type: ActionTypes.GET_ADDRESS_DETAILS_SUCCESS,
    responseData: responseData,
  };
}



