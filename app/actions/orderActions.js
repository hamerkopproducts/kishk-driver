import * as ActionTypes from './types';
import axios from 'react-native-axios';
import globals from '../lib/globals';
import { I18nManager } from 'react-native';
import apiHelper from '../lib/apiHelper';
export const apiServiceActionLoading = () => ({
  type: ActionTypes.LOGIN_SERVICE_LOADING,
});

export function GetTodayOrdersSuccess(responseData) {
  return {
    type: ActionTypes.TODAYS_ORDERS_SUCCESS,
    responseData: responseData,
  };
}

export function GetTodayOrdersError(responseData) {
  return {
    type: ActionTypes.TODAYS_ORDERS_ERROR,
    responseData: responseData,
  };
}
export function GetActiveOrdersSuccess(responseData) {
  return {
    type: ActionTypes.ACTIVE_ORDERS_SUCCESS,
    responseData: responseData,
  };
}

export function GetActiveOrdersError(responseData) {
  return {
    type: ActionTypes.ACTIVE_ORDERS_ERROR,
    responseData: responseData,
  };
}

export function GetDeliveredOrdersSuccess(responseData) {
  return {
    type: ActionTypes.DELIVERED_ORDERS_SUCCESS,
    responseData: responseData,
  };
}

export function GetDeliveredOrdersError(responseData) {
  return {
    type: ActionTypes.DELIVERED_ORDERS_ERROR,
    responseData: responseData,
  };
}
export function GetTodayRequestSuccess(responseData) {
  return {
    type: ActionTypes.TODAYS_REQUEST_SUCCESS,
    responseData: responseData,
  };
}

export function GetTodayRequestError(responseData) {
  return {
    type: ActionTypes.TODAYS_REQUEST_ERROR,
    responseData: responseData,
  };
}
export function GetActiveRequestSuccess(responseData) {
  return {
    type: ActionTypes.ACTIVE_REQUEST_SUCCESS,
    responseData: responseData,
  };
}

export function GetActiveRequestError(responseData) {
  return {
    type: ActionTypes.ACTIVE_REQUEST_ERROR,
    responseData: responseData,
  };
}

export function GetDeliveredRequestSuccess(responseData) {
  return {
    type: ActionTypes.DELIVERED_REQUEST_SUCCESS,
    responseData: responseData,
  };
}

export function GetDeliveredRequestError(responseData) {
  return {
    type: ActionTypes.DELIVERED_REQUEST_ERROR,
    responseData: responseData,
  };
}



export function doGetTodaysOrders(params,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getTodaysOrders(params), apiHelper.getAPIHeader(token))
      .then(response => {
        dispatch(GetTodayOrdersSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetTodayOrdersError(error.response.data.error));
      });
  };
}
export function doGetAllOrders(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getActiveOrders(), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetActiveOrdersSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetActiveOrdersError(error.response.data.error));
      });
  };
}
export function doGetDeliveredOrders(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getDeliveredOrders(), apiHelper.getAPIHeader(token))
      .then(response => {
        dispatch(GetDeliveredOrdersSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetDeliveredOrdersError(error.response.data.error));
      });
  };
}//04772247210
//doGetTodaysRequest,//doGetAllRequest, doGetDeliveredRequest
export function doGetTodaysRequest(params,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getTodaysRequest(params), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('TODAY  DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetTodayRequestSuccess(response.data));
      })
      .catch(error => {
        // console.log('ERROR TODAY  DATA@@@@@@@@@'+JSON.stringify(error.response));
        dispatch(GetTodayRequestError(error.response.data.error));
      });
  };
}
export function doGetAllRequest(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getActiveRequest(), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('ALL DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetActiveRequestSuccess(response.data));
      })
      .catch(error => {
        dispatch(GetActiveRequestError(error.response.data.error));
      });
  };
}
export function doGetDeliveredRequest(token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios
      .get(apiHelper.getDeliveredRequest(), apiHelper.getAPIHeader(token))
      .then(response => {
        // console.log('DELIVERED DATA@@@@@@@@@'+JSON.stringify(response.data));
        dispatch(GetDeliveredRequestSuccess(response.data));
      })
      .catch(error => {
        // console.log('DELIVERED DATA@@@@@@@@@'+JSON.stringify(error));
        dispatch(GetDeliveredRequestError(error.response.data.error));
      });
  };
}

export function addressdetailsActionSuccess(responseData) {
  return {
    type: ActionTypes.GET_ADDRESS_DETAILS_SUCCESS,
    responseData: responseData,
  };
}

