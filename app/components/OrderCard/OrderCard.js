import React, { useEffect, useState } from 'react';
import { View, Text, Image, TouchableOpacity, AppState, I18nManager, FlatList } from 'react-native';
import { styles, images } from './styles';
import appTexts from '../../lib/appTexts';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as collectionActions from '../../actions/collectionActions';
import ModalComponent from '../../components/ModalComponent';
import _ from 'lodash'

const OrderCard = (props) => {
  const {
    isAccordian,
    isCollectionButtonHighlighted,
    section,
  } = props;
  console.log("##################", section)
  const [isModalComponentVisible, setIsModalComponentVisible] = useState(false);
  const [subId, setSubId] = useState('');
  const [orderId, setOrderId] = useState('');

  const openModalComponent = (order_id, sub_id) => {
    setSubId(sub_id)
    setOrderId(order_id)
    setIsModalComponentVisible(!isModalComponentVisible)
  };
  const onYesClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
    let params = {
      "order_id": orderId,
      "item_id": subId,
    }
    props.collectTheOrder(params, props.token);
  };
  const onNoClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
  };
  const renderItem = ({ item, index }) => (
    <View>
      <View style={styles.cardBorder}>
    <View style={styles.innerContainer}>
      <Image source={{ uri: item.cover_image }} style={styles.orderIMG} />
      <View style={styles.innerHead}>
        <View style={styles.innerTitleHead}>
          <Text style={styles.innerTitle} numberOfLines={2}>
          {I18nManager.isRTL ? item.product.lang[1].name : item.product.lang[0].name}
          </Text>
        </View>
        <View style={styles.bottomTextWarp}>
          <View style={styles.bottomInnerWarp}>
            <Text style={styles.sarText}>{appTexts.ORDER.sar}</Text>
            <Text style={styles.sarValue}>{item.item_price}</Text>
            {I18nManager.isRTL ?
              <Text style={styles.quantity}>{item.item_count}X</Text> :
              <Text style={styles.quantity}>X{item.item_count}</Text>}
          </View>
          <View style={styles.bottomInnerWarp}>
            <Text style={styles.sarText}>{appTexts.ORDER.sar}</Text>
            <Text style={styles.sarValue}>{item.grant_total}</Text>
          </View>
        </View>
      </View>
    </View>
     <View style={styles.cardLine}></View>
     <View style={styles.bottomButtonWarp}>
       {item.item_status !== 'Item Collected' ?
         <TouchableOpacity style={styles.bottomMarkWarp}
           onPress={() => {
            openModalComponent(item.order_id, item.id);
           }}
         >
           <Text style={styles.markButtonLabel}>{appTexts.ORDER.markCollected}</Text>
         </TouchableOpacity> :
         <View style={styles.bottomButtonInnerWarp}>
           <Image source={images.tickIcon} style={styles.bottomIMG} />
           <Text style={styles.bottomButtonLabel}>{appTexts.ORDER.collected}</Text>
         </View>}
     </View>
     </View>
     </View>
  );
  return (
    <View style={isAccordian ? styles.accordianContainer : styles.container}>
      <FlatList
        data={section}
        extraData={section}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
      />
       <ModalComponent
        isModalComponentVisible={isModalComponentVisible}
        // modalSwipePress={closeModalComponent}
        modalImage={images.collectModal}
        modalTitle={appTexts.MODAL.collect}
        buttonLabelOne={appTexts.MODAL.no}
        buttonLabelTwo={appTexts.MODAL.yes}
        onYesClick = {onYesClick}
        onNoClick = {onNoClick}
      />
    </View>
  );
};

const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token', ''),
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

    collectTheOrder: collectionActions.collectOrder,

  }, dispatch)
};

const OrderCardScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderCard);

OrderCardScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});
export default OrderCardScreenWithRedux;
