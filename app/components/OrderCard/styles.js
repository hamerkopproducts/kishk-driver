import {StyleSheet, I18nManager} from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import globals from '../../lib/globals';
const images = {
  demo: require('../../assets/images/products/img3.png'),
  tickIcon: require('../../assets/images/ChooseLanguage/checkbox.png'),
  collectModal: require('../../assets/images/modal/collectModal.png'),
  // buttonArrow: require('../../assets/images/cart/arrow.png'),
};
const styles = StyleSheet.create({
  container: {
  // marginHorizontal:25,
   // marginVertical: 5,
   //backgroundColor:'red',
    width:widthPercentageToDP('95%'),
    alignItems:'center',
    justifyContent:'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
    backgroundColor: globals.COLOR.screenBackground,
    borderRadius: 20,
    shadowOffset: {
      width: 1,
      height: 1,
    },
        shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1 : 3,
    borderColor: globals.COLOR.lightgrey,
  },
  accordianContainer:{
  marginHorizontal:10,
  marginVertical: 5,
   width:widthPercentageToDP('95%'),
   alignItems:'center',
   justifyContent:'center',
   paddingHorizontal: 5,
   paddingVertical: 10,
   
  },
  orderIMG: {
    width: 100,
    height: 100,
    borderRadius: 15,
  },
  cardBorder:{
    backgroundColor:'white',
    
    margin: 10,
   borderRadius:20,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 6 : 3,
    borderColor: globals.COLOR.lightgrey,
    padding: 5,
      },
  
  innerContainer: {
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'center',
    marginLeft: I18nManager.isRTL ?  '10%' : 0,
    
  },
  innerHead: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  innerTitleHead: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  customWrap: {
    // alignItems: 'center',
    // justifyContent: 'center',
    // padding: 5,
    // backgroundColor: globals.COLOR.ItemColor,
    // borderRadius: 5,
    // marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:globals.COLOR.ItemColor,
    borderRadius: 5,
    minWidth:60,
   // marginLeft: 10,
    position: 'absolute',
    right:10 ,
    
    top: 5,
    paddingHorizontal: 5,       
  },
  customText: {
    fontSize: I18nManager.isRTL ? 10 : 12,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.black,
    
  },
  innerTitle: {
    width:'60%',
    fontSize: 14,
    fontFamily: globals.FONTS.cairoSemiBold,
    color: globals.COLOR.black,
  },
  bottomTextWarp: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  bottomInnerWarp: {
    flexDirection: 'row',
    marginRight: 5,
    alignItems: 'center',
  },
  sarText: {
    marginRight: 5,
    fontSize: I18nManager.isRTL ? 10 : 15,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.greyText,
  },
  sarValue: {
    marginRight: 5,
    fontSize: globals.FONTSIZE.fontSizeSixteen,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.black,
  },
  quantity: {
    fontSize: globals.FONTSIZE.fontSizeSixteen,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.black,
  },
  cardLine: {
    width: '100%',
    height: 1,
    backgroundColor: globals.COLOR.border,
    marginTop: 15,
  },
  bottomButtonWarp: {
    marginTop: 15,
    alignItems: 'center',
    marginBottom: 15,
  },
  bottomButtonInnerWarp: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.red,
    borderWidth: 1,
    paddingHorizontal: 28,
    paddingVertical: 2,
    borderRadius: 8,
  },
  bottomIMG: {
    width: 20,
    height: 20,
    borderRadius: 30,
    marginRight: 10,
  },
  bottomButtonLabel: {
    fontSize: 14,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.red,
  },
  bottomMarkWarp: {
    paddingHorizontal: 25,
    paddingVertical: 5,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: globals.COLOR.red,
  },
  markButtonLabel: {
    fontSize: 13,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.screenBackground,
  },
});

export {images, styles};
