import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backImage:require("../../assets/images/ChooseLanguage/Back.png"),
  crossImage:require("../../assets/images/header/cross.png"),
  love:require('../../assets/images/header/love-b.png'),
  logo: require("../../assets/images/header/logo.png"),
  rightIcon: require("../../assets/images/header/rightIcon.png"),
  searchIcon: require("../../assets/images/header/search.png"),
  share:require("../../assets/images/header/Share.png"),
  bellIcon:require("../../assets/images/header/bbell.png"),
  bell:require('../../assets/images/header/bell.png'),
  profileIcon:require('../../assets/images/Profile/profile.png'),
 };

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    height: globals.INTEGER.headerHeight,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: globals.COLOR.headerColor,
    marginTop:hp("2%"),
    //marginTop:'4%'
  },
  centre:{
marginTop:'2%',
alignItems:'center',
justifyContent:'center',

  },

  orderContainer:{
   // flex: 0.12,
    height: globals.INTEGER.headerHeight + 20,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: globals.COLOR.headerColor,
   // marginTop:hp("2%"),
  },
  leftIconContainer:{
   marginTop:hp("1%"),
    paddingLeft: globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    paddingRight:globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
   // height : "100%",
    left:0,
    flexDirection:'row',
   // paddingLeft:10,
    },
  leftIconContainercartnew:{
    marginTop:hp("1%"),
     paddingLeft: globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
     paddingRight:globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
     justifyContent: "center",
     alignItems: "center",
     position: "absolute",
    // height : "100%",
     left:0,
     flexDirection:'row',
     //paddingLeft:10,
   
     
   },
  headerTitlesContainer: {
    flex: 1,
    left:20,
      position: "absolute",
    height : "100%",
    marginTop: 5
  
  },
  center:{
    alignItems:'center',
    justifyContent:'center',
  },
  notifyText: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 8,
  },
  topHeader: {
    alignItems: "stretch",
    width:24,
    height:24,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] 
  },
  topHeaderClose: {
    alignItems: "stretch",
    width:24,
    height:24,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] ,
    tintColor:globals.COLOR.black
  },
    headerTitleContainer: {
    // justifyContent: "center",
    // alignItems: "flex-start",
    height : "100%",
    // alignSelf:'flex-start',
    marginTop:hp("3.5%"),
   marginRight: '25%'

  },
  headerTitleText: {
    textAlign: "left",
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold :globals.FONTS.cairoBold,
    //fontSize: globals.SCREEN_SIZE.width * 0.04,
    fontSize:18
  },
  headerLogo:{
    height: 20,
    width:120,
    
  },
  rightIconContainer:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    right: 0,
    //marginTop:hp("2%"),
  },
  SignTitleText:{
    textAlign: "right",
    
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoSemiBold,
    //fontSize: globals.SCREEN_SIZE.width * 0.04,
    fontSize:15
  },
  rightWordContainer:{
    flex: 1,
    width:"50%",
    // flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:globals.COLOR.lightgrey,
    position: "absolute",
    height : "60%",
    left: "110%",
   // marginTop:hp("2%"),
    borderRadius:5
  },
  progileIconContainer:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center",
    marginTop:hp("2%"),
  },
  rightIcon: {
    marginRight: 25,
    width:25,
    height:26,
    resizeMode:'contain',
    
  },
  profileIcon: {
    width:22,
    height:22,
    resizeMode:'cover',
    marginRight: 20,
    
  },
  filterContainer:{
    justifyContent: "center",
    alignItems: "center",
    marginRight: globals.MARGIN.marginTen,
    height : "100%"
  },
  caseFilterDropDownTextStyle:{
    textAlign: "center",
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: globals.SCREEN_SIZE.width * 0.037
  },
  log:{
    width:40,
    height:40,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] ,

  },
  searchBox:{
    borderWidth:0.5,
    borderColor:globals.COLOR.background,
    backgroundColor:globals.COLOR.background,
   width:'75%',
   height:40,
    borderRadius:10,
    right:20,
    left:25,
    flexDirection:'row',
    alignItems:'center',
    //justifyContent:'center'
  },
  searchCat:{
    borderWidth:0.5,
    borderColor:globals.COLOR.background,
    backgroundColor:globals.COLOR.background,
   width:'90%',
   height:40,
    borderRadius:10,
    marginLeft:'2%',
    marginRight:'2%',
    flexDirection:'row',
    alignItems:'center',
    //justifyContent:'center'
  },
  
  searchBoxCategory:{
    borderWidth:0.5,
    borderColor:globals.COLOR.background,
    backgroundColor:globals.COLOR.background,
    width:'90%',
    height:40,
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center',
   // right:20,
    //left:25,
    flexDirection:'row',
  },
  search:{
    width:30,
    height:30,
  },
  textView:{
   
  },
  searchIcon:{
   // paddingLeft:'5%',
    justifyContent:'center',
    //marginTop:'1%'
  },
  searchIconCategory:{
   // paddingLeft:'2%',
    justifyContent:'center',
    //marginTop:'1%'
  },
  textIn:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoLight,
    fontSize:12,
    textAlign: I18nManager.isRTL ? "left" : "right",
  },
  textInPlace:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoLight,
    fontSize:12,
    textAlign: I18nManager.isRTL ? "right" : "left",
   // width:'40%'
  },
  
  loveView:{
    right:'20%',
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",

  },
  love:{
    height: 30,
    width: 30,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  title:{
 
   flexDirection:'row',
   alignSelf:'center',
   paddingLeft:'10%'
  },
  leftHead:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoSemiBold,
    fontSize:I18nManager.isRTL ? 15:18,
    textAlign: I18nManager.isRTL ? "right" : "left", 
  },
  leftHeadBold:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold :globals.FONTS.cairoBold,
    fontSize:18,
    textAlign: I18nManager.isRTL ? "right" : "left", 
  },
  head:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold :globals.FONTS.cairoBold,
    fontSize:18,
    textAlign:"left", 
    color:globals.COLOR.text,
  },
  subHeadDone:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoSemiBold,
    fontSize:14,
    textAlign:  "left", 
    color:'#2c8927',
  },
  subHeadPending:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.cairoSemiBold,
    fontSize:14,
    textAlign:  "left", 
    color:'#FF8001',
  },
  leftHeadPro:{
    marginLeft:'3%',
    marginTop:'2%',
    //alignItems:'center',
    flexDirection:'row',
    alignItems:I18nManager.isRTL ? 'flex-end' :'flex-start'

   
  },
  notifyCount: {
    width: 18,
    height: 18,
    backgroundColor: '#ff2c2c',
    borderRadius: 10,
    position: 'absolute',
    right: 20,
    bottom:14,
    //top: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
});

export { images, styles  };