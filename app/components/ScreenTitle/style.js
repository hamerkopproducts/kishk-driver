import {StyleSheet, I18nManager} from 'react-native';
import globals from '../../lib/globals';

const images = {
  backArrow: require('../../assets/images/header/Back.png'),
};
const style = StyleSheet.create({
  container: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
    backgroundColor: globals.COLOR.headerColor,
  },
  innerWarp: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIMG: {
    width: 30,
    height: 30,
  },
  textWarp: {
    marginLeft: 15,
  },
  textHead: {
    fontSize: globals.FONTSIZE.fontSizeTwenty,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.black,
  },
  textSub: {
    fontSize: globals.FONTSIZE.fontSizeSixteen,
    fontFamily: globals.FONTS.cairoSemiBold,
    color: globals.COLOR.red,
  },
});

export {style, images};
