import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {style, images} from './style';

const ScreenTitle = () => {
  return (
    <View style={style.container}>
      <View style={style.innerWarp}>
        <TouchableOpacity>
          <Image source={images.backArrow} style={style.backIMG} />
        </TouchableOpacity>
        <View style={style.textWarp}>
          <Text style={style.textHead}>Order Details</Text>
          <Text style={style.textSub}>Ready for Collection</Text>
        </View>
      </View>
    </View>
  );
};

export default ScreenTitle;
