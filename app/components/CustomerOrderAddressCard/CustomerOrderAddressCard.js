import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, AppState, I18nManager } from 'react-native';
import { styles, images } from './styles';
import globals from '../../lib/globals';
import AddressdetailsCard from '../AddressdetailsCard';
import appTexts from '../../lib/appTexts';
import _ from 'lodash'
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
const CustomerOrderAddressCard = (props) => {
  const {
    order_Details,
    call,
    viewLocation
  } = props;
  const [addressTab, setAddressTab] = useState(true);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.cusNameHead}>{appTexts.ORDER.name}</Text>
        <Text style={styles.cusName}>{_.get(order_Details, 'customer.cust_name', '')}</Text>
      </View>
      <View style={styles.addressContainer}>
        <TouchableOpacity
          style={styles.addressTab}
          onPress={() => setAddressTab(true)}>
          <Text
            style={[
              styles.tabLabel,
              { color: addressTab ? globals.COLOR.red : globals.COLOR.textGrey },
              { fontWeight: addressTab ?  I18nManager.isRTL ? '600' : '600' : I18nManager.isRTL ? '100' : '100' },
            ]}>
            {appTexts.ORDER.Delivery}
          </Text>
          {addressTab && <View style={styles.tabLine}></View>}
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.addressTab, { marginLeft: 30 }]}
          onPress={() => setAddressTab(false)}>
          <Text
            style={[
              styles.tabLabel,
              { color: addressTab ? globals.COLOR.textGrey : globals.COLOR.red },
              { fontWeight: addressTab ? '100' : '600' },
            ]}>
            {appTexts.ORDER.Billing}
          </Text>
          {!addressTab && <View style={styles.tabLine}></View>}
        </TouchableOpacity>
      </View>
      <AddressdetailsCard order_Details={order_Details} addressTab={addressTab} />
      <View style={styles.contactWarp}>
        <TouchableOpacity onPress={() => viewLocation(order_Details.delivery_loc_coordinates, order_Details.delivery_loc)} style={styles.locationWarp}>
          <Image source={images.pin} style={styles.callIcon} />
          <Text style={[styles.buttonLab, { color: '#1492E6' }]}>
            {appTexts.ORDER.view}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity 
       onPress={() => call(order_Details.customer.country_code+order_Details.customer.phone)}
        style={styles.callWarp}>
          <Image source={images.callIcon} style={styles.callIcon} />
          <Text style={[styles.buttonLab, { color: '#434A5E' }]}>
            {appTexts.ORDER.call}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
CustomerOrderAddressCard.propTypes = {
  call: PropTypes.func,
  viewLocation: PropTypes.func,
};
export default CustomerOrderAddressCard;
