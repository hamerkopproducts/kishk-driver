import {StyleSheet, I18nManager} from 'react-native';
import globals from '../../lib/globals';
const images = {
  callIcon: require('../../assets/images/addressCard/call.png'),
  pin:require('../../assets/images/addressCard/blue.png'),
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: globals.COLOR.screenBackground,
    marginVertical: 10,
    paddingTop: 25,
    paddingBottom: 20,
    paddingHorizontal: 20,
  //  backgroundColor:'green'
  },
  cusNameHead: {
    color: '#707070',
    fontFamily: globals.FONTS.cairoRegular,
    fontSize: 14,
    textAlign:'left'
  },
  cusName: {
    color: '#434A5E',
    fontFamily: globals.FONTS.cairoSemiBold,
    fontSize: 15,
    textAlign:'left'
  },
  addressContainer: {
    flexDirection: 'row',
  },
  addressTab: {
    paddingVertical: 15,
  },
  tabLabel: {
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoSemiBold,
    fontSize: 16,
  },
  tabLine: {
    height: 3,
    backgroundColor: globals.COLOR.red,
    marginTop: 10,
  },
  contactWarp: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  locationWarp: {
    flexDirection: 'row',
    borderColor: '#1492E6',
    borderWidth: 1,
    borderRadius: 5,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    width:'48%'
  },
  callWarp: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#434A5E',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 20,
    width:'48%'
  },
  callIcon: {
    width: 22,
    height: 22,
  },
  buttonLab: {
    fontFamily: globals.FONTS.cairoSemiBold,
    fontSize: 16,
  },
});

export {styles, images};
