//import LinearGradient from "react-native-linear-gradient";
import React from 'react';
import { images, styles } from "./styles";
import {  View, Text, TouchableOpacity,Image,TouchableHighlight } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
//import RoundButton from "../RoundButton";
import Modal from 'react-native-modal';

const ModalComponent = (props) => {

  const {
      isModalComponentVisible,  
      modalSwipePress,
      modalImage,
      modalTitle,
      buttonLabelOne,
      buttonLabelTwo,
      onYesClick,
      onNoClick
      } = props;

  
  return (
    <Modal
        transparent={true}
        animationIn="slideInUp" 
          animationOut="slideOutRight" 
          onSwipeComplete={modalSwipePress}
          swipeDirection={["left", "right", "up", "down"]}
          isVisible={isModalComponentVisible}
          style={styles.modalMaincontentLogout}
        >
          <View style={styles.modalmainviewLogout}>
            
              <View style={styles.logoutImageView}>
                <Image source={modalImage} style={styles.logoutImage}/>
              </View>
              <View style={styles.logoutTextView}>
                <Text style={styles.logoutTextStyle}>{modalTitle}</Text>
              </View>
              <View style={styles.buttons}>
              <TouchableOpacity style={styles.butCancel} onPress={()=>{onNoClick()}}>
              <View>
               <Text style={styles.cancelText}>{buttonLabelOne}</Text> 
              </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.butLogout} onPress={()=>{onYesClick()}}>
              <View>
               <Text style={styles.logoutText}>{buttonLabelTwo}</Text> 
              </View>
              </TouchableOpacity>
              
            </View>
              
          </View>
        </Modal>
  );
};

ModalComponent.propTypes = {

isModalComponentVisible:PropTypes.bool,
modalSwipePress:PropTypes.func,
onNoClick:PropTypes.func,
onYesClick:PropTypes.func,
};

export default ModalComponent;