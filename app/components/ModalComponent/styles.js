import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    
};

const styles = StyleSheet.create({
    modalMaincontentLogout: {
        justifyContent: "center",
        alignItems:'center',
      
      },
      modalmainviewLogout: {
        backgroundColor: "white",
        //width: wp("90%"),
        //padding: "4%",
        //flex:1,
        paddingLeft:'3%',
        paddingRight:'3%',
       // flex:0.5,
        width:320,
        borderRadius:15,
        justifyContent:'center',
        //alignItems:'center',
        //borderRadius: 10,
        // borderTopRightRadius:15,
        // borderTopLeftRadius:15,
        borderColor: "rgba(0, 0, 0, 0.1)",
      },
      logoutImageView:{
        alignItems:'center',
        justifyContent:'center',
        marginTop:'10%'
      },
      logoutImage:{
        width:140,
        height:140,
      },
      logoutTextView:{
        alignItems:'center',
        justifyContent:'center',
        marginTop:'10%',
        marginBottom:'15%',
        //width:'100%',
        marginLeft:'10%',
        marginRight:'10%'
      //  backgroundColor:'purple'
        // paddingLeft:'10%',
        // paddingRight:'10%'
         
      },
      logoutTextStyle:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoSemiBold,
        color:globals.COLOR.blackTextColor,
        fontSize:18,
        textAlign:'center'
      },
      logoutTetStyle:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.cairoSemiBold,
        color:globals.COLOR.blackTextColor,
        fontSize:18,
      },
      signButtons:{
        flexDirection:'row',
        justifyContent:'space-between',
    
        //alignItems:'center',
        marginLeft:'6%',
        marginRight:'6%',
        marginTop:'3%',
        marginBottom:'8%'
      },
      buttons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        //marginTop: '10%',
        marginBottom: '15%',
        width: '90%',
        marginLeft: '5%',
        marginRight: '5%',
       // backgroundColor:'green'
      },
      butCancel: {
        width: '48%',
        height: 40,
        borderRadius: 10,
        borderColor:'red',
        borderWidth:0.5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
      },
      butLogout: {
        width: '48%',
        height: 40,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: globals.COLOR.red,
      },
      cancelText: {
        color: 'red',
        fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.cairoSemiBold,
        fontSize: I18nManager.isRTL ? 13 : 14,
      },
      logoutText: {
        color: 'white',
        fontFamily: I18nManager.isRTL
          ? globals.FONTS.notokufiArabic
          : globals.FONTS.cairoSemiBold,
        fontSize: I18nManager.isRTL ? 13 : 14,
      },

});

export { images, styles };