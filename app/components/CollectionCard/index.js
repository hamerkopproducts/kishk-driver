import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, I18nManager } from 'react-native';
import { styles, images } from './styles';
import appTexts from '../../lib/appTexts';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
const CollectionCard = props => {
  const { item, itemClick, tabIndex, isrequest } = props;
  return (
    <TouchableOpacity
      style={styles.fullWidthRowContainer}
      onPress={() => {
        itemClick(item.id, isrequest, item.sub_order_id);
      }}>
      <View style={styles.rowContainer}>
        <View style={styles.addressLine}>
          <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '83%' }}>
              {item.ord_id ? <Text style={styles.orderTextHead}>{appTexts.ORDER.id}</Text>
            :   <Text style={styles.orderTextHead}>{appTexts.ORDER.drid}</Text>
            }
              {!isrequest ? <Text style={item.order_status === 'Collection pending' ?  styles.statustextOne : styles.orderStatusCollected}>{item.order_status}</Text>
              : <Text style={item.status === '2' ?  styles.statustextOne : styles.orderStatusCollected}>{item.request_status}</Text>
            }
            </View>
            {item.ord_id ? <Text style={styles.textOne}>{item.ord_id}</Text>
            : <Text style={styles.textOne}>{item.delivery_request_id}</Text> }
          </View>
        </View>
       { !isrequest && <View style={{ paddingBottom: 20, marginTop: 10 }}>
          <Text style={styles.orderTextHead}>{appTexts.ORDER.subordid}</Text>
          <Text style={styles.textOne}>{item.sub_order_id}</Text>
        </View>}
       { isrequest && <View style={{ paddingBottom: 20, marginTop: 10 }}>
          <Text style={styles.orderTextHead}>{appTexts.ORDER.location}</Text>
          <Text style={styles.textOne}>{item.pickup_location}</Text>
        </View>}
        <View style={{ borderColor: '#F6F6F6', borderWidth: .6 }} />
        <View style={styles.personDetails}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={images.date} style={styles.locationIcon} />
            <Text style={styles.nameText}>{item.collection_date}</Text>
            <Image source={images.clock} style={styles.locationIcon} />
            {!isrequest ? <Text style={styles.nameText}>{item.time_slot}</Text>
            : <Text style={styles.nameText}>{item.collection_time}</Text> }
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
CollectionCard.propTypes = {
  item: PropTypes.object,
  tabIndex: PropTypes.number,
};

export default CollectionCard;
