import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, I18nManager } from 'react-native';
import { styles, images } from './styles';
import appTexts from '../../lib/appTexts';
const TodaysOrderCard = props => {
  const { item, itemClick, tabIndex, call, viewLocation,topTabIndex } = props;
  return (
    <TouchableOpacity
      style={styles.fullWidthRowContainer}
      onPress={() => {
        itemClick(item.id);
      }}>
      <View style={styles.rowContainer}>
        <View style={styles.addressLine}>
          <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row' }}>
              { topTabIndex === 0 ? <Text style={styles.orderTextHead}>{appTexts.ORDER.id}</Text> 
              : <Text style={styles.orderTextHead}>{appTexts.ORDER.drid}</Text> }
            </View>
            {topTabIndex === 0 ? <Text style={styles.textOne}>{item.ord_id}</Text>
          : <Text style={styles.textOne}>{item.delivery_request_id}</Text>  
          }
          </View>

        </View>
       { topTabIndex === 0 && <View style={{ paddingBottom: 20, marginTop: 0 }}>
          <Text style={styles.orderTextHead}>{appTexts.ORDER.subordid}</Text>
          <Text style={styles.textOne}>{item.sub_order_id}</Text>
        </View>}
        {topTabIndex === 1 && <Text style={styles.orderTextHead}>{appTexts.ORDER.location}</Text>}
        <View style={{ marginBottom: 5 }}>
          <View style={styles.locationOuterView}>
            <View style={styles.locationInnerView}>
              <Image source={images.location} style={styles.locationIcon} />
              <Text style={styles.locationName}>{item.delivery_location}</Text>
            </View>
            <TouchableOpacity onPress={() => viewLocation(item)} >
              <Text style={styles.viewLoc}>{appTexts.ORDERCARD.location}</Text>
            </TouchableOpacity>
          </View>
        </View>
    { topTabIndex === 0 && <View style={styles.devidedLine} /> }
      { topTabIndex === 0 && <View style={styles.nameView}>
          <View style={styles.nameInnerView}>
            <Text style={styles.custNameTitle}>{appTexts.ORDERCARD.cname}</Text>
            <Text style={styles.custName}>{item.customer_name}</Text>
          </View>
          <View style={styles.callView}>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => call(item.country_code + item.phone)}>
              <Image source={images.call} style={styles.locationIcon} />
              <Text style={styles.callNow}>{appTexts.ORDERCARD.callNow}</Text>
            </TouchableOpacity>
          </View>
        </View>}
        <View style={styles.devidedLine} />
        <View style={styles.personDetails}>

          <View style={styles.dateTimeView}>
            <View style={styles.dateView}>
              <Image source={images.date} style={styles.locationIcon} />
              <Text style={styles.nameText}>{item.delivery_date}</Text>
            </View>
            <View style={styles.timeView}>
              <Image source={images.clock} style={styles.locationIcon} />
              <Text style={styles.nameText}>{ topTabIndex === 0 ? item.delivery_time_slot : item.delivery_time}</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
TodaysOrderCard.propTypes = {
  item: PropTypes.object,
  call: PropTypes.func,
  viewLocation: PropTypes.func,
  tabIndex: PropTypes.number,
};

export default TodaysOrderCard;
