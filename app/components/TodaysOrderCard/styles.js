import { StyleSheet, I18nManager } from 'react-native';
import globals from '../../lib/globals';
const images = {
  date: require('../../assets/images/addressCard/date.png'),
  clock: require('../../assets/images/addressCard/time.png'),
  location: require('../../assets/images/addressCard/loc.png'),
  call: require('../../assets/images/addressCard/call.png'),
};
const styles = StyleSheet.create({
  fullWidthRowContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    marginTop: 5,
    marginBottom: 7,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 15,

    //borderRadius:10,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1 : 3,
    borderColor: globals.COLOR.lightgrey,
  },

  viewLoc: {
    color: '#1492E6',
    flexDirection: 'row-reverse',
    textDecorationLine: 'underline',
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
  },

  callView: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  nameView: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  locationInnerView: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '70%'
  },
  devidedLine: {
    borderColor: '#F6F6F6',
    borderWidth: .9,
    marginTop: 5
  },
  dateView: {
    width:'37%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  timeView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dateTimeView: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  locationOuterView: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  nameInnerView: {
    flexDirection: 'column',
    width: '70%'
  },
  orderidView: {
   // flexDirection: 'row',
    //alignItems: 'center',
    
  },
  textOne: {
    //textAlign: 'right',
    color: '#000000',
    fontSize: 14,
    marginLeft: 3,
    marginRight:8,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.cairoSemiBold
      : globals.FONTS.cairoSemiBold,
  },
  orderTextHead: {
    color: '#707070',
    fontFamily: globals.FONTS.cairoRegular,
    fontSize: 12,
    textAlign: 'left',
    marginLeft: 3,
    marginRight:10,
  },
  callNow: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairoSemiBold,
    marginLeft: 3,
    color: '#434A5E'
  },
  custName: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairoSemiBold,
      textAlign: 'left',
      color: '#434A5E'
  },
  custNameTitle: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairoSemiBold,
    color: '#707070',
    textAlign: 'left',
  },

  locationName: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
    marginLeft: 5,
    marginRight: 5,
    fontSize:12,
    color:'#434A5E'
  },
  orderIdText: {
    color: '#000000',
    fontSize: 14,
    marginRight: 8,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairosemiBold,
  },
  nameText: {
    color: '#222222',
    fontSize: 13,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
    marginLeft: 10
  },
  addressLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 8
  },
  locationIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
  },
  personDetails: {
    marginTop: 4,
    marginBottom: 4,
    paddingTop: 9,
  },
});

export { styles, images };
