import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  homeIconSelected : require("../../assets/images/footerTabItem/homgreen.png"),
  homeIconUnSelected : require("../../assets/images/footerTabItem/home.png"),
  orderIconSelected : require("../../assets/images/footerTabItem/deliverygreen.png"),
  orderIconUnSelected : require("../../assets/images/footerTabItem/Delivery.png"),
  scanIconSelected : require("../../assets/images/footerTabItem/scanning.png"),
  scanIconUnSelected : require("../../assets/images/footerTabItem/scanning.png"),
  profileIconSelected : require("../../assets/images/footerTabItem/profileselect.png"),
  profileIconUnSelected : require("../../assets/images/footerTabItem/profile.png")
};

const styles = StyleSheet.create({
  tabBarItem: {
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBarIconContainer:{
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight-50,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  tabBarIconContainerScan:{
    marginBottom: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 62,
    height: 62,
    alignItems: 'center',
    justifyContent: 'center',
   
    shadowColor: '#fff',
    shadowOpacity: 0.1,
    shadowOffset: { x: 2, y: 0 },
    shadowRadius: 4,
    borderRadius: 30,
    position: 'absolute',
    bottom: 20,
    right: 0,
    top: 5,
    left: 5,
    shadowOpacity: 5.0,

},
scanBtn: {
  position: 'absolute',
  alignSelf: 'center',
  backgroundColor: '#F0F1F0',
  width: 72,
  height: 72,
  borderRadius: 35,
  bottom: 35,
  zIndex: 10
},
actionBtn: {
    textShadowOffset: { width: 20, height: 5 },
},
  badgeCountContainer:{
    position: 'absolute',
    top: (globals.INTEGER.footerTabBarHeight/2)-15,
    left: ((globals.SCREEN_SIZE.width / 4)/2),
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  bottomimage:{
    height:45,
    width:45,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  bottomimagescan:{
    height:70,
    width:70,
  },
  badgeCount:{
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.helveticaNeueLight,
    textAlign: 'center',
    fontSize: globals.SCREEN_SIZE.width * 0.030
  },
  tabLabel:{
    top:0,
    color:'black',
    fontSize:10
  },
});

export { images, styles  };
