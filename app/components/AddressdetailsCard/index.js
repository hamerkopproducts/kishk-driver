import React, {Component} from 'react';
import {View, Image, Text, TouchableOpacity, I18nManager} from 'react-native';
import {styles, images} from './styles';

const AddressdetailsCard = ({order_Details, addressTab}) => {
  return (
    <View>
      <View style={styles.fullWidthRowContainer}>
        <View style={styles.rowContainer}>
          <View style={styles.addressLine}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={images.office} style={styles.resIcon} />

              <Text style={styles.textOne}>
                {addressTab ? order_Details.b_address_type : order_Details.d_address_type }
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', marginTop: 2}}>
            <Text style={styles.nameText}>
            {addressTab ? order_Details.billing_name : order_Details.delivery_name }
            </Text>
          </View>
          <View style={styles.personDetails}>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.nameText}>
                {addressTab ? order_Details.billing_phone : order_Details.delivery_phone }
              </Text>
            </View>
          </View>
          <View style={styles.addresswrapper}>
            <Text style={styles.addressText}>
            {addressTab ? order_Details.billing_address : order_Details.delivery_address } 
            </Text>
            <Text style={styles.addressText}>
            {addressTab ? order_Details.billing_city : order_Details.delivery_city } 
            </Text>
            <Text style={styles.addressText}>
            {addressTab ? order_Details.billing_country : order_Details.delivery_country } 
            </Text>
            <Text style={styles.addressText}>
            {addressTab ? order_Details.b_land_mark : order_Details.d_land_mark } 
            </Text>
            <Text style={styles.addressText}>
            {addressTab ? order_Details.billing_zip : order_Details.delivery_zip } 
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default AddressdetailsCard;
