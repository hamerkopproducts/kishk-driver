import React , { useEffect, useState }from 'react';
import { View, Text, FlatList, I18nManager, Image, TouchableOpacity } from 'react-native';
import OrderCard from '../OrderCard/OrderCard';
import { styles, images } from './styles';
import appTexts from '../../lib/appTexts';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as collectionActions from '../../actions/collectionActions';
import _ from 'lodash'
import ModalComponent from '../../components/ModalComponent';
import NetInfo from "@react-native-community/netinfo";
import globals from '../../lib/globals';


const OrderSummaryCard = props => {
  const {
    isCollectionScreen,
    subOrderData,
    navigation,
    fullData
  } = props;
  const [isModalComponentVisible, setIsModalComponentVisible] = useState(false);
  const [subId, setSubId] = useState('');
  const [orderId, setOrderId] = useState('');

//mounted
useEffect(() => handleComponentMounted(), []);

const handleComponentMounted = () => {
  NetInfo.fetch().then(state => {
    if (state.isConnected) {
      // props.getOrdersDetails(order_id, props.token, isCollectionScreen)
    } else {
      functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
    }
  });
}
//updated
useEffect(() => handleComponentUpdated());

const handleComponentUpdated = () => {
  NetInfo.fetch().then(state => {
    if (state.isConnected) {
      // if (_.get(props, 'collectOrderStatus.success')) {
      //   props.getOrdersDetails(order_id, props.token,isCollectionScreen)
      //   functions.displayToast(
      //     'success',
      //     'top',
      //     appTexts.COLLECTORDER.success,
      //     _.get(props, 'collectOrderStatus.msg'),
      //   );
      // }
    } else {
      functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
    }
    // if(_.get(props, 'orderDetailsData.order_status') === 'Collection completed' && 
    // _.get(props, 'collectOrderStatus.success'))
    // {
    //   props.resetCollcetSuccess()
    //   props.navigation.navigate('HomeScreen')
    // }
    // else if(_.get(props, 'collectOrderStatus.success'))
    // {
    //   // console.log(";;;;;;;;;;;;;",_.get(props, 'collectOrderStatus.success'), _.get(props, 'orderDetailsData.order_status'))
    //   props.resetCollcetSuccess()
    // }
  });

};

 const openModalComponent = (order_id, sub_id) => {
    setSubId(sub_id)
    setOrderId(order_id)
    setIsModalComponentVisible(!isModalComponentVisible)
  };
  const onYesClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
    let params = {
      "order_id": orderId,
      "sub_id": subId,
    }
    props.deliverTheOrder(params, props.token)
  };
  const onNoClick = () => {
    setIsModalComponentVisible(!isModalComponentVisible)
  };
  const SummaryCard = ({ item }) => (
    <View style={styles.summaryCardWrapper}>
      <View style={styles.innerContainer}>
        <Image source={{ uri: item.cover_image }} style={styles.orderIMG} />
        <View style={styles.innerHead}>
          <View style={styles.innerTitleHead}>
            <Text style={styles.innerTitle} numberOfLines={2}>
              {I18nManager.isRTL ? item.product.lang[1].name : item.product.lang[0].name}
            </Text>
          </View>
          <View style={styles.bottomTextWarp}>
            <View style={styles.bottomInnerWarp}>
              <Text style={styles.sarText}>{appTexts.ORDER.sar}</Text>
              <Text style={styles.sarValue}>{item.item_price}</Text>
              {I18nManager.isRTL ?
                <Text style={styles.quantity}>{item.item_count}X</Text> :
                <Text style={styles.quantity}>X{item.item_count}</Text>}
            </View>
            <View style={styles.bottomInnerWarp}>
              <Text style={styles.sarText}>{appTexts.ORDER.sar}</Text>
              <Text style={styles.sarValue}>{item.grant_total}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
  const renderItem = ({ item, index }) =>
  ( // style={styles.supplierCard}
    <View >
      <Text style={styles.innerHeading}>{item.sub_id}</Text>
      <FlatList
        data={item.items}
        extraData={item.items}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        renderItem={renderSummaryCard}
      />
      {
      item.sub_order_status === 1 ?
      (<View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.buttonWarpContainer}
        onPress={() => {
          openModalComponent(item.order_id, item.id);
        }}
        >
          <Text style={styles.buttonTextLabel}>
            {appTexts.ORDER.markDelivered}
          </Text>
        </TouchableOpacity>
      </View>) :
      (<View style={styles.bottomButtonInnerWarp}>
        <Image source={images.tickIcon} style={styles.bottomIMG} />
        <Text style={styles.bottomButtonLabel}>{appTexts.ORDER.delivered}</Text>
      </View>)
}
    </View>
  );
  const renderSummaryCard = ({ item, index }) => (
    <SummaryCard item={item} />
  );
  return (
    <View style={styles.container}>
      <Text style={styles.containerTitle}>{appTexts.ORDER.summary} ({fullData.item_count}) </Text>
      <FlatList
        data={subOrderData}
        extraData={subOrderData}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
      />
        <ModalComponent
        isModalComponentVisible={isModalComponentVisible}
        // modalSwipePress={closeModalComponent}
        modalImage={images.deliverModal}
        modalTitle={appTexts.MODAL.deliver}
        buttonLabelOne={appTexts.MODAL.no}
        buttonLabelTwo={appTexts.MODAL.yes}
        onYesClick = {onYesClick}
        onNoClick = {onNoClick}
      />
    </View>
    
  );
};


const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, 'loginReducer.userData.data.access_token', ''),
    deliveredOrderStatus: _.get(state, 'collectionReducer.deliveredOrderStatus', ''),
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    deliverTheOrder: collectionActions.deliverOrder,

  }, dispatch)
};

const OrderSummaryCardWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderSummaryCard);

OrderSummaryCardWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default OrderSummaryCardWithRedux;
