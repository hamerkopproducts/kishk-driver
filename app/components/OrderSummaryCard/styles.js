import {StyleSheet, I18nManager} from 'react-native';
import globals from '../../lib/globals';

const images = {
  demo: require('../../assets/images/products/img3.png'),
  tickIcon: require('../../assets/images/ChooseLanguage/checkbox.png'),
  deliverModal: require('../../assets/images/modal/deliverModal.png'),
  tickIcon: require('../../assets/images/ChooseLanguage/checkbox.png'),
  // buttonArrow: require('../../assets/images/cart/arrow.png'),
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: globals.COLOR.screenBackground,
    //marginVertical: 10,
    //backgroundColor:'red',
   marginBottom:5,
    paddingTop: 25,
    paddingBottom: 20,
    paddingHorizontal: 5,
    
  },
  supplierCard: {
    backgroundColor:'white',
    // flexDirection: 'row',
    margin: 10,
    // paddingHorizontal:10,
   borderRadius:20,
    // paddingVertical:20,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 6 : 3,
    borderColor: globals.COLOR.lightgrey,
    // borderWidth: 1, 
    // marginBottom: 10, 
    // borderRadius: 10, 
    padding: 5,
    // shadowOffset: {
    //   width: 1,
    //   height: 1,
    // },
    // shadowOpacity: 0.2,
    // elevation: Platform.OS === 'ios' ? 6 : 3,
    // borderColor: globals.COLOR.lightgrey,
    // backgroundColor: 'white'
  },
  bottomButtonLabel: {
    fontSize: 14,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.red,
  },
  bottomIMG: {
    width: 20,
    height: 20,
    borderRadius: 30,
    marginRight: 10,
  },
  bottomButtonInnerWarp: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderColor: globals.COLOR.red,
    borderWidth: 1,
    paddingHorizontal: 28,
    paddingVertical: 2,
    borderRadius: 8,
    marginTop: 10,
  },
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    backgroundColor: globals.COLOR.screenBackground,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  buttonWarpContainer: {
    backgroundColor: globals.COLOR.red,
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderRadius: 10,
  },
  buttonTextLabel: {
    backgroundColor: 'red',
    color: 'white',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    fontSize: 13,
  },
  summaryCardWrapper:{
  // backgroundColor:'green',
//  borderRadius:10,
  },
  containerTitle: {
    fontSize: 16,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold :globals.FONTS.cairoBold,
    color: globals.COLOR.black,
    marginLeft:10
  },
  innerWarp: {
    marginHorizontal: 5,
    marginVertical: 10,
  },
  innerHeading: {
    fontSize: 14,
    fontFamily: globals.FONTS.cairoSemiBold,
    color: '#434A5E',
    textAlign:'left',
    marginLeft:10
  },
  orderIMG: {
    width: 100,
    height: 100,
    borderRadius: 15,
    marginVertical:5
  },
  innerContainer: {
   backgroundColor:'white',
    flexDirection: 'row',
    margin: 10,
    paddingHorizontal:10,
   borderRadius:20,
    paddingVertical:20,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 6 : 3,
    borderColor: globals.COLOR.lightgrey,
           
  },
  innerHead: {
    marginHorizontal: 10,
    marginVertical: 10,
    marginTop:25
   // backgroundColor:'green'
  },
  innerTitleHead: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  customWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: globals.COLOR.ItemColor,
    borderRadius: 5,
    marginLeft: 10,
    position: 'absolute',
    right: 10,
    top: 13,
    paddingHorizontal: 5,    
  },
  customText: {
    fontSize: I18nManager.isRTL ? 10 :14,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.black,
    textAlign:'auto'
    //width: 50,
  },
  innerTitle: {
    width: '80%',
    fontSize: 13,
    fontFamily: globals.FONTS.cairoSemiBold,
    color: globals.COLOR.black,
  },
  bottomTextWarp: {
    flexDirection: 'row',
    marginTop: 10,
  },
  bottomInnerWarp: {
    flexDirection: 'row',
    marginRight: 35,
    alignItems: 'center',
  },
  sarText: {
    marginRight: 5,
    fontSize: I18nManager.isRTL ? 10 :14,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.greyText,
  },
  sarValue: {
    marginRight: 5,
    fontSize: 15,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.cairoBold,
    color: globals.COLOR.black,
  },
  quantity: {
    fontSize: globals.FONTSIZE.fontSizeSixteen,
    fontFamily: globals.FONTS.cairoRegular,
    color: globals.COLOR.black,
  },
});

export {styles, images};
