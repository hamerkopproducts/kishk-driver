import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from "./styles";

const DividerLine = (props) => {
	const {
		dividerStyle
	} = props;

  return (
	  <View style={styles.dividerStyle}/>
  );
};

DividerLine.propTypes = {
	dividerStyle: PropTypes.object
};

export default DividerLine;
