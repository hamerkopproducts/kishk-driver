import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, I18nManager, AppState } from 'react-native';
import { styles } from './styles';
import DotedDivider from '../DotedDivider';
import appTexts from '../../lib/appTexts';

const PriceCard = ({ order_Details }) => {
  return (
    <View style={styles.fullContainer}>
      <View style={styles.cardHeader}>
        <Text style={styles.headerLabel}>{appTexts.ORDER.bill}</Text>
      </View>
   { order_Details.sub_total > 0 &&
   <View style={styles.formwrapper}>
        <Text style={styles.TextView}>{appTexts.ORDER.subtotal}</Text>
        <View style={styles.wrapper}>
          <View style={styles.pricelight}>
            <Text style={styles.TextViewLight}>{appTexts.ORDER.sar}</Text>
          </View>       
          <Text style={styles.TextView}>{order_Details.sub_total}</Text>
        </View>
      </View>}
      {order_Details.delivery_charge > 0 &&
        <View style={styles.deliverywrap}>
        <Text style={styles.TextViewLight}>{appTexts.ORDER.deliveryCh}</Text>
        <View style={styles.wrapper}>
          <View style={{ right: 3, top: 1 }}>
            <Text style={styles.TextViewLight}>{appTexts.ORDER.sar}</Text>
          </View>
          <Text style={styles.TextView}>{order_Details.delivery_charge}</Text>
        </View>
      </View>}
     { order_Details.service_charge > 0 &&
     <View style={styles.deliverywrap}>
        <Text style={styles.TextViewLight}>{appTexts.ORDER.serviceCh}</Text>
        <View style={styles.wrapper}>
          {order_Details.service_charge_type === 'percentage' ?
            <Text style={styles.TextView}>{order_Details.service_charge} %</Text>
            :
            <View style={{flexDirection: 'row', right: 3, top: 1 }}>
              <Text style={styles.TextViewLight}>{appTexts.ORDER.sar}</Text>
              <Text style={styles.TextView}>{order_Details.service_charge}</Text>
            </View>

          }
        </View>
      </View>}

     { order_Details.cod_fee > 0 &&
     <View style={styles.deliverywrap}>
        <Text style={styles.TextViewLight}>{appTexts.ORDER.codfee}</Text>
        <View style={styles.wrapper}>
          <View style={{ right: 3, top: 1 }}>
            <Text style={styles.TextViewLight}>{appTexts.ORDER.sar}</Text>
          </View>
          <Text style={styles.TextView}>{order_Details.cod_fee}</Text>
        </View>
      </View>}
     { order_Details.coupon_code > 0 &&
     <View style={styles.deliverywrap}>
        <Text style={styles.TextViewLight}>{appTexts.ORDER.discount}</Text>
        <View style={styles.wrapper}>
          <View style={{ right: 3, top: 1 }}>
            <Text style={styles.TextViewLight}>{appTexts.ORDER.sar}</Text>
          </View>
          <Text style={styles.TextView}>{order_Details.coupon_amount}</Text>
        </View>
      </View>}

     { order_Details.vat > 0 &&
     <View style={styles.deliverywrap}>
        <Text style={styles.TextViewLight}>{appTexts.ORDER.vat}</Text>
        <View style={styles.wrapper}>
          <Text style={styles.TextView}>{order_Details.vat} %</Text>
        </View>
      </View>}

      <DotedDivider />

      <View style={styles.deliverywrap}>
        <Text style={styles.TextView}>{appTexts.ORDER.total}</Text>
        <View style={styles.wrapper}>
          <Text style={styles.TextView}>{appTexts.ORDER.sar} </Text>
          <Text style={styles.TextView}>{order_Details.grant_total}</Text>
        </View>
      </View>
    </View>
  );
};

export default PriceCard;
