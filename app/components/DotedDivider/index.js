import React, {Component} from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const DotedDivider = props => {
  return <View style={styles.dividerStyle} />;
};

export default DotedDivider;
