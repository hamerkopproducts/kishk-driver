import {StyleSheet, I18nManager} from 'react-native';
import globals from '../../lib/globals';
const images = {
  date:require('../../assets/images/addressCard/date.png'),
  clock:require('../../assets/images/addressCard/time.png'),
};
const styles = StyleSheet.create({
  fullWidthRowContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 120,
    marginTop: 5,
    marginBottom: 7,
    backgroundColor: '#FFFFFF',
    padding: 8,
    borderRadius: 15,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1 : 3,
    borderColor: globals.COLOR.lightgrey,
  },
  secondContainer: {
    width: '100%',
    height: 20,
    flexDirection: 'row',
  },
  thirdContainer: {
    width: '100%',
    height: 20,
    flexDirection: 'row',
  },

  textOne: {
    color: '#000000',
    fontSize: 14,
    marginLeft: 8,
    marginRight:8,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.cairosemiBold,
  },
  nameText: {
    color: '#222222',
    fontSize: 13,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold
      : globals.FONTS.cairoBold,
      paddingLeft:'3.5%',
      paddingRight:'6.5%'
  },
  addressLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom:20,
    paddingTop:8
  },
  locationIcon: {
    width: 17,
    height: 17,
    resizeMode:'contain'
  },
  personDetails: {
    marginTop: 4,
    marginBottom: 4,
    paddingTop:9,
    paddingLeft:5,
  },
});

export {styles, images};
