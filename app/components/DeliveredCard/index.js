import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Image, Text, TouchableOpacity, I18nManager} from 'react-native';
import {styles, images} from './styles';
const CollectionCard = props => {
  const {item, itemClick, tabIndex,topTabIndex} = props;

  return (
    <TouchableOpacity
      style={styles.fullWidthRowContainer}
      onPress={() => {
        itemClick(item.id);
      }}>
      <View style={styles.rowContainer}>
        <View style={styles.addressLine}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.textOne}>{topTabIndex === 0 ? item.ord_id : item.delivery_request_id}</Text>
          </View>
        </View>
        <View style={{borderColor: '#F6F6F6', borderWidth: .6}} />
        <View style={styles.personDetails}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={images.date} style={styles.locationIcon} />
            <Text style={styles.nameText}>{item.delivery_date}</Text>
            <Image source={images.clock} style={styles.locationIcon} />
            <Text style={styles.nameText}>{ topTabIndex === 0 ? item.delivery_time_slot : item.delivery_time}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
CollectionCard.propTypes = {
  item: PropTypes.object,
  tabIndex: PropTypes.number,
};

export default CollectionCard;
