import React, { Component,useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity} from 'react-native';
import { images, styles } from "./styles";
import { connect } from "react-redux";
import globals from "../../lib/globals";
import appTexts from '../../lib/appTexts';
// import DateModal from '../DateModal';

const FloatButton = (props) => {
  const {
       isDate,
       isFilter,
       isCategory,
       buttonText,
       onButtonClick,
       onAddtoClick
      } = props;

      const [isDateModalVisible,setIsDateModalVisibile] = useState(false);
     
    
      const openDateModal = () =>{
        setIsDateModalVisibile(!isDateModalVisible)
      };
    
      const closeModal =() =>{
        setIsDateModalVisibile(!isDateModalVisible)
      };

      

      return (
    <View style={isDate ? styles.floata : isFilter ? styles.floata : isCategory ? styles.floata : styles.float}>
            {isDate ? null : isFilter ? null : isCategory ? null :
            <View style={styles.price}>
              <View style={styles.row}>
                <Text style={styles.lightSAR}>{appTexts.PRODUCT.sar} </Text>
                <Text style={styles.dark}>{appTexts.PRODUCT.amt} </Text>
                </View>
                <View style={{flexDirection:'row'}}>
                <Text style={styles.light}>{appTexts.PRODUCT.sar}</Text>
                <Text style={styles.light}>{appTexts.PRODUCT.hundred}</Text>
                </View>
            </View>
            }

            {isFilter ? (
              <View style={styles.clearBox}>
                    <Text style={styles.clearText}>{appTexts.FILTER.clear}</Text>
              </View>
            ): null}

            {isFilter ?  (
              <View style={styles.butFill}>
              <Text style={styles.results}>{appTexts.FILTER.results}</Text>
              <Text style={styles.results}>{appTexts.FILTER.apply}</Text>

            </View>
            ): null}
            {isCategory ? ( <TouchableOpacity onPress={()=>{onButtonClick();}}>
            <View style={styles.but1}>
              
                <Text style={styles.butText}>{buttonText}</Text>
             
            </View></TouchableOpacity>): null}

            {isDate ?  (
              <View style={styles.but1}>
              <TouchableOpacity onPress={() => { openDateModal() }}>
                <Text style={styles.butText}>{appTexts.DELIVERY_DATE.confirm}</Text>
                {/* <DateModal isDateModalVisible={isDateModalVisible} closeModal={closeModal}/> */}
                </TouchableOpacity>
            </View>
            ): isFilter ? null : isCategory ? null :
          
          
            <View style={styles.but}>
              <TouchableOpacity onPress={() => { onAddtoClick() }}>
                <Text style={styles.butText}>{appTexts.PRODUCT.addCart}</Text>
                
                </TouchableOpacity>
            </View>
            }
    </View>
       
     );
   };
   FloatButton.propTypes = {
     
   };
   
   export default FloatButton;
   